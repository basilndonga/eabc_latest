<?php

/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// No direct access
defined('_JEXEC') or die;

/**
 * @param    array    A named array
 *
 * @return    array
 */
function MembersBuildRoute(&$query)
{
	$segments = array();
	$view     = null;

	if (isset($query['task']))
	{
		$taskParts  = explode('.', $query['task']);
		$segments[] = implode('/', $taskParts);
		$view       = $taskParts[0];
		unset($query['task']);
	}
	if (isset($query['view']))
	{
		$segments[] = $query['view'];
		$view       = $query['view'];
		unset($query['view']);
	}
	if (isset($query['id']))
	{
		if ($view !== null)
		{
			$segments[] = $query['id'];
		}
		else
		{
			$segments[] = $query['id'];
		}

		unset($query['id']);
	}

	return $segments;
}

/**
 * @param    array    A named array
 * @param    array
 *
 * Formats:
 *
 * index.php?/shetrades/task/id/Itemid
 *
 * index.php?/shetrades/id/Itemid
 */
function MembersParseRoute($segments)
{
	$vars = array();
	JLoader::register('MembersFrontendHelper', JPATH_SITE . '/components/com_members/helpers/members.php');

	// view is always the first element of the array
	$vars['view'] = array_shift($segments);
	$model        = MembersFrontendHelper::getModel($vars['view']);

	while (!empty($segments))
	{
		$segment = array_pop($segments);

		// If it's the ID, let's put on the request
		if (is_numeric($segment))
		{
			$vars['id'] = $segment;
		}
		else
		{
			$vars['task'] = $vars['view'] . '.' . $segment;
		}
	}

	return $vars;
}

