<?php

/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
defined('_JEXEC') or die;

class MembersFrontendHelper
{
	

	/**
	 * Get an instance of the named model
	 *
	 * @param string $name
	 *
	 * @return null|object
	 */
	public static function getModel($name)
	{
		$model = null;

		// If the file exists, let's
		if (file_exists(JPATH_SITE . '/components/com_members/models/' . strtolower($name) . '.php'))
		{
			require_once JPATH_SITE . '/components/com_members/models/' . strtolower($name) . '.php';
			$model = JModelLegacy::getInstance($name, 'MembersModel');
		}

		return $model;
	}
	public static function getUserBusinesses($user_id){
		$db = JFactory::getDbo();
	        $db->getQuery(true);
	        $query = 'SELECT id FROM #__shetrades_biz_info WHERE created_by = '.$db->quote($user_id);
	        $db->setQuery($query);
	        $result = $db->loadObjectList();
	        return $result;
	}
	/*
    * function to get the owner of a business given the biz id
    */
    public static function getBusinessOwner($bizid){ 
        $db = JFactory::getDbo();
        $db->getQuery(true);
        $query = 'SELECT created_by FROM #__shetrades_biz_info WHERE id = '.$db->quote($bizid);
        $db->setQuery($query);
        $result = $db->loadResult();
        return $result;
    }
	public static function generateUsersforBiz(){
		$usersConfig = JComponentHelper::getParams('com_users');
        	$newUsertype = $usersConfig->get('new_usertype');

		$db = JFactory::getDbo();
		$db->getQuery(true);
		$query = 'SELECT id,name,email FROM #__shetrades_biz_info WHERE created_by = "0" AND state = "1"';
		$db->setQuery($query);
		$result = $db->loadObjectList();
		
		$list = array();
		$created_users = array();
		foreach ($result as $biz) {
			// generate random email e.g bizid@shetrades.com
			$newemail = 'biz_'.$biz->id.'_@shetrades.com';
			$biz->email = $newemail;
			//array_push($list, $biz);
			// create the user array
			$userdata = array(
		            "name"      => $biz->name, 
		            "username"  => $biz->email, 
		            "password"  => 'shetrades@2015',
		            "email"     => $biz->email
		        );
		        $user = new JUser;
		        $user->set('groups', array($newUsertype));
	                $user->set('usertype', 'deprecated');
	                //Write to database
		        if(!$user->bind($userdata)) {
		            throw new Exception("Could not bind data. Error: " . $user->getError());
		        }
		        if (!$user->save()) {
	                    throw new Exception("Could not save user. Error: " . $user->getError());
	                }
	                else {
	            	    // update the biz_info created_by column with the new user id
	            	    $query = 'UPDATE #__shetrades_biz_info SET created_by = '.$db->quote($user->id).' WHERE id = '.$db->quote($biz->id);
			    $db->setQuery($query);
			    $db->execute();
	            	    array_push($created_users, $user->id);
	                }
		}
		return $created_users;
	}
	public static function getBusinessesForMap(){
		$db = JFactory::getDbo();
	        $db->getQuery(true);
	        $query = "SELECT a.id, a.name, b.nicename AS country, a.city, a.company_desc FROM #__shetrades_biz_info a, #__shetrades_country b WHERE a.country = b.id AND a.state= 1";
	        $db->setQuery($query);
	        $result = $db->loadObjectList();
	        return $result;
	}
}
