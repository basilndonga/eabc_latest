<?php

/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
defined('_JEXEC') or die;

class MembersHelperMessaging
{	
	public static function getMessages(){
        $db = JFactory::getDbo();
        $db->getQuery(true);
        $query = 'SELECT m.sender AS sender_id,m.recipient AS recipient_id,m.body AS message_body,m.received,su.name AS sender_name,ru.name AS recipient_name 
                  FROM #__shetrades_messaging_system AS m
                  LEFT JOIN #__users AS su ON su.id = m.sender
                  LEFT JOIN #__users AS ru ON ru.id = m.recipient 
                  WHERE m.received ="0"';
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
	}
	public static function markDelivered($message_id){
		$db = JFactory::getDbo();
        $db->getQuery(true);
        $query = 'UPDATE #__shetrades_messaging_system SET delivered = "1" WHERE id = "'.$message_id.'"';
        $db->setQuery($query);
        if($db->execute()){
        	return true;
        }
        else {
        	return false;
        }
	}
  public static function getInbox($recipient){
      // recipient column
      $db = JFactory::getDbo();
      $db->getQuery(true);
      $query = 'SELECT m.id,m.sender AS sender_id,m.recipient AS recipient_id,m.body AS message_body,m.received,m.created_on,su.name AS sender_name,ru.name AS recipient_name 
                FROM #__shetrades_messaging_system AS m
                LEFT JOIN #__users AS su ON su.id = m.sender
                LEFT JOIN #__users AS ru ON ru.id = m.recipient 
                WHERE m.recipient = "'.$recipient.'" AND m.received ="0"';
      $db->setQuery($query);
      //$db->execute();
      $result = $db->loadObjectList();
      return $result;

  }
  /*
  * Gett messages in a certain conversation
  * One thread for two unique users
  * the fist message is the parent or initiator of the thread.
  */

  public static function getConversation($thread){
    $thread1 = $thread;
    $thread = explode('_',$thread);
    // swap the digits as well
    $thread2 = $thread[1].'_'.$thread[0];
    $db = JFactory::getDbo();
    $db->getQuery(true);
    $query = 'SELECT m.id,m.thread,m.created_on,m.sender AS sender_id,m.recipient AS recipient_id,m.body AS message_body,m.received,su.name AS sender_name,ru.name AS recipient_name 
              FROM #__shetrades_messaging_system AS m
              LEFT JOIN #__users AS su ON su.id = m.sender
              LEFT JOIN #__users AS ru ON ru.id = m.recipient 
              WHERE m.thread = "'.$thread1.'" OR m.thread = "'.$thread2.'" ';
    $db->setQuery($query);
    $result = $db->loadObjectList();
    return $result;

  }
  public static function getUnreadConversation($recipient,$thread){
    $thread1 = $thread;
    $thread = explode('_',$thread);
    // swap the digits as well
    $thread2 = $thread[1].'_'.$thread[0];
    $db = JFactory::getDbo();
    $db->getQuery(true);
    $query = 'SELECT m.id,m.thread,m.created_on,m.sender AS sender_id,m.recipient AS recipient_id,m.body AS message_body,m.received,su.name AS sender_name,ru.name AS recipient_name 
              FROM #__shetrades_messaging_system AS m
              LEFT JOIN #__users AS su ON su.id = m.sender
              LEFT JOIN #__users AS ru ON ru.id = m.recipient 
              WHERE m.received = "0" AND m.recipient = "'.$recipient.'" AND m.thread = "'.$thread1.'" OR m.thread = "'.$thread2.'" ';
    $db->setQuery($query);
    $result = $db->loadObjectList();
    return $result;

  }
  public static function getOutbox($sender){
      // sender column
      $db = JFactory::getDbo();
      $db->getQuery(true);
      $query = 'SELECT m.id,m.sender AS sender_id,m.recipient AS recipient_id,m.body AS message_body,m.received,m.created_on,su.name AS sender_name,ru.name AS recipient_name 
                FROM #__shetrades_messaging_system AS m
                LEFT JOIN #__users AS su ON su.id = m.sender
                LEFT JOIN #__users AS ru ON ru.id = m.recipient 
                WHERE m.sender = "'.$sender.'" AND m.received ="0"';
      $db->setQuery($query);
      $result = $db->loadObjectList();
      return $result;

  }
  /*
  * $data is an array
  * modified to include the discussion thread
  */
  public static function saveMessage($data){
      $db = JFactory::getDbo();
      $db->getQuery(true);
      // set the thread
      $thread = $data['sender'].'_'.$data['receiver'];
      $query = 'INSERT INTO #__shetrades_messaging_system (thread,sender,recipient,body) 
                VALUES ('.$db->quote($thread).','.$db->quote($data['sender']).','.$db->quote($data['receiver']).','.$db->quote($data['message']).')';
      $db->setQuery($query);
      if ($db->execute()){
          return true;
      }
      else {
          return false;
      }

  }
  /*
  * Function to check if a conversation thread exists between two users
  */
  public static function conversationExists($user1,$user2){
      $thread1 = $user1.'_'.$user2;
      // swap the digits as well
      $thread2 = $user2.'_'.$user1;
      $db = JFactory::getDbo();
      $db->getQuery(true);
      $query = 'SELECT id,thread FROM #__shetrades_messaging_system WHERE thread = "'.$thread1.'" OR thread = "'.$thread2.'"';
      $db->setQuery($query);
      $db->execute();
      $total = $db->getNumRows();
      if ($total > 0){
        return true;
      }
      else {
        return false;
      }
      
  }
  public static function markReceived($id){
      $db = JFactory::getDbo();
      $db->getQuery(true);
      $query = 'UPDATE #__shetrades_messaging_system SET received = 1 WHERE id ='.$id;
      $db->setQuery($query);
      $db->execute();
  }
}
