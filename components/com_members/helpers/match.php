<?php

/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
defined('_JEXEC') or die;

class MembersHelperMatch
{
	/*
	* A universal helper class to get messages and notifications, this can be used anywhere in the component
	* I don't know why I made the methods to be static!!
	* all gettters return an array of objects/items
	* 
	*/
	
	public static function getmatches($user_id){
        $db = JFactory::getDbo();
        $db->getQuery(true);
        $query_ = 'SELECT bf.id,bf.user_id,bf.biz_id,bi.name,bi.email FROM #__shetrades_biz_favorite AS bf 
                  LEFT JOIN #__shetrades_biz_info AS bi ON bi.id = bf.biz_id 
                  WHERE bf.user_id = "'.$user_id.'" AND bf.is_match = "1" AND bf.seen ="0"';
        $query = 'SELECT bf.id,bf.user_id,bf.biz_id,bi.name,bi.email,bi.created_by AS biz_owner_id,u.name AS biz_owner_name FROM #__shetrades_biz_favorite AS bf 
                  LEFT JOIN #__shetrades_biz_info AS bi ON bi.id = bf.biz_id
                  LEFT JOIN #__users AS u ON u.id = bi.created_by 
                  WHERE bf.user_id = "'.$user_id.'" AND bf.is_match = "1" AND bf.seen ="0"';
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return json_encode($result);
	}
	public static function markAsSeen($id){
		$db = JFactory::getDbo();
        $db->getQuery(true);
        $query = 'UPDATE #__shetrades_biz_favorite SET seen = "1" WHERE id = "'.$id.'"';
        $db->setQuery($query);
        if($db->execute()){
        	return true;
        }
        else {
        	return false;
        }
	}
}
