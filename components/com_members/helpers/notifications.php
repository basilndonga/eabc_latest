<?php

/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
defined('_JEXEC') or die;

class MembersHelperNotifications
{
	/*
	* A universal helper class to get messages and notifications, this can be used anywhere in the component
	* I don't know why I made the methods to be static!!
	* all gettters return an array of objects
	* 
	*/
	
	public static function getMessages($userid){
        $db = JFactory::getDbo();
        $db->getQuery(true);
        $query = 'SELECT * FROM #__shetrades_buyer_notification WHERE buyer_id = '.$db->quote($userid).' AND delivered = "0"';
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
	}
	public static function getBuyerMessages($userid){
		return self::getMessages($userid);
	}
	public static function getBizMessages($bizid){
		$db = JFactory::getDbo();
        $db->getQuery(true);
        $query = 'SELECT * FROM #__shetrades_biz_notification WHERE biz_id = '.$db->quote($bizid).' AND delivered = "0"';
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
	}
	public static function markBizMessageDelivered($id){
		$db = JFactory::getDbo();
		$db->getQuery(true);
		$query = 'UPDATE #__shetrades_biz_notification SET delivered = "1" WHERE id = '.$db->quote($id);
		$db->setQuery($query);
		if ($db->execute()){
			return true;
		}
		else {
			return false;
		}
	}
	public static function markBuyerMessageDelivered($id){
		$db = JFactory::getDbo();
		$db->getQuery(true);
		$query = 'UPDATE #__shetrades_buyer_notification SET delivered = "1" WHERE id = '.$db->quote($id);
		$db->setQuery($query);
		if ($db->execute()){
			return true;
		}
		else {
			return false;
		}
	}
}
