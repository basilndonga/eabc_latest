<?php

/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
defined('_JEXEC') or die;

class MembersHelperAccreditors
{
	/*
	* A universal class to get accreditors, this can be used anywhere in the component
	* Returns an array
	*/
	
	public static function getAccreditorsOld($ACC_USER_GROUP = 10){
        $accreditors = JAccess::getUsersByGroup($ACC_USER_GROUP);
        // this will return the user ids
        // print_r($accreditors);
        $accreditor_names = array();
        foreach($accreditors as $accreditor) {
            $user = JFactory::getUser($accreditor);
            $details = array(
                'userid' => $user->id,
                'name' => $user->name
            );
            array_push($accreditor_names, $details);
        }
        return $accreditor_names;

	}
    public static function getAccreditors(){
        $db = JFactory::getDbo();
        $db->getQuery(true);
        $query = 'SELECT id,org_name FROM #__shetrades_verifiers WHERE state = "1" ORDER BY org_name ASC';
        $db->setQuery($query);
        $accreditors = $db->loadObjectList();
        
        return $accreditors;

    }
    #
    # get a single verifier details given just the id
    #
    public static function getAccreditor($id){
        $db = JFactory::getDbo();
        $db->getQuery(true);
        $query = 'SELECT id,org_name,email,website, FROM #__shetrades_verifiers ORDER BY org_name ASC';
        $db->setQuery($query);
        $accreditor = $db->loadObject();
        
        return $accreditor;

    }
    public static function register($data = array()){
        /*
         Array
        (
            [organisation_name] => Kenya Chamber of Commerce
            [representative] => Levy Sabala
            [email] => emuthomi@gbc.co.ke
            [org_type] => International Organisation
            [country] => Afghanistan
            [website] => www.kenyachamberofcommerce.com
            [user_id] => 855
        )
        */
        $db = JFactory::getDbo();
        $db->getQuery(true);
        $query = 'INSERT INTO #__shetrades_verifiers (created_by,org_name,shetrades_rep,email,org_type,country,website) 
                VALUES ('.$db->quote($data['user_id']).','.$db->quote($data['organisation_name']).','.$db->quote($data['representative']).',
                '.$db->quote($data['email']).','.$db->quote($data['org_type']).','.$db->quote($data['country']).',
                '.$db->quote($data['website']).')';
        $db->setQuery($query);
        if ($db->execute()){
            self::notifyAdmins($data);
            return true;
        }
        else {
            return false;
        }

    }
    /*
    * Function to send email to verifier
    * accepst 2 params, id and message
    */
    public static function sendEmailtoVerifier($vid){
        $app = JFactory::getApplication();

        $mailfrom = $app->get('mailfrom');
        $fromname = $app->get('fromname');
        $sitename = $app->get('sitename');
        $verifier = self::getAccreditor($vid); // verifier details
        
        $email      = $verifier->email;
        $subject    = 'Verify a business on EABC';
        $name       = $verifier->org_name;
        $action_uri = JUri::base().'index.php?option=com_members_verifiers&view=verifier&id='.$vid;
        $message = 'A new business has been registered with you as a verifier on EABC.';

        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';

        $style = 'font-family:arial;font-size:12px;';           
        // Prepare email body
        $prefix = JText::sprintf('COM_CONTACT_ENQUIRY_TEXT', JUri::base());
        $body = '<h2 style="font-family:arial;font-size:13px;padding:5px 0px;border-bottom:solid 1px #777">Business Registration</h2>';
        $body .= '<p style="font-family:arial;font-size:12px">Dear EABC Verifier,</p>';
        $body .= '<p style="font-family:arial;font-size:12px">'.$message.',</p>';
        $body .= '<p style="font-family:arial;font-size:12px">To approve this business as verified by you, click the link below</p>';
        $body .= '<p style="font-family:arial;font-size:12px">'.$action_uri.'</p>';
        

        $mail = JFactory::getMailer();
        $mail->addRecipient($email);
        //$mail->addReplyTo(array($email, $name));
        $mail->setSender(array($mailfrom, $fromname));
        $mail->setSubject($sitename.': '.$subject);
        $mail->MsgHTML($body);
        $sent = $mail->Send();

        return $sent;

    }
    /*
    * Notify administrators that a new verifier has registered on the website
    * Sen email to administrators so they can accept
    */
    public static function notifyAdmins($details){
        /*
         Array
        (
            [organisation_name] => Kenya Chamber of Commerce
            [representative] => Levy Sabala
            [email] => emuthomi@gbc.co.ke
            [org_type] => International Organisation
            [country] => Afghanistan
            [website] => www.kenyachamberofcommerce.com
            [user_id] => 855
        )
        */
        $app = JFactory::getApplication();
        // fetch super admins who can receive system emails
        /*$db = JFactory::getDbo();
        $db->getQuery(true);
        $query = 'SELECT email FROM #__users WHERE sendEmail = 1';
        $db->setQuery($query);
        $admins = $db->loadObjectList();
        */

        $mailfrom = $app->get('mailfrom');
        $fromname = $app->get('fromname');
        $sitename = $app->get('sitename');

        $subject    = 'Accept a New Verifier on EABC';
        //$action_uri = JUri::base().'index.php?option=EABC&view=verifier&id='.$vid;
        $message = 'A new verifier has registered on EABC with the following details:';
        $verifier_details = '<table>';
        $verifier_details .= '<tr><td>Organisation: </td><td>'.$details->organisation_name.'</td></tr>';
        $verifier_details .= '<tr><td>Representative: </td><td>'.$details->representative.'</td></tr>';
        $verifier_details .= '<tr><td>Email: </td><td>'.$details->email.'</td></tr>';
        $verifier_details .= '<tr><td>Type of Organisation: </td><td>'.$details->org_type.'</td></tr>';
        $verifier_details .= '<tr><td>Country: </td><td></td>'.$details->country.'</tr>';
        $verifier_details .= '<tr><td>Website: </td><td>'.$details->website.'</td></tr>';
        $verifier_details .= '</table>';

        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';

        $style = 'font-family:arial;font-size:12px;';           
        // Prepare email body
        $body = '<h2 style="font-family:arial;font-size:13px;padding:5px 0px;border-bottom:solid 1px #777">Verifier Registration</h2>';
        $body .= '<p style="font-family:arial;font-size:12px">Dear EABC Admin,</p>';
        $body .= '<p style="font-family:arial;font-size:12px">'.$message.',</p>';
        $body .= $verifier_details;
        $body .= '<p style="font-family:arial;font-size:12px">Login in to the administrator to approve this registration</p>';
        
        // instatntiate mail object
        $mail = JFactory::getMailer();
        // send email to each admin
        /*foreach ($admins as $admin){
              $mail->addRecipient($admin->email);
              $mail->setSender(array($mailfrom, $fromname));
              $mail->setSubject($sitename.': '.$subject);
              $mail->MsgHTML($body);
              $sent = $mail->Send();            
        }*/
        
        $mail->addRecipient('womenandtrade@intracen.org');
        $mail->setSender(array($mailfrom, $fromname));
        $mail->setSubject($sitename.': '.$subject);
        $mail->MsgHTML($body);
        $sent = $mail->Send(); 

        return $sent;

    }
}
