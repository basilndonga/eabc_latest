<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');

/**
 * Shetrades model.
 */
class ShetradesModelBusinessinfoForm extends JModelForm
{

  var $_item = null;

  /**
   * Method to auto-populate the model state.
   *
   * Note. Calling getState in this method will result in recursion.
   *
   * @since    1.6
   */
  protected function populateState()
  {
    $app = JFactory::getApplication('com_shetrades');

    // Load state from the request userState on edit or from the passed variable on default
    if (JFactory::getApplication()->input->get('layout') == 'edit')
    {
      $id = JFactory::getApplication()->getUserState('com_shetrades.edit.businessinfo.id');
    }
    else
    {
      $id = JFactory::getApplication()->input->get('id');
      JFactory::getApplication()->setUserState('com_shetrades.edit.businessinfo.id', $id);
    }
    $this->setState('businessinfo.id', $id);

    // Load the parameters.
    $params       = $app->getParams();
    $params_array = $params->toArray();
    if (isset($params_array['item_id']))
    {
      $this->setState('businessinfo.id', $params_array['item_id']);
    }
    $this->setState('params', $params);

  }

  /**
   * Method to get an ojbect.
   *
   * @param    integer    The id of the object to get.
   *
   * @return    mixed    Object on success, false on failure.
   */
  public function &getData($id = null)
  {
    if ($this->_item === null)
    {
      $this->_item = false;

      if (empty($id))
      {
        $id = $this->getState('businessinfo.id');
      }

      // Get a level row instance.
      $table = $this->getTable();

      // Attempt to load the row.
      if ($table !== false && $table->load($id))
      {

        $user = JFactory::getUser();
        $id   = $table->id;
        if($id){
  $canEdit = $user->authorise('core.edit', 'com_shetrades.businessinfo.'.$id) || $user->authorise('core.create', 'com_shetrades.businessinfo.'.$id);
}
else{
  $canEdit = $user->authorise('core.edit', 'com_shetrades') || $user->authorise('core.create', 'com_shetrades');
}
        if (!$canEdit && $user->authorise('core.edit.own', 'com_shetrades.businessinfo.'.$id))
        {
          $canEdit = $user->id == $table->created_by;
        }

        if (!$canEdit)
        {
          throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'), 500);
        }

        // Check published state.
        if ($published = $this->getState('filter.published'))
        {
          if ($table->state != $published)
          {
            return $this->_item;
          }
        }

        // Convert the JTable to a clean JObject.
        $properties  = $table->getProperties(1);
        $this->_item = JArrayHelper::toObject($properties, 'JObject');
      }
    }

    return $this->_item;
  }

  public function getTable($type = 'Businessinfo', $prefix = 'ShetradesTable', $config = array())
  {
    $this->addTablePath(JPATH_ADMINISTRATOR . '/components/com_shetrades/tables');

    return JTable::getInstance($type, $prefix, $config);
  }

  public function getItemIdByAlias($alias)
  {
    $table = $this->getTable();

    $table->load(array( 'alias' => $alias ));

    return $table->id;
  }

  /**
   * Method to check in an item.
   *
   * @param    integer        The id of the row to check out.
   *
   * @return    boolean        True on success, false on failure.
   * @since    1.6
   */
  public function checkin($id = null)
  {
    // Get the id.
    $id = (!empty($id)) ? $id : (int) $this->getState('businessinfo.id');

    if ($id)
    {

      // Initialise the table
      $table = $this->getTable();

      // Attempt to check the row in.
      if (method_exists($table, 'checkin'))
      {
        if (!$table->checkin($id))
        {
          return false;
        }
      }
    }

    return true;
  }

  /**
   * Method to check out an item for editing.
   *
   * @param    integer        The id of the row to check out.
   *
   * @return    boolean        True on success, false on failure.
   * @since    1.6
   */
  public function checkout($id = null)
  {
    // Get the user id.
    $id = (!empty($id)) ? $id : (int) $this->getState('businessinfo.id');

    if ($id)
    {

      // Initialise the table
      $table = $this->getTable();

      // Get the current user object.
      $user = JFactory::getUser();

      // Attempt to check the row out.
      if (method_exists($table, 'checkout'))
      {
        if (!$table->checkout($user->get('id'), $id))
        {
          return false;
        }
      }
    }

    return true;
  }

  /**
   * Method to get the profile form.
   *
   * The base form is loaded from XML
   *
   * @param    array   $data     An optional array of data for the form to interogate.
   * @param    boolean $loadData True if the form is to load its own data (default case), false if not.
   *
   * @return    JForm    A JForm object on success, false on failure
   * @since    1.6
   */
  public function getForm($data = array(), $loadData = true)
  {
    // Get the form.
    $form = $this->loadForm('com_shetrades.businessinfo', 'businessinfoform', array(
      'control'   => 'jform',
      'load_data' => $loadData
    ));
    if (empty($form))
    {
      return false;
    }

    return $form;
  }

  /**
   * Method to get the data that should be injected in the form.
   *
   * @return    mixed    The data for the form.
   * @since    1.6
   */
  protected function loadFormData()
  {
    $data = JFactory::getApplication()->getUserState('com_shetrades.edit.businessinfo.data', array());
    if (empty($data))
    {
      $data = $this->getData();
    }
    

    return $data;
  }

  /**
   * Method to save the form data.
   *
   * @param    array        The form data.
   *
   * @return    mixed        The user id on success, false on failure.
   * @since    1.6
   */
  public function save($data)
  {
    $id    = (!empty($data['id'])) ? $data['id'] : (int) $this->getState('businessinfo.id');
    $state = (!empty($data['state'])) ? 1 : 0;
    $user  = JFactory::getUser();

    if ($id)
    {
      //Check the user can edit this item
      $authorised = $user->authorise('core.edit', 'com_shetrades.businessinfo.'.$id) || $authorised = $user->authorise('core.edit.own', 'com_shetrades.businessinfo.'.$id);
      if ($user->authorise('core.edit.state', 'com_shetrades.businessinfo.'.$id) !== true && $state == 1)
      { //The user cannot edit the state of the item.
        $data['state'] = 0;
      }
    }
    else
    {
      //Check the user can create new items in this section
      $authorised = $user->authorise('core.create', 'com_shetrades');
      if ($user->authorise('core.edit.state', 'com_shetrades.businessinfo.'.$id) !== true && $state == 1)
      { //The user cannot edit the state of the item.
        $data['state'] = 0;
      }
    }

    if ($authorised !== true)
    {
      throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'), 403);
    }

    $table = $this->getTable();
    if ($table->save($data) === true)
    {
      $db = JFactory::getDBO();
      $data_info = JFactory::getApplication()->input->post->getArray();
      $biz_buying = array(
            'biz_id' => $table->id,
            'type' => $db->quote('none'),
            'item' => $db->quote($data_info['jform']['company_bo'])
          );
      $biz_offering = array(
            'biz_id' => $table->id,
            'type' => $db->quote('none'),
            'item' => $db->quote($data_info['jform']['service_bo'])
          );
      $accreditation = array(
            'biz_id' => $db->quote($table->id),
            'accreditation_id' => $db->quote($data_info['jform']['member']),
            'accreditation_date' => $db->quote(date('Y-m-d H:i:s'))
          );
      $certificates = array(
          'biz_id' => $db->quote($table->id),
          'certificates' => $db->quote($data_info['jform']['certification'])
          );
      $companyreps = array(
          'biz_id' => $db->quote($table->id),
          'title' => $db->quote($data_info['jform']['title']),
          'company_rep' => $db->quote($data_info['jform']['company_rep']),
          'job_title' => $db->quote($data_info['jform']['job_title'])
          );

      $this->saveBizBuying($biz_buying);
      $this->saveBizoffering($biz_offering);
	  //Run several recurssions of the below function
	  for($i = 0; $i < count($accreditation['accreditation_id']); $i++){
		  $accredited_by = array(
		  	'biz_id' => $db->quote($table->id),
			'accreditation_id' => $db->quote($accreditation['accreditation_id'][$i]),
			'accreditation_date' => $db->quote(date('Y-m-d H:i:s'))
		  	);
			$this->saveBizAccreditations($accredited_by);
		  }
      //	  
	  // End
      $this->saveBizCerts($certificates);
      $this->saveBizRep($companyreps);
      //
/*	  echo '<pre>';
	  print_r($data_info);
	  print_r($biz_buying);
	  print_r($biz_offering);
	  print_r($accreditation);
	  print_r($certificates);
	  print_r($companyreps);
	  echo '</pre>';
	  exit();*/
      return $table->id;
    }
    else
    {
      return false;
    }

  }

  public function delete($data)
  {
    $id = (!empty($data['id'])) ? $data['id'] : (int) $this->getState('businessinfo.id');
    if (JFactory::getUser()->authorise('core.delete', 'com_shetrades.businessinfo.'.$id) !== true)
    {
      throw new Exception(403, JText::_('JERROR_ALERTNOAUTHOR'));
    }
    $table = $this->getTable();
    if ($table->delete($data['id']) === true)
    {
      return $id;
    }
    else
    {
      return false;
    }
  }

  public function getCanSave()
  {
    $table = $this->getTable();

    return $table !== false;
  }
  
  /* Hack by Mike*/
  public function saveBizBuying($data){
    $db = JFactory::getDBO();
    $query = $db->getQuery(true);
    $columns = array_keys($data);
    $values = array_values($data);
    $query = "INSERT INTO #__shetrades_biz_buying (" .implode(',', $columns). ") VALUES (" .implode(',',$values). ")";
    $db->setQuery($query);
    $db->execute();
  }

  public function saveBizOffering($data){
    $db = JFactory::getDBO();
    $query = $db->getQuery(true);
    $columns = array_keys($data);
    $values = array_values($data);
    $query = "INSERT INTO #__shetrades_biz_offering (" .implode(',', $columns). ") VALUES (" .implode(',',$values). ")";
    $db->setQuery($query);
    $db->execute();
  }

  public function saveBizAccreditations($data){
    $db = JFactory::getDBO();
    $query = $db->getQuery(true);
    $columns = array_keys($data);
    $values = array_values($data);
    $query = "INSERT INTO #__shetrades_accreditations (" .implode(',', $columns). ") VALUES (" .implode(',',$values). ")";
    $db->setQuery($query);
    $db->execute();
  }

  public function saveBizCerts($data){
    $db = JFactory::getDBO();
    $query = $db->getQuery(true);
    $columns = array_keys($data);
    $values = array_values($data);
    $query = "INSERT INTO #__shetrades_biz_certification (" .implode(',', $columns). ") VALUES (" .implode(',',$values). ")";
    $db->setQuery($query);
    $db->execute();
  }

  public function saveBizRep($data){
    $db = JFactory::getDBO();
    $query = $db->getQuery(true);
    $columns = array_keys($data);
    $values = array_values($data);
    $query = "INSERT INTO #__shetrades_biz_rep (" .implode(',', $columns). ") VALUES (" .implode(',',$values). ")";
    $db->setQuery($query);
    $db->execute();
  }
}