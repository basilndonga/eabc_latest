<?php

/**
 * @version     1.0.0
 * @package     com_members
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
defined('_JEXEC') or die;
error_reporting(1);
jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Members records.
 */
class MembersModelBusinessinfos extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param    array    An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				                'id', 'a.id',
                'ordering', 'a.ordering',
                'state', 'a.state',
                'created_by', 'a.created_by',
                'name', 'a.name',
                'email', 'a.email',
                'phone', 'a.phone',
                'no_of_female', 'a.no_of_female',
                'location', 'a.location',
                'status', 'a.status',
                'viewed', 'a.viewed',
                'created_on', 'a.created_on',
                'modified_on', 'a.modified_on',
                'country', 'a.country',
                'city', 'a.city',
                'headed_by_woman', 'a.headed_by_woman',
                'annual_revenue_usd','a.annual_revenue_usd',
                'annual_values_exports','a.annual_values_exports',
                'year_of_exports', 'a.year_of_exports'

			);
		}
		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// List state information
		$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));
		$this->setState('list.limit', $limit);

		$limitstart = $app->input->getInt('limitstart', 0);
		$this->setState('list.start', $limitstart);

		if ($list = $app->getUserStateFromRequest($this->context . '.list', 'list', array(), 'array'))
		{
			foreach ($list as $name => $value)
			{
				// Extra validations
				switch ($name)
				{
					case 'fullordering':
						$orderingParts = explode(' ', $value);

						if (count($orderingParts) >= 2)
						{
							// Latest part will be considered the direction
							$fullDirection = end($orderingParts);

							if (in_array(strtoupper($fullDirection), array( 'ASC', 'DESC', '' )))
							{
								$this->setState('list.direction', $fullDirection);
							}

							unset($orderingParts[ count($orderingParts) - 1 ]);

							// The rest will be the ordering
							$fullOrdering = implode(' ', $orderingParts);

							if (in_array($fullOrdering, $this->filter_fields))
							{
								$this->setState('list.ordering', $fullOrdering);
							}
						}
						else
						{
							$this->setState('list.ordering', $ordering);
							$this->setState('list.direction', $direction);
						}
						break;

					case 'ordering':
						if (!in_array($value, $this->filter_fields))
						{
							$value = $ordering;
						}
						break;

					case 'direction':
						if (!in_array(strtoupper($value), array( 'ASC', 'DESC', '' )))
						{
							$value = $direction;
						}
						break;

					case 'limit':
						$limit = $value;
						break;

					// Just to keep the default case
					default:
						$value = $value;
						break;
				}

				$this->setState('list.' . $name, $value);
			}
		}

		// Receive & set filters
		if ($filters = $app->getUserStateFromRequest($this->context . '.filter', 'filter', array(), 'array'))
		{
			foreach ($filters as $name => $value)
			{
				$this->setState('filter.' . $name, $value);
			}
		}

		$ordering = $app->input->get('filter_order');
		if (!empty($ordering))
		{
			$list             = $app->getUserState($this->context . '.list');
			$list['ordering'] = $app->input->get('filter_order');
			$app->setUserState($this->context . '.list', $list);
		}

		$orderingDirection = $app->input->get('filter_order_Dir');
		if (!empty($orderingDirection))
		{
			$list              = $app->getUserState($this->context . '.list');
			$list['direction'] = $app->input->get('filter_order_Dir');
			$app->setUserState($this->context . '.list', $list);
		}

		$list = $app->getUserState($this->context . '.list');

		if (empty($list['ordering']))
{
	$list['ordering'] = 'ordering';
}

if (empty($list['direction']))
{
	$list['direction'] = 'asc';
}

		if (isset($list['ordering']))
		{
			$this->setState('list.ordering', $list['ordering']);
		}
		if (isset($list['direction']))
		{
			$this->setState('list.direction', $list['direction']);
		}

	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return    JDatabaseQuery
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query
			->select(
				$this->getState(
					'list.select', 'DISTINCT a.*'
				)
			);

		$query->from('`#__shetrades_biz_info` AS a');
		$query->where('headed_by_woman = 1');
		//$query->where('perc_owned_by_woman >= 30');

		
	    // Join over the users for the checked out user.
	    $query->select('uc.name AS editor');
	    $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

	    // join the accreditations table
        
	    //$query->select('acc.accreditation_id AS accreditor');
	    //$query->join('LEFT', '#__shetrades_accreditations AS acc ON acc.biz_id=a.id');

	    // join the countries table

	    $query->select('co.nicename AS country');
	    $query->join('LEFT', '#__shetrades_country AS co ON co.id=a.country');
	    
		// Join over the created by field 'created_by'
		$query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');

			
		if (!JFactory::getUser()->authorise('core.edit.state', 'com_members'))
		{
			$query->where('a.state = 1');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.name LIKE '.$search.'  OR  a.location LIKE '.$search.' )');
			}
		}

		//Check if person is search from homepage
		$search_home = JFactory::getApplication()->input->post->getArray();
		$search_term = $search_home['search'];

		if (!empty($search_term))
		{
				$search_term = $db->Quote('%' . $db->escape($search_term, true) . '%');
				$query->where('( a.name LIKE '.$search_term.'  OR  a.location LIKE '.$search_term.' )');
		}
		
		//Check if its advanced search
		$search_advanced = JFactory::getApplication()->input->post->getArray();
	        $owner = JFactory::getApplication()->input->get('owner');
	        if (!empty($owner)){
	            $query->where('a.created_by = "'.$owner.'" ');
	        }
		// print_r($search_advanced);

		if(!empty($search_advanced['advancedsearch'])){

			//If coutnry is enabled
			if($search_advanced['country'] != ''){
				$query->where('a.country = "'.$search_advanced['country'].'" ');
			}

			// If verified or not
			if($search_advanced['verification'] != ''){
				// $query->where('a.country = "'.$search_advanced['country'].'" ');
			}

			// Selling this product
			if($search_advanced['product'] != ''){
				// $query->where('a.country = "'.$search_advanced['country'].'" ');
			}

			// Selling this service
			if($search_advanced['service'] != ''){
				// $query->where('a.country = "'.$search_advanced['country'].'" ');
			}

			// Buying this product
			if($search_advanced['product_buy'] != ''){
				// $query->where('a.country = "'.$search_advanced['country'].'" ');
			}

			// Buying this service
			if($search_advanced['service_buy'] != ''){
				// $query->where('a.country = "'.$search_advanced['country'].'" ');
			}

			// Buying this service
			if($search_advanced['service_buy'] != ''){
				// $query->where('a.country = "'.$search_advanced['country'].'" ');
			}

			// Percentage owned by women
			$query->where('a.perc_owned_by_woman >= "'.$search_advanced['percentownedwomen'].'" ');

			// Employees count
			$query->where('a.no_of_employees >= "'.$search_advanced['empslider'].'" ');
			// }

		}
		// start custom filters
        /*
		$products = $this->getState('filter.products');
		if (!empty($products))
			$query->where('a.products = '.$products);
        */
		// $exports_exp = $this->getState('filter.year_of_exports');
		// if (!empty($exports_exp))
		// 	$query->where('a.year_of_exports = '.$exports_exp);
        
		// $revenue = $this->getState('filter.annual_revenue_usd');
		// if (!empty($revenue)){
		// 	$query->where('a.annual_revenue_usd >= "'.$revenue.'" ');
		// }
		// $perc_women = $this->getState('filter.perc_owned_by_woman');
		// if (!empty($perc_women)){
		// 	$query->where('a.perc_owned_by_woman >= '.$perc_women);
		// }	

  //       $employees = $this->getState('filter.no_of_employees');
		// if (!empty($employees)){
		// 	$query->where('a.no_of_employees >= "'.$employees.'" ');
		// }						

		// $exports = $this->getState('filter.annual_values_exports');
		// if (!empty($exports)){
		// 	$query->where('a.annual_values_exports >= "'.$exports.'" ');
		// }
			
		// /**/
		// $headed_by_woman = $this->getState('filter.headed_by_woman');
		// if (!empty($headed_by_woman)){
		// 	$query->where('a.headed_by_woman = "'.$headed_by_woman.'" ');
		// }
			
		 //$country = $this->getState('filter.country');
		 //if (!empty($country)){
		 //	//$query->where('a.country = "'.$country.'" ');
		 //	$query->where('a.country = "'.$country.'" ');
		 //}
		// end custom filters
		

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');
		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		
		//echo $query->dump();
		//print_r($query->__toString());
		return $query;
	}

	public function getItems()
	{
		$items = parent::getItems();	
		$newitems = array();
		foreach ($items as $item){
		    // check if logo file exists 	
		    if ($item->logo != ''){
			$logopath = JPATH_BASE.'/images/components/com_members/'.$item->logo;
			if (file_exists($logopath)){
                        //$logo = $logopath; 
                        $logo = JURI::base().'images/components/com_members/'.$item->logo;  
			}
			else {
                            $logo = 'none';
			}
		    }
		    else {
			$logo = 'none';
		    }
		    $item->logo = $logo;
		    //print_r($item);
		    //var_dump((array) $item);
		    $item_array = (array)$item;
		    $db = JFactory::getDbo();
		    $query = $db->getQuery(true);
		    $query = 'SELECT acc.accreditation_id,u.name FROM #__shetrades_accreditations AS acc LEFT JOIN #__users AS u ON u.id = acc.accreditation_id WHERE acc.biz_id = '.$item->id;
		    $query2s = 'SELECT bb.item,bb.type,bb.biz_id,s.name 
		               FROM #__shetrades_biz_buying AS bb 
		               LEFT JOIN #__shetrades_service AS s ON s.id = bb.item 
		               WHERE bb.type = "service" AND bb.biz_id = '.$item->id;
		    $query2p = 'SELECT bb.item,bb.type,bb.biz_id,s.name 
		               FROM #__shetrades_biz_buying AS bb 
		               LEFT JOIN #__shetrades_service AS s ON s.id = bb.item 
		               WHERE bb.type = "product" AND bb.biz_id = '.$item->id;

		    $query3s = 'SELECT bo.item,bo.type,bo.biz_id,s.name 
		               FROM #__shetrades_biz_offering AS bo 
		               LEFT JOIN #__shetrades_service AS s ON s.id = bo.item 
		               WHERE bo.type = "service" AND bo.biz_id ='.$item->id;

            $query3p = 'SELECT bo.item,bo.type,bo.biz_id,p.name
		               FROM #__shetrades_biz_offering AS bo 
		               INNER JOIN #__shetrades_product AS p ON p.id = bo.item
		               WHERE bo.biz_id = "'.$item->id.'" AND bo.type = "product"';

		    $query4 = 'SELECT certificates FROM #__shetrades_biz_certification WHERE biz_id ='.$item->id;
		    $query5 = 'SELECT title,company_rep,job_title FROM #__shetrades_biz_rep WHERE biz_id ='.$item->id;
		    $query6 = 'SELECT profile,product_services_img,facilities_img FROM #__shetrades_biz_media WHERE biz_id ='.$item->id;
		    // accreditors
		    $db->setQuery($query);
		    $result = $db->loadObjectList();
		    $item_array['accreditors'] = array();
		    foreach($result as $row){
		       array_push($item_array['accreditors'],$row->name);
		    }
		    // buying services
		    $db->setQuery($query2s);
		    $result = $db->loadObjectList();
		    $item_array['services_buying'] = array();
		    foreach($result as $row){
		       array_push($item_array['services_buying'],$row->name);
		    }
		    // buying products
		    $db->setQuery($query2p);
		    $result = $db->loadObjectList();
		    $item_array['products_buying'] = array();
		    foreach($result as $row){
		       array_push($item_array['products_buying'],$row->name);
		    }
		    // offering services
		    $db->setQuery($query3s);
		    $result = $db->loadObjectList();
		    $item_array['services_offering'] = array();
		    foreach($result as $row){
		       array_push($item_array['services_offering'],$row->name);
		    }
		    // offering products
		    $db->setQuery($query3p);
		    $result = $db->loadObjectList();
		    $item_array['products_offering'] = array();
		    foreach($result as $row){
		       array_push($item_array['products_offering'],$row->name);
		    	//$j = array('type'=>$row->type,'biz_id'=>$row->biz_id,'name'=>$row->name,'code'=>$row->product_code,'product_id'=>$row->id);
		    	//array_push($item_array['products_offering'],json_encode($j));
		    }
		    //
		    $db->setQuery($query4);
		    $result = $db->loadObjectList();
		    $item_array['certification'] = array();
		    foreach($result as $row){
		       array_push($item_array['certification'],$row->certificates);
		    }
		    //
		    $db->setQuery($query5);
		    $result = $db->loadObjectList();
		    $item_array['rep'] = array();
		    foreach($result as $row){
		       array_push($item_array['rep'],array('name'=>$row->company_rep,'title'=>$row->title));
		    }
		    //
		    $db->setQuery($query6);
		    $result = $db->loadObjectList();
		    $item_array['images'] = array();
		    foreach($result as $row){
		       array_push($item_array['images'],array('profile'=>$row->profile,'product_services'=>$row->product_services_img,'facilities'=>$row->facilities_img));
		    }
		   
		    $object = (object)$item_array;
		    array_push($newitems,$object);
		    /*
		    echo '<pre>';
		    print_r($item_array);
		    //print_r($object);
		    echo '</pre>';
		     */
		    	  
		}
		//return $items;	
		return $newitems;	
	}

	/**
	 * Overrides the default function to check Date fields format, identified by
	 * "_dateformat" suffix, and erases the field if it's not correct.
	 */
	protected function loadFormData()
	{
		$app              = JFactory::getApplication();
		$filters          = $app->getUserState($this->context . '.filter', array());
		$error_dateformat = false;
		foreach ($filters as $key => $value)
		{
			if (strpos($key, '_dateformat') && !empty($value) && !$this->isValidDate($value))
			{
				$filters[ $key ]  = '';
				$error_dateformat = true;
			}
		}
		if ($error_dateformat)
		{
			$app->enqueueMessage(JText::_("COM_SHETRADES_SEARCH_FILTER_DATE_FORMAT"), "warning");
			$app->setUserState($this->context . '.filter', $filters);
		}

		return parent::loadFormData();
	}

	/**
	 * Checks if a given date is valid and in an specified format (YYYY-MM-DD)
	 *
	 * @param string Contains the date to be checked
	 *
	 */
	private function isValidDate($date)
	{
		return preg_match("/^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])$/", $date) && date_create($date);
	}

	/**
	 * Get the filter form
	 *
	 * @return  JForm/false  the JForm object or false
	 *
	 */
	public function getFilterForm()
	{
		$form = null;

		// Try to locate the filter form automatically. Example: ContentModelArticles => "filter_articles"
		if (empty($this->filterFormName))
		{
			$classNameParts = explode('Model', get_called_class());

			if (count($classNameParts) == 2)
			{
				$this->filterFormName = 'filter_' . strtolower($classNameParts[1]);
			}
		}

		if (!empty($this->filterFormName))
		{
			// Get the form.
			$form = new JForm($this->filterFormName);
			$form->loadFile(dirname(__FILE__) . DS . 'forms' . DS . $this->filterFormName . '.xml');
			$filter_data = JFactory::getApplication()->getUserState($this->context, new stdClass);
			$form->bind($filter_data);
		}

		return $form;
	}

	/**
	 * Function to get the active filters
	 */
	public function getActiveFilters()
	{
		$activeFilters = false;

		if (!empty($this->filter_fields))
		{
			for ($i = 0; $i < count($this->filter_fields); $i++)
			{
				$filterName = 'filter.' . $this->filter_fields[ $i ];

				if (property_exists($this->state, $filterName) && (!empty($this->state->{$filterName}) || is_numeric($this->state->{$filterName})))
				{
					$activeFilters = true;
				}
			}
		}

		return $activeFilters;
	}

	private function getParameterFromRequest($paramName, $default = null, $type = 'string')
	{
		$variables = explode('.', $paramName);
		$input     = JFactory::getApplication()->input;

		$nullFound = false;
		if (count($variables) > 1)
		{
			$data = $input->get($variables[0], null, 'ARRAY');
		}
		else
		{
			$data = $input->get($variables[0], null, $type);
		}
		for ($i = 1; $i < count($variables) && !$nullFound; $i++)
		{
			if (isset($data[ $variables[ $i ] ]))
			{
				$data = $data[ $variables[ $i ] ];
			}
			else
			{
				$nullFound = true;
			}
		}

		return ($nullFound) ? $default : JFilterInput::getInstance()->clean($data, $type);

	}

}
