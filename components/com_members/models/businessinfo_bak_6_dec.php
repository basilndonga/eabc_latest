<?php

/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

/**
 * Shetrades model.
 */
class ShetradesModelBusinessinfo extends JModelItem
{

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since    1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('com_shetrades');

		// Load state from the request userState on edit or from the passed variable on default
		if (JFactory::getApplication()->input->get('layout') == 'edit')
		{
			$id = JFactory::getApplication()->getUserState('com_shetrades.edit.businessinfo.id');
		}
		else
		{
			$id = JFactory::getApplication()->input->get('id');
			JFactory::getApplication()->setUserState('com_shetrades.edit.businessinfo.id', $id);
		}
		$this->setState('businessinfo.id', $id);

		// Load the parameters.
		$params       = $app->getParams();
		$params_array = $params->toArray();
		if (isset($params_array['item_id']))
		{
			$this->setState('businessinfo.id', $params_array['item_id']);
		}
		$this->setState('params', $params);
	}

	/**
	 * Method to get an ojbect.
	 *
	 * @param    integer    The id of the object to get.
	 *
	 * @return    mixed    Object on success, false on failure.
	 */
	public function &getData($id = null)
	{
		if ($this->_item === null)
		{
			$this->_item = false;

			if (empty($id))
			{
				$id = $this->getState('businessinfo.id');
			}

			// Get a level row instance.
			$table = $this->getTable();

			// Attempt to load the row.
			if ($table->load($id))
			{
				// Check published state.
				if ($published = $this->getState('filter.published'))
				{
					if ($table->state != $published)
					{
						return $this->_item;
					}
				}

				// Convert the JTable to a clean JObject.
				$properties  = $table->getProperties(1);
				$this->_item = JArrayHelper::toObject($properties, 'JObject');
			}
		}

		
		if ( isset($this->_item->created_by) ) {
			$this->_item->created_by_name = JFactory::getUser($this->_item->created_by)->name;
		}

		return $this->_item;
	}

	public function getTable($type = 'Businessinfo', $prefix = 'ShetradesTable', $config = array())
	{
		$this->addTablePath(JPATH_ADMINISTRATOR . '/components/com_shetrades/tables');

		return JTable::getInstance($type, $prefix, $config);
	}

	public function getItemIdByAlias($alias)
	{
		$table = $this->getTable();

		$table->load(array( 'alias' => $alias ));

		return $table->id;
	}

	/**
	 * Method to check in an item.
	 *
	 * @param    integer        The id of the row to check out.
	 *
	 * @return    boolean        True on success, false on failure.
	 * @since    1.6
	 */
	public function checkin($id = null)
	{
		// Get the id.
		$id = (!empty($id)) ? $id : (int) $this->getState('businessinfo.id');

		if ($id)
		{

			// Initialise the table
			$table = $this->getTable();

			// Attempt to check the row in.
			if (method_exists($table, 'checkin'))
			{
				if (!$table->checkin($id))
				{
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Method to check out an item for editing.
	 *
	 * @param    integer        The id of the row to check out.
	 *
	 * @return    boolean        True on success, false on failure.
	 * @since    1.6
	 */
	public function checkout($id = null)
	{
		// Get the user id.
		$id = (!empty($id)) ? $id : (int) $this->getState('businessinfo.id');

		if ($id)
		{

			// Initialise the table
			$table = $this->getTable();

			// Get the current user object.
			$user = JFactory::getUser();

			// Attempt to check the row out.
			if (method_exists($table, 'checkout'))
			{
				if (!$table->checkout($user->get('id'), $id))
				{
					return false;
				}
			}
		}

		return true;
	}

	public function getCategoryName($id)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select('title')
			->from('#__categories')
			->where('id = ' . $id);
		$db->setQuery($query);

		return $db->loadObject();
	}

	public function publish($id, $state)
	{
		$table = $this->getTable();
		$table->load($id);
		$table->state = $state;

		return $table->store();
	}

	public function delete($id)
	{
		$table = $this->getTable();

		return $table->delete($id);
	}
	
	/*
	*
	*
	*
	*/
	public function remoteUpdate($id){
        // do an update from the app
        return true;
	}

	/*
	* The biz owner has agreed to connect with the person who liked their biz, let's complete the match
	* $id is the key in #__shetrades_biz_favorite table
	* our task is to update the row and set is_match to one!
	*/

	public function match($id,$bizid,$userid){
		$biz_info = $this->getData($bizid);
		$match = "1";
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query = "UPDATE #__shetrades_biz_favorite SET is_match = ".$db->quote($match)." WHERE id = ".$db->quote($id)." ";
		$db->setQuery($query);
		if ($db->execute()){
			// successfully marked it as a match, now we need to log the notif and maybe notify the user
			$message = 'You have been matched with '.$biz_info->name;
        	$params = '{"matched_biz":"'.$bizid.'","name":"'.$biz_info->name.'"}';
        	$query = 'INSERT INTO #__shetrades_buyer_notification (buyer_id,message,type,params) VALUES("'.$userid.'","'.$message.'","match",'.$db->quote($params).')';
        	$db->setQuery($query);
        	$db->execute();
        	return true;
		}
		else {
			return false;
		}
	}

	/*
	* send a notification to someone, this can be the biz owner or buyer
	* 
	*/

	public function notify(){	
        return true;
	}
	public function notifyBizOwner($email,$data){	
		$app = JFactory::getApplication();

		$mailfrom = $app->get('mailfrom');
		$fromname = $app->get('fromname');
		$sitename = $app->get('sitename');
		
		$email		= JstringPunycode::emailToPunycode($email);
		$subject	= $data['subject'];
		$message	= $data['message'];
		$name       = $data['name'];
		$callback   = $data['action_uri'];

		$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';

		$style = 'font-family:arial;font-size:12px;';			
		// Prepare email body
		$prefix = JText::sprintf('COM_CONTACT_ENQUIRY_TEXT', JUri::base());
		$body = '<h2 style="font-family:arial;font-size:13px;padding:5px 0px;border-bottom:solid 1px #777">Contact Enquiry</h2>';
		$body .= '<p style="font-family:arial;font-size:12px">Dear '.$name.',</p>';
		$body .= '<p style="font-family:arial;font-size:12px">'.$message.',</p>';
		$body .= '<p style="font-family:arial;font-size:12px">To connect with the buyer, click on the link below</p>';
		$body .= '<p style="font-family:arial;font-size:12px">'.$callback.'</p>';
		

		$mail = JFactory::getMailer();
		$mail->addRecipient($email);
		//$mail->addReplyTo(array($email, $name));
		$mail->setSender(array($mailfrom, $fromname));
		$mail->setSubject($sitename.': '.$subject);
		$mail->MsgHTML($body);
		$sent = $mail->Send();

		return $sent;
	}
	public function like($bizid,$userid){
		// get the biz info 
		$biz_info = $this->getData($bizid);
		$biz_owner = $biz_info->created_by;
		$biz_owner_user = JFactory::getUser($biz_owner);
		$biz_owner_email = $biz_owner_user->email;
		// get the likers info
		$user_info = JFactory::getUser($userid);
        $db = $this->getDBO();
	    $query = $db->getQuery(true);
	    $match = '0';
	    // log the like/fav
		$query = 'INSERT INTO #__shetrades_biz_favorite(biz_id,user_id,is_match) VALUES ("'.$bizid.'","'.$userid.'","'.$match.'")';
	    $db->setQuery($query);
        if ($db->execute()){
        	$favorite_id = $db->insertid();
        	$action_uri = JUri::base().'index.php?option=com_shetrades&task=api.biz&action=match&favorite_id='.$favorite_id.'&biz_id='.$bizid.'&user_id='.$userid;
        	// log the notification and, notify the biz owner of the like
        	$message = $user_info->name.' liked your business -'.$biz_info->name;
        	$params = '{"liked_by":"'.$userid.'","biz_name":"'.$biz_info->name.'","name":"'.$user_info->name.'","email":"'.$user_info->email.'","action_uri":"'.$action_uri.'"}';
        	$query = 'INSERT INTO #__shetrades_biz_notification (biz_id,message,type,params) VALUES("'.$bizid.'","'.$message.'","like",'.$db->quote($params).')';
        	$db->setQuery($query);
        	if($db->execute()){
        		// fire an email to biz owner
        		//echo $biz_owner_email;
        		$email_message = array(
        			'subject' => 'A new like on SheTrades',
        			'name' => $biz_owner_user->name,
        			'message' => $message,
        			'action_uri' => $action_uri 

        		);
        		//echo $email_message['action_uri'];
        		//$this->notifyBizOwner($email,$email_message);
        	}
        	// return true whether the notification is successful or not, we don't care at this moment
        	return true;
        }
        else{
        	// could not log the fav
        	return false;
        }       
	}
	
	public function remoteSave($data){
		$sqlinfo = array(
            'name' => $data['name'],
            'country' => $data['country'],
            'city' => $data['city'],
            'company_desc' => $data['company_desc'],
            'company_difference' => $data['company_difference'],           
            //'isaccredited' => $data['isaccredited'],
            //'accreditations' => $data['accreditations'], // array
            'ismemberofanother' => $data['ismemberofanother'],
            //'offeringproducts' => $data['offeringproducts'], // array
            //'offeringservices' => $data['offeringservices'], // array
            //'buyingproducts' => $data['buyingproducts'], // array
            //'buyingservices' => $data['buyingservices'], // array
            'headed_by_woman' => $data['headed_by_woman'],
            'perc_owned_by_woman' => $data['women_owned_percent'],
            'no_of_employees' => $data['no_of_employees'],
            'no_of_female' => $data['no_of_female'],
            //'rep_name' => $data['rep_name'],
            //'rep_jobtitle' => $data['rep_jobtitle'],
            //'rep_title' => $data['rep_title'],
            //'certifications' => $data['certifications'], // goes into another table
            'annual_revenue_usd' => $data['annual_sales'],
            'annual_values_exports' => $data['annual_exports'],
            'year_of_exports' => $data['years_exporting'],
            'export_countries' => $data['countries_exporting_to'],
            'primary_customers' => $data['primary_customers'],
            'exports_from' => $data['exports_from'],
            'lead_times' => $data['lead_time'],
            'delivery_terms' => $data['shipment_terms'],
            'www' => $data['website'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'fax' => $data['fax'],
            'po_box' => $data['po_box'],
            'location' => $data['office_locations'],
            'facebook' => $data['facebook'],
            'twitter' => $data['twitter'],
            'linkedin' => $data['linkedin'],
            //'googleplus' => $data['googleplus'],
            'created_by' => $data['user_id']
            );
		/*
		[accreditations] => Array
		        (
		            [0] => 858
		            [1] => 859
		        )
		*/
        $certifications = $data['certifications'];
        $rep_name = $data['rep_name'];
        $rep_jobtitle = $data['rep_jobtitle'];
        $rep_title = $data['rep_title'];

        $accreditations = $data['accreditations'];
        //$accreditations = implode(':',array_values($accreditations)); // turned into a string e.g 856,878,890

        $offeringproducts = $data['offeringproducts'];
        //$offeringproducts = implode(',',array_values($offeringproducts));

        $offeringservices = $data['offeringservices'];
        //$offeringservices = implode(',',array_values($offeringservices));

        $buyingproducts = $data['buyingproducts'];
        //$buyingproducts = implode(',',array_values($buyingproducts));

        $buyingservices = $data['buyingservices'];
        //$buyingservices = implode(',',array_values($buyingservices));

        $table = $this->getTable();
		if ($table->save($sqlinfo) === true)
		{
			$bizid = $table->id;

			$db = $this->getDBO();
	        $query = $db->getQuery(true);
	        $query = 'UPDATE #__shetrades_biz_info SET created_by = "'.$data['user_id'].'" WHERE id = "'.$bizid.'"';
	        $db->setQuery($query);
	        $db->execute();
			//
			$cert_array = array(
				'biz_id' => $bizid,
				'certificates' => $certifications
			);
			$this->saveBizCerts($cert_array);
			$bizrep_array = array(
				'biz_id' => $bizid,
				'company_rep' => $rep_name,
				'job_title' => $rep_jobtitle,
				'title' => $rep_title
			);
			$this->saveBizRep($bizrep_array);
			foreach ($accreditations as $key => $value) {
	            $acc_array = array(
	        	    'biz_id' => $bizid, 
	        	    'accreditation_id' => $value
	            );
	            $this->saveBizAccreditations($acc_array);
	        }
			
	        //
	        foreach ($offeringproducts as $key => $value) {
	            $offer_prods = array(
	                'biz_id' => $bizid,
	                'type' => 'product',
	                'item' => $value
	            );
	            $this->saveBizOffering($offer_prods);
	        }
	        //
	        foreach ($offeringservices as $key => $value) {
	            $offer_serv = array(
	                'biz_id' => $bizid,
	                'type' => 'service',
	                'item' => $value
	            );
	            $this->saveBizOffering($offer_serv);
	        }
	        //
	        foreach ($buyingproducts as $key => $value) {
	            $buy_prods = array(
	                'biz_id' => $bizid,
	                'type' => 'product',
	                'item' => $value
	            );
	            $this->saveBizBuying($buy_prods);
	        }
	        //
	        foreach ($buyingservices as $key => $value) {
	            $buy_serv = array(
	                'biz_id' => $bizid,
	                'type' => 'service',
	                'item' => $value
	            );
	            $this->saveBizBuying($buy_serv);
	        }
	        //$this->sendEmailtoBizOwner($data['email']);
	        return $table->id;


		}
		else
		{
			return false;
		}

	}	
	public function saveBizBuying($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);
	    $columns = array_keys($data);
		$values = array_values($data);
		/*
		$query->insert($db->quoteName('#__shetrades_biz_buying'))
			  ->columns($db->quoteName(implode(',', $columns)))
			  ->values(implode(',', $values));
		*/
		$query = 'INSERT INTO #__shetrades_biz_buying(' .implode(',', $columns). ') VALUES ("' .implode('","',$values). '")';
	    $db->setQuery($query);
        $db->execute();

	}
	
	public function saveBizOffering($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);
	    $columns = array_keys($data);
		$values = array_values($data);
		$query = 'INSERT INTO #__shetrades_biz_offering (' .implode(',', $columns). ') VALUES ("' .implode('","',$values). '")';
	    $db->setQuery($query);
        $db->execute();
		
	}
	
	public function saveBizCerts($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);
	    $columns = array_keys($data);
		$values = array_values($data);
		$query = 'INSERT INTO #__shetrades_biz_certification (' .implode(',', $columns). ') VALUES ("' .implode('","',$values). '")';
	    $db->setQuery($query);
        $db->execute();

	}
	
	public function saveBizAccreditations($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);
	    $columns = array_keys($data);
		$values = array_values($data);
	
		$query = 'INSERT INTO #__shetrades_accreditations (' .implode(',', $columns). ') VALUES ("' .implode('","',$values). '")';
	    $db->setQuery($query);
        $db->execute();
        foreach($values as $verifier_id){
        	// we only have thier ids
        	//ShetradesHelperAccreditors::sendEmailtoVerifier($verifier_id);
        }

	}
	public function saveBizRep($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);
	    $columns = array_keys($data);
		$values = array_values($data);
	
		$query = 'INSERT INTO #__shetrades_biz_rep (' .implode(',', $columns). ') VALUES ("' .implode('","',$values). '")';
	    $db->setQuery($query);
        $db->execute();

	}
	public function uploadimg($file, $id, $column){
	    //Check file
	    $filename = JFile::makeSafe($file['name']);
	    $src = $file['tmp_name']; // Src
	    $location = JPATH_BASE . '/images/components/com_shetrades/';
	    $dest = JPATH_BASE . '/images/components/com_shetrades/' .$filename; //Destination
	    $ext =  JFile::getExt($file['name']);
	    $newname = $column. '_' .$id. '_' .time(). '.' .$ext;
	    JFile::upload($src, $dest); // Upload file
	    rename($dest, $location . $newname);
	    //Update the biz info table
	    $db = JFactory::getDBO();
	    $query = $db->getQuery(true);
	    if($column == 'logo'){
	      $query = "UPDATE #__shetrades_biz_info SET " .$column. " = '" .$newname. "' WHERE id = '" .$id. "'";
	    }
	    else{
	      $query = "UPDATE #__shetrades_biz_media SET " .$column. " = '" .$newname. "' WHERE biz_id = '" .$id. "'";
	    }
	    $db->setQuery($query);
	    $db->execute();
	}
	public function setNewProfilePicture($file, $id){
	    //Check file
	    $filename = JFile::makeSafe($file['name']);
	    $src = $file['tmp_name']; // Src
	    $location = JPATH_BASE . '/images/components/com_shetrades/';
	    $dest = JPATH_BASE . '/images/components/com_shetrades/' .$filename; //Destination
	    $ext =  JFile::getExt($file['name']);
	    $newname = 'profile' .$id. '_' .time(). '.' .$ext;
	    JFile::upload($src, $dest); // Upload file
	    rename($dest, $location . $newname);
	    //Update the biz info table
	    $db = JFactory::getDBO();
	    $query = $db->getQuery(true);
	    $query = "INSERT INTO #__shetrades_biz_media SET biz_id = '" .$id. "', profile = '" .$newname. "'";
	    $db->setQuery($query);
	    $db->execute();    
	}
	public function sendEmailtoBizOwner($email){
		$app = JFactory::getApplication();

		$mailfrom = $app->get('mailfrom');
		$fromname = $app->get('fromname');
		$sitename = $app->get('sitename');
		
		$email		= JstringPunycode::emailToPunycode($email);
		$subject	= 'Thank you for registering at SheTrades' ;
		//$message	= $data['message'];
		//$name       = $data['name'];

		$style = 'font-family:arial;font-size:12px;';			
		// Prepare email body
		$prefix = JText::sprintf('COM_CONTACT_ENQUIRY_TEXT', JUri::base());
		$body = '<h2 style="font-family:arial;font-size:13px;padding:5px 0px;border-bottom:solid 1px #777">Business Registration</h2>';
		$body .= '<p style="font-family:arial;font-size:12px">You have successfully registered a business at Shetrades</p>';
		$body .= '<p style="font-family:arial;font-size:12px"></p>';
		
		$mail = JFactory::getMailer();
		$mail->addRecipient($email);
		//$mail->addReplyTo(array($email, $name));
		$mail->setSender(array('noreply@shetrades.com', $fromname));
		$mail->setSubject($sitename.': '.$subject);
		$mail->MsgHTML($body);
		$sent = $mail->Send();

		return $sent;

	}


}
