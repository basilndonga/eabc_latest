<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

$document = JFactory::getDocument();
$document->addStyleSheet('components/com_members/assets/css/list.css');
$document->addStyleSheet('components/com_members/assets/css/dashboard.css');

$user       = JFactory::getUser();
$userId     = $user->get('id');

$canCreate  = $user->authorise('core.create', 'com_members');
$canEdit    = $user->authorise('core.edit', 'com_members');
$canCheckin = $user->authorise('core.manage', 'com_members');
$canChange  = $user->authorise('core.edit.state', 'com_members');
$canDelete  = $user->authorise('core.delete', 'com_members');

?>
<div class="page-header">
  <h1>My Dashboard</h1>
</div>
<div class="front-end-list row dashboard-view">  
  <!-- begin panel -->
  <div class="col-sm-4 col-lg-4 col-md-4 dashboard-item">
    <a href="<?php echo JRoute::_('index.php?option=com_members&view=messages'); ?>">
       <div class="thumbnail">     
          <h1 class="dash-item-icon"><i class="fa fa-envelope"></i></h1>
          <h3> Inbox</h3>     
       </div>
    </a>
  </div>
  <!-- end panel -->
  <!-- begin panel -->
  <div class="col-sm-4 col-lg-4 col-md-4 dashboard-item">
    <a href="<?php echo JRoute::_('index.php?option=com_members&view=notifications'); ?>">
       <div class="thumbnail">     
          <h1 class="dash-item-icon"><i class="fa fa-warning"></i></h1>
          <h3>Notifications</h3>     
       </div>
    </a>
  </div>
  <!-- end panel -->
   <!-- begin panel -->
  <div class="col-sm-4 col-lg-4 col-md-4 dashboard-item">
    <a href="<?php echo JRoute::_('index.php?option=com_members&view=businessinfos&Itemid=513'); ?>">
       <div class="thumbnail">
          <h1 class="dash-item-icon"><i class="fa fa-list"></i></h1>
          <h3> List of Businesses</h3>   
       </div>
    </a>
  </div>
  <!-- end panel -->
  <!-- begin panel -->
  <div class="col-sm-4 col-lg-4 col-md-4 dashboard-item">
    <a href="<?php echo JRoute::_('index.php?option=com_members&view=businessinfos&owner='.$userId.'&Itemid=513'); ?>">
       <div class="thumbnail">
          <h1 class="dash-item-icon"><i class="fa fa-briefcase"></i></h1>
          <h3> My Registered Businesses</h3>   
       </div>
    </a>
  </div>
  <!-- end panel -->
  <!-- begin panel -->
  <div class="col-sm-4 col-lg-4 col-md-4 dashboard-item">
    <a href="<?php echo JRoute::_('index.php?option=com_members&view=dashboard&layout=favorites'); ?>">
       <div class="thumbnail">
          <h1 class="dash-item-icon"><i class="fa fa-heart"></i></h1>
          <h3> My Favorites</h3>   
       </div>
    </a>
  </div>
  <!-- end panel -->
</div>