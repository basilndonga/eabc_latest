<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::base() . '/components/com_members/assets/css/item.css');
$canEdit = JFactory::getUser()->authorise('core.edit', 'com_members.' . $this->item->id);
if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_members' . $this->item->id)) {
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>
<?php if ($this->item) : ?>

    <div class="item_fields">
        <table class="table">
            <tr>
			<th><?php echo JText::_('COM_SHETRADES_FORM_LBL_ACCREDITATION_ID'); ?></th>
			<td><?php echo $this->item->id; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_FORM_LBL_ACCREDITATION_STATUS'); ?></th>
			<td><?php echo $this->item->status; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_FORM_LBL_ACCREDITATION_CREATED_BY'); ?></th>
			<td><?php echo $this->item->created_by_name; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_FORM_LBL_ACCREDITATION_BIZ_ID'); ?></th>
			<td><?php echo $this->item->biz_id; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_FORM_LBL_ACCREDITATION_ACCREDITATION_ID'); ?></th>
			<td><?php echo $this->item->accreditation_id; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_FORM_LBL_ACCREDITATION_COMMENT'); ?></th>
			<td><?php echo $this->item->comment; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_FORM_LBL_ACCREDITATION_ACCREDITATION_DATE'); ?></th>
			<td><?php echo $this->item->accreditation_date; ?></td>
</tr>

        </table>
    </div>
    <?php if($canEdit && $this->item->checked_out == 0): ?>
		<button type="button" onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_members&task=accreditation.edit&id='.$this->item->id); ?>';"><?php echo JText::_("COM_SHETRADES_EDIT_ITEM"); ?></button>
	<?php endif; ?>
								<?php if(JFactory::getUser()->authorise('core.delete','com_members.accreditation.'.$this->item->id)):?>
									<button type="button" onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_members&task=accreditation.remove&id=' . $this->item->id, false, 2); ?>';"><?php echo JText::_("COM_SHETRADES_DELETE_ITEM"); ?></button>
								<?php endif; ?>
    <?php
else:
    echo JText::_('COM_SHETRADES_ITEM_NOT_LOADED');
endif;
?>
