<?php

/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

require_once JPATH_COMPONENT.'/helpers/members.php';
require_once JPATH_COMPONENT.'/helpers/notifications.php';
require_once JPATH_COMPONENT.'/helpers/match.php';

/**
 * View class for a list of Shetrades.
 */
class MembersViewNotifications extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;
    protected $params;
    protected $user;
    protected $doc;
    protected $user_businesses;
    protected $biz_notifications = array();
    protected $buyer_notifications = array();

    /**
     * Display the view
     */
    public function display($tpl = null) {
        $app = JFactory::getApplication();
        $this->user = JFactory::getUser();
        $this->doc = JFactory::getDocument();
        JHtml::_('jquery.framework');
        $this->doc->addScript(JUri::base() . 'components/com_members/assets/js/shetrades.js');
        
        // get the businesses that belong to this user. returns IDs
        $this->user_businesses = MembersFrontendHelper::getUserBusinesses($this->user->id);
        
        // use the businesses ids to fetch biz notifications  
        
        foreach ($this->user_businesses as $mybiz){
           // fetch notification for each
           $notifs = MembersHelperNotifications::getBizMessages($mybiz->id);
           array_push($this->biz_notifications,$notifs);
        }
        
        // we need to also get any buyer messages that we may have
        
        $this->buyer_notifications = MembersHelperNotifications::getBuyerMessages($this->user->id);

        $this->params = $app->getParams('com_members');       

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }

        $this->_prepareDocument();
        parent::display($tpl);
    }

    /**
     * Prepares the document
     */
    protected function _prepareDocument() {
        $app = JFactory::getApplication();
        $menus = $app->getMenu();
        $title = null;

        // Because the application sets a default page title,
        // we need to get it from the menu item itself
        $menu = $menus->getActive();
        if ($menu) {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else {
            $this->params->def('page_heading', JText::_('COM_SHETRADES_DEFAULT_PAGE_TITLE'));
        }
        $title = $this->params->get('page_title', '');
        if (empty($title)) {
            $title = $app->getCfg('sitename');
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
        }
        $this->document->setTitle($title);

        if ($this->params->get('menu-meta_description')) {
            $this->document->setDescription($this->params->get('menu-meta_description'));
        }

        if ($this->params->get('menu-meta_keywords')) {
            $this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
        }

        if ($this->params->get('robots')) {
            $this->document->setMetadata('robots', $this->params->get('robots'));
        }
    }
    /*
    * proxy function to get the owner of a business given the biz id
    */
    protected function getBusinessOwner($bizid){ 

        return MembersFrontendHelper::getBusinessOwner($bizid);
    }

}