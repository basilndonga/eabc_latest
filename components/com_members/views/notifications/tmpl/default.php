<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
//JHTML::_('script', 'system/multiselect.js', false, true);
//JHtml::_('formbehavior.chosen', 'select');
//JHtml::_('behavior.formvalidation');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_members/assets/css/list.css');
$document->addStyleSheet('components/com_members/assets/css/dashboard.css');
$user = $this->user;
$userId     = $user->get('id');

$biz_notifs_count = count($this->biz_notifications[0]);
$buyer_notifs_count = count($this->buyer_notifications);

$canCreate  = $user->authorise('core.create', 'com_members');
$canEdit    = $user->authorise('core.edit', 'com_members');
$canCheckin = $user->authorise('core.manage', 'com_members');
$canChange  = $user->authorise('core.edit.state', 'com_members');
$canDelete  = $user->authorise('core.delete', 'com_members');

?>
<div class="page-header">
  <h1>Notifications</h1>
</div>
<div class="front-end-list row notifications-view"> 
  <ul class="nav nav-tabs">
    <li role="presentation" class="active"><a href="#" data-target="#BizNotifs" data-toggle="tab">Members Notifications</a></li>
    <li role="presentation"><a href="#" data-target="#BuyerNotifs" data-toggle="tab">Buyer Notifications</a></li>
  </ul>
<div class="tab-content">
  <div class="tab-pane active" id="BizNotifs"> 
      <?php if ($biz_notifs_count > 0) { ?>
	  <div class="list-group biz-notifs">
	       <?php foreach($this->biz_notifications[0] as $notification){
	         $list = '<div class="list-group-item" id="list-group-item-'.$notification->id.'">';
	         $list .= '<div class="col-sm-3"><strong>'.$notification->created_on.' </strong></div> ';
	         $list .= '<div class="col-sm-5">'.$notification->message.'</div>';
	         if($notification->type == 'like'){
	            $likeparams = json_decode($notification->params);         
	            $list .='<div class="col-sm-4" id="biz-like-buttons-'.$notification->id.'"> ';
	            $list .=' <button class="btn btn-default biz-like-dismiss" data-id="'.$notification->id.'">Dismiss</button> ';
	            $list .=' <button class="btn btn-success biz-like-connect" data-id="'.$notification->id.'" data-url="'.$likeparams->action_uri.'">Like Back</button> ';
	            $list .='</div>';           
	         }
	         else {
	            $list .='<div class="col-sm-4"> ';
	            $list .=' <button class="btn btn-danger" data-id="'.$notification->id.'">Mark as Read</button> ';
	            $list .='</div>';          
	         }
	         $list .='</div>';
	         echo $list;   
	       }
	       ?>
	  </div>
	<?php } else {
	   echo '<p>You do not have any notifications associated with your registered members</p>';
	} ?>
  </div>
  <!-- end tab 1 content -->
  <div class="tab-pane" id="BuyerNotifs">
     <?php if ($buyer_notifs_count > 0) { ?>
	  <div class="list-group buyer-notifs">
	       <?php foreach($this->buyer_notifications as $notification){
	         $list = '<div class="list-group-item" id="list-group-item-'.$notification->id.'">';
	         $list .= '<div class="col-sm-3"><strong>'.$notification->created_on.' </strong></div> ';
	         $list .= '<div class="col-sm-5">'.$notification->message.'</div>';
	         if($notification->type == 'match'){
	            $matchparams = json_decode($notification->params);         
	            $list .='<div class="col-sm-4" id="biz-like-buttons-'.$notification->id.'"> ';
	            $list .=' <button class="btn btn-default biz-match-dismiss" data-id="'.$notification->id.'">Dismiss</button> ';
	            $list .=' <button class="btn btn-success send-message-to-match" ';
	            $list .='  data-id="'.$notification->id.'" ';
	            $list .='  data-bizid="'.$matchparams->matched_biz.'" ';
	            $list .='  data-recipient="'.$this->getBusinessOwner($matchparams->matched_biz).'" ';
	            $list .='  data-sender="'.$userId.'" ';
	            $list .='  data-bizname="'.$matchparams->name.'"> ';
	            $list .='  Send a Message to '.$matchparams->name;
	            $list .='</button> ';
	            $list .='</div>';           
	         }
	         else {
	            $list .='<div class="col-sm-4"> ';
	            $list .=' <button class="btn btn-danger" data-id="'.$notification->id.'">Mark as Read</button> ';
	            $list .='</div>';          
	         }
	         $list .='</div>';
	         echo $list;   
	       }
	       ?>
	  </div>
     <?php } else {
        echo '<p>You do not have any user notifications</p>';
          }
     ?>
  </div>
</div>
  
  
</div>