<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');

$document = JFactory::getDocument();
$document->addStyleSheet('components/com_members/assets/css/list.css');
$document->addStyleSheet('components/com_members/assets/css/dashboard.css');

$user       = $this->user;
$userId     = $user->get('id');

$canCreate  = $user->authorise('core.create', 'com_members');
$canEdit    = $user->authorise('core.edit', 'com_members');
$canCheckin = $user->authorise('core.manage', 'com_members');
$canChange  = $user->authorise('core.edit.state', 'com_members');
$canDelete  = $user->authorise('core.delete', 'com_members');

$input = JFactory::getApplication()->input;
$thread = $input->get('thread'); // e.g 1305_1025
// split the $thread into array
$thread_arr = explode('_',$thread);
// check which does not match with this user id
$arr_recipient = array_merge(array_diff($thread_arr, array($userId)));

$conversation = $this->getConversation($thread);
/*echo '<pre>';
print_r($thread_arr);
print_r($arr_recipient);
//print_r($conversation);
echo '</pre>';
*/
?>
<div class="page-header">
  <h1>Conversation Thread</h1>
</div>
<div class="col-sm-12 conversation-view">
     <?php 
       foreach ($conversation as $message){
          if ($message->recipient_id == $userId){
            $msgtype = 'message-received';
            $msgname = $message->sender_name;
          }
          else {
            $msgtype = 'message-sent';
            $msgname = $user->name;
          }
          $div = '<div class="message '.$msgtype.' pull-left">';
	    $div .='<div class="message-name">'.$msgname.'</div>';
		$div .='<div class="message-text">';
		  $div .= $message->message_body;
		  $div .='<div class="message-date">'.$message->created_on.'</div>';
		$div .='</div>';	
	  $div .='</div>';
	  echo $div;
          
          
       }
     ?>
</div>
<div class="col-sm-12 messagebar">
       <textarea class="col-sm-10" placeholder="write your message here..." id="thread-reply-input" rows="5" style="height:50px;"></textarea>
       <button class="col-sm-2 btn btn-danger send-msg-from-thread" data-sender="<?php echo $userId; ?>" data-recipient="<?php echo $arr_recipient[0]; ?>">Send</button>
</div>