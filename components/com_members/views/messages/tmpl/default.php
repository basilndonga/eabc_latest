<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');

$document = JFactory::getDocument();
$document->addStyleSheet('components/com_members/assets/css/list.css');
$document->addStyleSheet('components/com_members/assets/css/dashboard.css');

$user       = $this->user;
$userId     = $user->get('id');

$canCreate  = $user->authorise('core.create', 'com_members');
$canEdit    = $user->authorise('core.edit', 'com_members');
$canCheckin = $user->authorise('core.manage', 'com_members');
$canChange  = $user->authorise('core.edit.state', 'com_members');
$canDelete  = $user->authorise('core.delete', 'com_members');

$total = count($this->received);
/*echo '<pre>';
print_r($this->received);
//print_r($this->sent);
echo '</pre>';
*/
?>
<div class="page-header">
  <h1>My Inbox</h1>
</div>

<aside class="right-side">
<!-- Content Header (Page header) -->

<!-- Main content -->
<section class="content">
    <!-- MAILBOX BEGIN -->
    <div class="mailbox row">
        <div class="col-xs-12">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row pad">
                                <!--<div class="col-sm-6">
                                <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#compose-modal"><i class="fa fa-pencil"></i> Compose Message</a>
                                </div>-->
                                <div class="col-sm-6">
                                    <a href="<?php echo JRoute::_('index.php?option=com_members&view=messages'); ?>" class="btn"><i class="fa fa-inbox"></i> Inbox</a>
                                    <a href="<?php echo JRoute::_('index.php?option=com_members&view=messages&layout=sent'); ?>" class="btn"><i class="fa fa-mail-forward"></i> Sent</a>

                                </div>
                            </div><!-- /.row -->

                            <div class="table-responsive">
                                <?php if ($total > 0) { ?>
                                <!-- THE MESSAGES -->
                                <table class="table table-mailbox">
                                    <?php foreach ($this->received as $inbox) { 
                                    $thread_link = JRoute::_('index.php?option=com_members&view=messages&layout=thread&thread='.$inbox->sender_id.'_'.$inbox->recipient_id);
                                    $row = '<tr class="unread">';
                                       //$row .=' <td class="small-col"><input type="checkbox" /></td>';
                                       //$row .=' <td class="small-col"><i class="fa fa-star"></i></td>';
                                       $row .=' <td class="name"><a href="#">'.$inbox->sender_name.'</a></td>';
                                       $row .=' <td class="subject"><a href="#">'.$inbox->message_body.'</a></td>';
                                       $row .=' <td class="time">'.$inbox->created_on.'</td>';
                                       $row .=' <td class="actions">';
                                           //$row .='<button class="btn btn-default inbox-mark-as-read" data-id="'.$inbox->id.'">Mark as read</button>';
                                           $row .='<a class="btn btn-success reply-to-inbox" href="'.$thread_link.'" ';
                                           $row .='  data-id="'.$inbox->id.'" ';
				           $row .='  data-recipient="'.$inbox->sender_id.'" ';
				           $row .='  data-sender="'.$inbox->recipient_id.'" ';
				           //$row .='  data-sendername="'.$inbox->sender_name.'"';
				           $row .='> ';
                                           $row .='Reply</a>';
                                           //$row.='<button class="btn btn">Delete</button>';
                                       $row .='</td>';   
                                    $row .='</tr>';
                                    echo $row;
                                     } ?>                              
                                    
                                </table>
                                <?php } else {
                                   echo '<p>No messages here</p>';
                                }
                                ?>
                            </div><!-- /.table-responsive -->
                        </div><!-- /.col (RIGHT) -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <div class="pull-right">
                        <small>Showing <?php echo $total; ?> messages</small>
                    </div>
                </div><!-- box-footer -->
            </div><!-- /.box -->
        </div><!-- /.col (MAIN) -->
    </div>
    <!-- MAILBOX END -->

</section><!-- /.content -->
</aside><!-- /.right-side -->
