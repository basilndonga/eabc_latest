<?php

/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// No direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.view');

/**
 * View to edit
 */
class MembersViewBusinessinfo extends JViewLegacy {

    protected $state;
    protected $item;
    protected $form;
    protected $params;
    protected $doc;
    protected $user;

    /**
     * Display the view
     */
    public function display($tpl = null) {
  
        $app = JFactory::getApplication();
        $this->user = JFactory::getUser();
        $this->doc = JFactory::getDocument();
        JHtml::_('jquery.framework');
        $this->doc->addScript(JUri::base() . 'components/com_members/assets/js/shetrades.js');
        $this->doc->addStyleSheet(JUri::base() . 'components/com_members/assets/css/item.css');

        $this->state = $this->get('State');
        //$this->item = $this->get('Data');
        $this->item = $this->get('Item');
        $this->params = $app->getParams('com_members');

        if (!empty($this->item)) {          
		$this->form		= $this->get('Form');
        }

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }       

        if ($this->_layout == 'edit') {
            $authorised = $this->user->authorise('core.create', 'com_members');
            if ($authorised !== true) {
                throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
            }
        }

        $this->_prepareDocument();
        parent::display($tpl);
    }

    /**
     * Prepares the document
     */
    protected function _prepareDocument() {
        $app = JFactory::getApplication();
        $menus = $app->getMenu();
        $title = null;

        // Because the application sets a default page title,
        // we need to get it from the menu item itself
        $menu = $menus->getActive();
        if ($menu) {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else {
            $this->params->def('page_heading', JText::_('COM_SHETRADES_DEFAULT_PAGE_TITLE'));
        }
        //$title = $this->params->get('page_title', '');
        $title = $this->item->name;
        if (empty($title)) {
            $title = $app->getCfg('sitename');
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
        }
        $this->document->setTitle($title);

        if ($this->params->get('menu-meta_description')) {
            $this->document->setDescription($this->params->get('menu-meta_description'));
        }

        if ($this->params->get('menu-meta_keywords')) {
            $this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
        }

        if ($this->params->get('robots')) {
            $this->document->setMetadata('robots', $this->params->get('robots'));
        }
    }

}
