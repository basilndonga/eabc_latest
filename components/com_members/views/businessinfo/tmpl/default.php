<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;
$user_id = $this->user->get('id');

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_members.' . $this->item->id);
if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_members' . $this->item->id)) {
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>
<?php if ($this->item) : 
/*echo '<pre>';
print_r($this->item);
echo '</pre>';

jexit();
*/
?>

<div class="page-header">
  <h1> <?php echo $this->item->name; ?> </h1>
</div>
<div class="row">
<div class="col-md-12">
  <div class="row">
    <div class="col-md-5">
      <div class="row inner">
        <div class="col-md-6">
          <?php if($this->item->logo){ ?>
          <img src="<?php echo $this->item->logo; ?>" alt="<?php echo $this->item->name; ?>" class="img-responsive center-block" >
          <?php //echo $this->item->logo;?>
          <?php }else{ ?>
          <img src="images/no-profile-picture.png" style="width: 207px; height: auto;" alt="<?php echo $this->item->name; ?>" class="img-responsive center-block">
          <?php } ?>
        </div>
        <div class="col-md-6">
          <div class="foundation-col-6 user-info"> 
            <h1 itemprop="name" class="username fn premium"><?php echo $this->item->rep[0]['name'];?> </h1>
            <!-- end rep title -->
            <div class="job-detail"> <span itemprop="jobTitle" class="job-title title"><?php echo $this->item->rep[0]['jobtitle'];?></span>
              <div data-qa="1a8b47" class="employment-status" itemscope="" itemtype="http://schema.org/Organization" itemprop="worksFor"> at <?php echo $this->item->name; ?>
                <div class="microdata-block">
                  <div class="location"><?php echo $this->item->country; ?></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-7">
      <div class="foundation-row upsell-icons">
        <div class="col-md-12 first-upsell-group">
          <ul class="clfx">
            <li data-logged-out-action="upsellIconEmail" data-registration-target="contact"> <a href="mailto:<?php echo $this->item->email; ?>"><i style="font-size: 30px; padding-top: 12px;" class="fa fa-envelope"></i>
              <p>E-mail</p>
              </a> </li>
            <li data-logged-out-action="upsellIconPhone" data-registration-target="contact"> <a href="tel:<?php echo $this->item->phone; ?>"><i style="font-size: 30px; padding-top: 12px;" class="fa fa-phone"></i>
              <p>Phone</p>
              </a> </li>
            <li data-logged-out-action="upsellIconAddress" data-registration-target="contact"> <i style="font-size: 30px; padding-top: 12px;" class="fa fa-map-marker"></i>
              <p>Address</p>
            </li>
            <li data-logged-out-action="upsellIconMessenger" data-registration-target="contact"> <a href="#"><i style="font-size: 30px; padding-top: 12px;" class="fa fa-connectdevelop"></i>
              <p>Connect</p>
              </a> </li>
            <li data-logged-out-action="upsellIconFax" data-registration-target="contact"> <i style="font-size: 30px; padding-top: 12px;" class="fa fa-fax"></i>
              <p>Fax</p>
            </li>
            <?php if($this->item->www){ ?>
            <li data-logged-out-action="upsellIconWeb" data-registration-target="contact"> <a href="//<?php echo $this->item->www; ?>" target="_blank"><i style="font-size: 30px; padding-top: 12px;" class="fa fa-link"></i>
              <p>Website</p>
              </a> </li>
            <?php }else{ ?>
            <li data-logged-out-action="upsellIconWeb" data-registration-target="contact"> <i style="font-size: 30px; padding-top: 12px;" class="fa fa-link"></i>
              <p>Website</p>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <!-- Show like button once logged in -->
      <div class="col-md-12 text-center"> 
        <!--<a class="upsell-button add-to-fav" href="#" data-biz_id="<?php echo $this->item->id ?>" data-user_id="<?php echo $user_id; ?>"> 
          <span><i class="fa fa-heart"></i> Add to Favorites</span> 
        </a>-->
        <button type="button" class="btn btn-success add-to-fav" data-biz_id="<?php echo $this->item->id ?>" data-user_id="<?php echo $user_id; ?>"><span><i class="fa fa-heart"></i> Add to Favorites</span> </button>
      </div>
      <!-- end show like button once logged in --> 
    </div>
  </div>
  <!--End image --> 
  <br />
</div>
<!-- begin boddy content -->
<div class="col-md-12">
  <div class="component-layout-card lo-profile-details">
    <div class="foundation-row">
      <div>
        <div class="profile-details-headline"> More information about <strong><?php echo $this->item->name; ?></strong> </div>
        <div class="profile-details ">
          <section id="cv">
            <div class="row-fluid block">
              <div class="col-md-6">
                <h2>Business Description</h2>
                <p><?php echo $this->item->company_desc; ?> </p>
              </div>
              <div class="col-md-6">
                <h2>Company information</h2>
                <!-- company information here -->
                <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
                  <ul id="myTabs" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Basic</a></li>
                    <li class="" role="presentation"><a aria-expanded="false" href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Membership & Certifications</a></li>
                    <!--<li class="" role="presentation"><a aria-expanded="true" href="#dropdown1" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1">IT Services Information</a></li>-->
                   <li class="" role="presentation"><a aria-expanded="true" href="#social" role="tab" id="social-tab" data-toggle="tab" aria-controls="social">Social Media</a></li>
                    </li>
                  </ul>
                  <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
                      <div class="panel-heading">
                        <table class="table table-striped">
                          <tbody>
                            <tr>
                              <td><i class="fa fa-usd"></i> Annual value of sales in USD for most recent financial year </td>
                              <td><?php echo $this->item->annual_revenue_usd; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-usd"></i> Annual value of exports for most recent financial year</td>
                              <td><?php echo $this->item->annual_values_exports; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-calendar-o"></i> How many years have you been exporting</td>
                              <td><?php echo $this->item->annual_values_exports; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-globe"></i> Which countries do you export to? </td>
                              <td><?php echo $this->item->annual_values_exports; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-globe"></i> Which countries do you export from? </td>
                              <td><?php echo $this->item->annual_values_exports; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-venus"></i> Headed by Woman</td>
                              <td>
                                <?php   
					  if ($this->item->headed_by_woman == 1)   
					  {   
					  ?>
						<?php echo 'Yes';?>
						<?php
					  } else {?>
						<?php echo 'No';?>
						<?php
					}
				?>
				</td>
                            </tr>
                            <tr>
                              <td>Number of Employees</td>
                              <td><?php echo $this->item->no_of_employees; ?></td>
                            </tr>
                            <tr>
                              <td>Percentage of business owned by Women</td>
                              <td><?php echo $this->item->perc_owned_by_woman; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-clock-o"></i> Lead Time </td>
                              <td><?php echo $this->item->lead_times; ?></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                      <div class="panel-heading">
                        <div class="row">
                          <div class="col-md-12"><strong>Membership:</strong></div>
                          <div class="col-md-12"> 
                            <!-- get membership -->
                            <?php
                                foreach($this->item->accreditors as $accreditor){	
                                   echo '<p>'.$accreditor.'</p>';
                                }
                            ?> 
                            <!-- end membership --> 
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12"><strong>Certifications:</strong></div>
                          <div class="col-md-12"> 
                            <!-- get Certification -->
                            <?php
                                foreach($this->item->certification as $certification){	
                                   echo '<p>'.$certification.'</p>';
                                }
                            ?> 
                            <!-- end Certification --> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in" id="social" aria-labelledby="social-tab">
                      <div class="panel-heading">
                        <div class="row text-center">
                          <div class="col-md-4"><a href="//facebook.com/<?php echo $this->item->facebook; ?>" target="_blank"><i style="font-size: 60px; color: #00ADEF;" class="fa fa-facebook-official"></i></a> </div>
                          <div class="col-md-4"><a href="//twitter.com/<?php echo $this->item->twitter; ?>" target="_blank"><i style="font-size: 60px; color: #00ADEF;" class="fa fa-twitter-square"></i></a> </div>
                          <div class="col-md-4"><a href="//linkedin.com/<?php echo $this->item->linkedin; ?>" target="_blank"><i style="font-size: 60px; color: #00ADEF;" class="fa fa-linkedin-square"></i></a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end company information --> 
              </div>
            </div>
            <div id="row-fluid">
              <h2><span class="strong"><?php echo $this->item->name; ?></span> Products and Services</h2>
              <div id="haves" class="block col-md-6">
                <h2>Products Offering</h2>
                <ul class="tag-list cv-tag-list inv-link inline-block-list clfx">
			<?php 
			foreach($this->item->products_offering as $p_offering){
		           echo '<li itemscope="itemscope" itemtype="http://schema.org/Offer" itemprop="makesOffer">'.$p_offering.'</li>';
		        }
			?>          
                <ul>
                <h2>Services Offering</h2>
                <ul class="tag-list cv-tag-list inv-link inline-block-list clfx">
			<?php 
			foreach($this->item->services_offering as $s_offering){
		           echo '<li itemscope="itemscope" itemtype="http://schema.org/Offer" itemprop="makesOffer">'.$s_offering.'</li>';
		        }
			?>          
                <ul>
              </div>
              <div id="haves" class="block col-md-6">
                <h2>Looking for Services</h2>

                <ul class="tag-list cv-tag-list inv-link inline-block-list clfx">
			<?php 
			foreach($this->item->services_buying as $s_buying){
			   echo '<li itemscope="itemscope" itemtype="http://schema.org/Offer" itemprop="makesOffer">'.$s_buying.'</li>';
			}
			?>          
                <ul>
                <h2>Looking for Products</h2>

                <ul class="tag-list cv-tag-list inv-link inline-block-list clfx">
			<?php 
			foreach($this->item->products_buying as $p_buying){
			   echo '<li itemscope="itemscope" itemtype="http://schema.org/Offer" itemprop="makesOffer">'.$p_buying.'</li>';
			}
			?>          
                <ul>
              </div>
            </div>
            <div class="clear"></div>
          </section>
        </div>
      </div>
    </div>
  </div>
</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>
<button type="button" onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_members&task=businessinfo.edit&id='.$this->item->id); ?>';"><?php echo JText::_("COM_SHETRADES_EDIT_ITEM"); ?></button>
<?php endif; ?>
<?php if(JFactory::getUser()->authorise('core.delete','com_members.businessinfo.'.$this->item->id)):?>
<button type="button" onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_members&task=businessinfo.remove&id=' . $this->item->id, false, 2); ?>';"><?php echo JText::_("COM_SHETRADES_DELETE_ITEM"); ?></button>
<?php endif; ?>
<?php
else:
    echo JText::_('COM_SHETRADES_ITEM_NOT_LOADED');
endif;
?>
<?php 
	/*JPluginHelper::importPlugin('content');
	$share = plgContentBt_socialshare::socialButtons();
	echo $share['script']; // Required
	echo $share['buttons']; // Social button
	echo $share['recommend']; // Recommendation bar
	echo $share['comment']; // facebook comment box*/
?>
<?php
 function getBusiness($user_id){
  $db = JFactory::getDbo();
  $query = $db->getQuery(true);  
  $query->select(array('id'));
  $query->from('#__shetrades_biz_info');
  $query->where($db->quoteName('created_by')." = ".$user_id);
  $db->setQuery($query);
  $results = $db->loadObject();
  return $results;
 }
?>