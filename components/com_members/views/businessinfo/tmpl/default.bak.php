<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::base() . '/components/com_shetrades/assets/css/item.css');
$canEdit = JFactory::getUser()->authorise('core.edit', 'com_shetrades.' . $this->item->id);
if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_shetrades' . $this->item->id)) {
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>
<?php if ($this->item) : ?>

<div class="page-header">
  <h1> <?php echo $this->item->name; ?> </h1>
</div>
<div class="row">
<div class="col-md-12">
  <div class="row">
    <div class="col-md-5">
      <div class="row inner">
        <div class="col-md-6">
          <?php if($this->item->logo){ ?>
          <img src="images/components/com_shetrades/<?php echo $this->item->logo; ?>" alt="<?php echo $this->item->name; ?>" class="img-responsive center-block" >
          <?php //echo $this->item->logo;?>
          <?php }else{ ?>
          <img src="images/no-profile-picture.png" style="width: 207px; height: auto;" alt="<?php echo $this->item->name; ?>" class="img-responsive center-block">
          <?php } ?>
        </div>
        <div class="col-md-6">
          <div class="foundation-col-6 user-info"> 
            <!-- get company rep title -->
            <?php
		  	$db = JFactory::getDbo();
			$query = $db->getQuery(true);			
			$query->select(array('biz_id', 'company_rep', 'job_title'));
			$query->from('#__shetrades_biz_rep');
			$query->where($db->quoteName('biz_id')." = ".$this->item->id);			
			$db->setQuery($query);			
			$results = $db->loadObject();			
			?>
            <h1 itemprop="name" class="username fn premium"><?php echo $results->company_rep;?> </h1>
            <!-- end rep title -->
            <div class="job-detail"> <span itemprop="jobTitle" class="job-title title"><?php echo $results->job_title;?></span>
              <div data-qa="1a8b47" class="employment-status" itemscope="" itemtype="http://schema.org/Organization" itemprop="worksFor"> at <?php echo $this->item->name; ?>
                <div class="microdata-block">
                  <div class="location"><?php echo $this->item->country; ?></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-7">
      <div class="foundation-row upsell-icons">
        <div class="col-md-12 first-upsell-group">
          <ul class="clfx">
            <li data-logged-out-action="upsellIconEmail" data-registration-target="contact"> <a href="mailto:<?php echo $this->item->email; ?>"><i style="font-size: 30px; padding-top: 12px;" class="fa fa-envelope"></i>
              <p>E-mail</p>
              </a> </li>
            <li data-logged-out-action="upsellIconPhone" data-registration-target="contact"> <a href="tel:<?php echo $this->item->phone; ?>"><i style="font-size: 30px; padding-top: 12px;" class="fa fa-phone"></i>
              <p>Phone</p>
              </a> </li>
            <li data-logged-out-action="upsellIconAddress" data-registration-target="contact"> <i style="font-size: 30px; padding-top: 12px;" class="fa fa-map-marker"></i>
              <p>Address</p>
            </li>
            <li data-logged-out-action="upsellIconMessenger" data-registration-target="contact"> <a href="#"><i style="font-size: 30px; padding-top: 12px;" class="fa fa-connectdevelop"></i>
              <p>Connect</p>
              </a> </li>
            <li data-logged-out-action="upsellIconFax" data-registration-target="contact"> <i style="font-size: 30px; padding-top: 12px;" class="fa fa-fax"></i>
              <p>Fax</p>
            </li>
            <?php if($this->item->www){ ?>
            <li data-logged-out-action="upsellIconWeb" data-registration-target="contact"> <a href="//<?php echo $this->item->www; ?>" target="_blank"><i style="font-size: 30px; padding-top: 12px;" class="fa fa-link"></i>
              <p>Website</p>
              </a> </li>
            <?php }else{ ?>
            <li data-logged-out-action="upsellIconWeb" data-registration-target="contact"> <i style="font-size: 30px; padding-top: 12px;" class="fa fa-link"></i>
              <p>Website</p>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <!-- sign up button --> 
      <!-- Show login button once logged in -->
      <?php   
          $user =& JFactory::getUser();   
          $user_id = $user->get('id');   
          if (!$user_id)   
          {   
          ?>
      <div class="col-md-12 text-center"> <a class="upsell-button" href="index.php?option=com_users&view=login"> <span><i class="fa fa-sign-in"></i> Sign up and view the entire profile</span> </a> </div>
      <?php
          } else {?>
      <div class="col-md-12 text-center"> <a class="upsell-button" href="register-your-business"> <span><i class="fa fa-laptop"></i> Create your business</span> </a> </div>
      <?php
  		}
        ?>
      <!-- end show login button once logged in --> 
      <!-- end sign up button--> 
    </div>
  </div>
  <!--End image --> 
  <br />
</div>
<!-- begin boddy content -->
<div class="col-md-12">
  <div class="component-layout-card lo-profile-details">
    <div class="foundation-row">
      <div>
        <div class="profile-details-headline"> More information about <strong><?php echo $this->item->name; ?></strong> </div>
        <div class="profile-details ">
          <section id="cv">
            <div class="row-fluid block">
              <div class="col-md-6">
                <h2>Business Description</h2>
                <p><?php echo $this->item->company_desc; ?> </p>
              </div>
              <div class="col-md-6">
                <h2>Company information</h2>
                <!-- company information here -->
                <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs">
                  <ul id="myTabs" class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Basic</a></li>
                    <li class="" role="presentation"><a aria-expanded="false" href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Membership & Certifications</a></li>
                    <!--<li class="" role="presentation"><a aria-expanded="true" href="#dropdown1" role="tab" id="dropdown1-tab" data-toggle="tab" aria-controls="dropdown1">IT Services Information</a></li>-->
                   <li class="" role="presentation"><a aria-expanded="true" href="#social" role="tab" id="social-tab" data-toggle="tab" aria-controls="social">Social Media</a></li>
                    </li>
                  </ul>
                  <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">
                      <div class="panel-heading">
                        <table class="table table-striped">
                          <tbody>
                            <tr>
                              <td><i class="fa fa-usd"></i> Annual value of sales in USD for most recent financial year </td>
                              <td><?php echo $this->item->annual_revenue_usd; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-usd"></i> Annual value of exports for most recent financial year</td>
                              <td><?php echo $this->item->annual_values_exports; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-calendar-o"></i> How many years have you been exporting</td>
                              <td><?php echo $this->item->annual_values_exports; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-globe"></i> Which countries do you export to? </td>
                              <td><?php echo $this->item->annual_values_exports; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-globe"></i> Which countries do you export from? </td>
                              <td><?php echo $this->item->annual_values_exports; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-venus"></i> Headed by Woman</td>
                              <td>
                                <?php   
								  if ($this->item->headed_by_woman == 1)   
								  {   
								  ?>
									<?php echo 'Yes';?>
									<?php
								  } else {?>
									<?php echo 'No';?>
									<?php
								}
								?></td>
                            </tr>
                            <tr>
                              <td># of Employees</td>
                              <td><?php echo $this->item->no_of_employees; ?></td>
                            </tr>
                            <tr>
                              <td>% owned by Women</td>
                              <td><?php echo $this->item->perc_owned_by_woman; ?></td>
                            </tr>
                            <tr>
                              <td><i class="fa fa-clock-o"></i> Lead Time </td>
                              <td><?php echo $this->item->lead_times; ?></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                      <div class="panel-heading">
                        <div class="row">
                          <div class="col-md-12"><strong>Membership:</strong></div>
                          <div class="col-md-12"> 
                            <!-- get membership -->
                            <?php
                                $db = JFactory::getDbo();
                                $query = $db->getQuery(true);			
                                $query->select(array('biz_id', 'accreditation_id'));
                                $query->from('#__shetrades_accreditations');
                                $query->where($db->quoteName('biz_id')." = ".$this->item->id);			
                                $db->setQuery($query);			
                                $results = $db->loadObject();			
                                ?>
                            <?php echo $results->accreditation_id;?> 
                            <!-- end membership --> 
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12"><strong>Certifications:</strong></div>
                          <div class="col-md-12"> 
                            <!-- get Certification -->
                            <?php
                                $db = JFactory::getDbo();
                                $query = $db->getQuery(true);			
                                $query->select(array('biz_id', 'certificates'));
                                $query->from('#__shetrades_biz_certification');
                                $query->where($db->quoteName('biz_id')." = ".$this->item->id);			
                                $db->setQuery($query);			
                                $results = $db->loadObject();			
                                ?>
                            <?php echo $results->certificates;?> 
                            <!-- end Certification --> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--<div role="tabpanel" class="tab-pane fade in" id="dropdown1" aria-labelledby="dropdown1-tab">
                      <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu synth chambray yr.</p>
                    </div>--> 
                    <div role="tabpanel" class="tab-pane fade in" id="social" aria-labelledby="social-tab">
                      <div class="panel-heading">
                        <div class="row text-center">
                          <div class="col-md-4"><a href="//facebook.com/<?php echo $this->item->facebook; ?>" target="_blank"><i style="font-size: 60px; color: #00ADEF;" class="fa fa-facebook-official"></i></a> </div>
                          <div class="col-md-4"><a href="//twitter.com/<?php echo $this->item->twitter; ?>" target="_blank"><i style="font-size: 60px; color: #00ADEF;" class="fa fa-twitter-square"></i></a> </div>
                          <div class="col-md-4"><a href="//linkedin.com/<?php echo $this->item->linkedin; ?>" target="_blank"><i style="font-size: 60px; color: #00ADEF;" class="fa fa-linkedin-square"></i></a> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- end company information --> 
              </div>
            </div>
            <div id="row-fluid">
              <h2><span class="strong"><?php echo $this->item->name; ?>'s</span> Offer & demand information</h2>
              <div id="haves" class="block col-md-6">
                <h2>Offering</h2>
              <?php 
			  $db = JFactory::getDbo(); 
				 $query = $db->getQuery(true); 
				 $query->select('biz_id, item')->from($db->quoteName('#__shetrades_biz_offering'));
                 $query->where($db->quoteName('biz_id')." = ".$this->item->id);	
				 $db->setQuery($query); 
				 $offerings = $db->loadObjectList(); 
			   ?>
              <ul class="tag-list cv-tag-list inv-link inline-block-list clfx">
				<?php 
				   foreach($offerings as $offering){
					   echo '<li itemscope="itemscope" itemtype="http://schema.org/Offer" itemprop="makesOffer">'.$offering->item.'</li>';
					   }
				?>          
                <ul>
              </div>
              <div id="haves" class="block col-md-6">
                <h2>Looking for</h2>
                <?php 
			  $db = JFactory::getDbo(); 
				 $query = $db->getQuery(true); 
				 $query->select('biz_id, item')->from($db->quoteName('#__shetrades_biz_buying'));
                 $query->where($db->quoteName('biz_id')." = ".$this->item->id);	
				 $db->setQuery($query); 
				 $buyings = $db->loadObjectList(); 
			   ?>
                <ul class="tag-list cv-tag-list inv-link inline-block-list clfx">
				<?php 
				   foreach($buyings as $buying){
					   echo '<li itemscope="itemscope" itemtype="http://schema.org/Offer" itemprop="makesOffer">'.$buying->item.'</li>';
					   }
				?>          
                <ul>
              </div>
            </div>
            <div class="clear"></div>
          </section>
        </div>
      </div>
    </div>
  </div>
</div>

<?php if($canEdit && $this->item->checked_out == 0): ?>
<button type="button" onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_shetrades&task=businessinfo.edit&id='.$this->item->id); ?>';"><?php echo JText::_("COM_SHETRADES_EDIT_ITEM"); ?></button>
<?php endif; ?>
<?php if(JFactory::getUser()->authorise('core.delete','com_shetrades.businessinfo.'.$this->item->id)):?>
<button type="button" onclick="window.location.href='<?php echo JRoute::_('index.php?option=com_shetrades&task=businessinfo.remove&id=' . $this->item->id, false, 2); ?>';"><?php echo JText::_("COM_SHETRADES_DELETE_ITEM"); ?></button>
<?php endif; ?>
<?php
else:
    echo JText::_('COM_SHETRADES_ITEM_NOT_LOADED');
endif;
?>
<?php 
	/*JPluginHelper::importPlugin('content');
	$share = plgContentBt_socialshare::socialButtons();
	echo $share['script']; // Required
	echo $share['buttons']; // Social button
	echo $share['recommend']; // Recommendation bar
	echo $share['comment']; // facebook comment box*/
?>
