<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHTML::_('script', 'system/multiselect.js', false, true);
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.formvalidation');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_members/assets/css/list.css');

$user       = JFactory::getUser();
$userId     = $user->get('id');

$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$ordering   = ($listOrder == 'a.ordering');
$canCreate  = $user->authorise('core.create', 'com_members');
$canEdit    = $user->authorise('core.edit', 'com_members');
$canCheckin = $user->authorise('core.manage', 'com_members');
$canChange  = $user->authorise('core.edit.state', 'com_members');
$canDelete  = $user->authorise('core.delete', 'com_members');
//Check if someone has done a search
$search_home = JFactory::getApplication()->input->post->getArray();
$jinput = JFactory::getApplication()->input;
$itemid = $jinput->get('Itemid');
$search_term = $search_home['search'];
//Get Countries
$db = JFactory::getDBO();
$query = $db->getQuery(true);
$query->select($db->quoteName(array('id', 'nicename')));
$query->from($db->quoteName('#__shetrades_country'));
$query->order('nicename ASC');
$db->setQuery($query);
$countries = $db->loadObjectList();

//Get products
$dbp = JFactory::getDBO();
$pquery = $dbp->getQuery(true);
$pquery->select($dbp->quoteName(array('name', 'product_code')));
$pquery->from($dbp->quoteName('#__shetrades_product'));
$pquery->order('id ASC');
$dbp->setQuery($pquery);
$products = $dbp->loadObjectList();

//Get Services
$dbs = JFactory::getDBO();
$squery = $dbs->getQuery(true);
$squery->select($dbs->quoteName(array('name', 'service_code')));
$squery->from($dbs->quoteName('#__shetrades_service'));
$squery->order('id ASC');
$dbs->setQuery($squery);
$services = $dbs->loadObjectList();

?>

<div class="page-header">
  <h1> Member Profiles </h1>
</div>
<div class="front-end-list row">
<?php if($jinput->get('layout') == 'advanced'){ ?>
  <div class="col-sm-12 col-lg-12 col-md-12">
    <p>I want to view businesses / companies that : </p>
    <form role="form" action="<?php echo JRoute::_('index.php?option=com_members&view=businessinfos&Itemid=513&layout=advanced&advsearch=on'); ?>" method="post">
      <input type="hidden" name="advancedsearch" value="on"/>
      <div class="form-group col-xs-12 col-sm-4">
        <label for="exampleInputEmail1">Are in this country: <span class="important">*</span></label>
        <select name="country" class="form-control">
          <option value="">--Select country--</option>
          <?php foreach($countries as $country){ ?>
          <option value="<?php echo $country->id; ?>"><?php echo $country->nicename; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group col-xs-12 col-sm-4">
        <label for="exampleInputEmail1">Are verified:</label>
        <select name="verification" class="form-control">
          <option value="">--Select if verified or not--</option>
          <option value="yes">Yes</option>
          <option value="no">No</option>
        </select>        
      </div>
      <div class="form-group col-xs-12 col-sm-4">
        <label for="exampleInputEmail1">Sell these products</label>
        <select name="product" class="form-control">
          <option value="">--Not applicable--</option>
          <?php foreach($products as $product){ ?>
          <option value="<?php echo $product->product_code; ?>"><?php echo $product->product_code. ' - ' .$product->name; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group col-xs-12 col-sm-4">
        <label for="exampleInputEmail1">Offer these services</label>        
        <select name="service" class="form-control">
          <option value="">--Not applicable--</option>
          <?php foreach($services as $service){ ?>
          <option value="<?php echo $service->service_code; ?>"><?php echo $service->service_code. ' - ' .$service->name; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group col-xs-12 col-sm-4">
        <label for="exampleInputEmail1">Buy these products</label>
        <select name="product_buy" class="form-control">
          <option value="">--Not applicable--</option>
          <?php foreach($products as $product){ ?>
          <option value="<?php echo $product->product_code; ?>"><?php echo $product->product_code. ' - ' .$product->name; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group col-xs-12 col-sm-4">
        <label for="exampleInputEmail1">Buy these services</label>        
        <select name="service_buy" class="form-control">
          <option value="">--Not applicable--</option>
          <?php foreach($services as $service){ ?>
          <option value="<?php echo $service->service_code; ?>"><?php echo $service->service_code. ' - ' .$service->name; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group col-xs-12 col-sm-4">
        <label for="exampleInputEmail1">Are <span class="range"><strong>0</strong></span>% owned by women</label>
        <input type="range" name="percentownedwomen" class="rangeslider" min="0" max="100" value="0" required>
      </div>
      <div class="form-group col-xs-12 col-sm-4">
        <label for="exampleInputEmail1">Have a minimum exporting experience - <span class="employeenum"><strong>0</strong></span> Years</label>
        <input type="range" name="employeenum" class="empslider" min="0" max="20" value="0">
      </div>
      <div class="form-group col-xs-12 col-sm-4">
      	<button type="submit" class="btn btn-primary advancedsearchbtn">Click to search</option>
      </div>
    </form>
  </div>
  <br/>
  <br/>
<?php } ?>
<?php if(!$jinput->get('layout')){ ?>
  <div class="col-sm-12 col-lg-12 col-md-12">
    <form role="form" action="index.php?option=com_members&amp;view=businessinfos&amp;Itemid=513" method="post">
      <div class="form-group input-group">
        <input type="text" class="form-control" name="search" placeholder="Find members and partners" value="<?php echo $search_term; ?>" required>
        <span class="input-group-btn">
          <button name="search_btn" type="submit" class="btn btn-default" title="Search"><span class="fa fa-search"></span></button>
        </span>
      </div>
      <?php if(!empty($search_term)){ ?>
      <p class="result_bar">Total: <span><?php echo count($this->items); ?></span> results found</p>
      <?php } ?>
    </form>
  </div>
<?php } ?>
  <form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm" class="form-inline">
  <?php foreach ($this->items as $i => $item) : ?>
  <?php $canEdit = $user->authorise('core.edit', 'com_members'); ?>
  <?php if (!$canEdit && $user->authorise('core.edit.own', 'com_members')): ?>
  <?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
  <?php endif; ?>
  
  <!-- begin panel -->
  <div class="col-sm-4 col-lg-4 col-md-4">
    <div class="thumbnail">
      <?php 
        // check if user is guest
        if ($user->guest){
          $link = '#';
          $attributes = 'data-toggle="modal" data-target="#AccessModal"';
        }
        else {
          if ($this->hasBusiness()){
             $link = JRoute::_('index.php?option=com_members&view=businessinfo&id=' . (int) $item->id.'&Itemid='.$itemid);
             $attributes = '';
          }
          else {
             $link = '#';
             $attributes = 'data-toggle="modal" data-target="#AccessModal"';
          }
          
        }
      
      ?>
      <a class="popup-message-onclick" href="<?php echo $link; ?>" <?php echo $attributes; ?>>
        <div class="col-sm-3">
        <?php if($item->logo){ ?>
          <img src="images/no-profile-picture.png" alt="<?php echo $this->escape($item->name); ?>" class="company_prodile_img img-responsive">
        <?php }else{ ?>
          <img src="images/no-profile-picture.png" alt="<?php echo $this->escape($item->name); ?>" class="img-responsive">
        <?php } ?>
        </div>
        <div class="col-sm-9 company-description">
          <?php
            if(strlen($item->name) < 30){
              $name = $item->name;
            }else{
              $name = substr($item->name,0,30). '....';
            }
          ?>
          <p><?php echo $this->escape($name); ?></p>
          <?php if($item->city){echo $this->escape($item->city). '<br/>';} ?>
          <?php if($item->country){echo $this->escape($item->country);} ?>
        </div>
      </a>
    </div>
  </div>
  <!-- end panel -->
  <?php endforeach; ?>
</div>

<?php 
echo $this->pagination->getListFooter(); 
?>
</form>
<div id="AccessModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Attention</h4>
      </div>
      <?php
        if ($user->guest){  ?>
           <div class="modal-body">
            <p>You need to be logged in to view the business profile.</p>
           </div>
           <div class="modal-footer">
             <a href="<?php echo JRoute::_('index.php?option=com_users&view=login', false, 2); ?>" class="btn btn-primary">Log in</a> or
             <a href="<?php echo JRoute::_('index.php?option=com_users&view=registration', false, 2); ?>" class="btn btn-success">Create an Account</a>
           </div>
      <?php
        } 
        if (!$user->guest){
            if(!$this->hasBusiness()){ ?>
            <div class="modal-body">
              <p>You need to have registered at least one business to view other business profiles.</p>
            </div>
            <div class="modal-footer">
             <a href="<?php echo JRoute::_('index.php?option=com_members&view=businessinfoform&Itemid=525', false, 2); ?>" class="btn btn-success">Register Business</a>
            </div>
            
            <?php
               
            }       
        }    
      ?>
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Show login button once logged in -->
<?php     
  if ($userId)   
  {   
  ?>
<div class="front-end-list col-xs-12">
  <?php if ($canCreate): ?>
  <button class="btn btn-large btn-primary" type="button" onclick="window.location.href = '<?php echo JRoute::_('index.php?option=com_members&task=businessinfoform.edit&id=0', false, 2); ?>';"><?php echo JText::_('Add your Business'); ?></button>    <?php endif; ?>
  
  <?php if ($canCreate) : ?>
		<a href="<?php echo JRoute::_('index.php?option=com_members&task=verifierform.edit&id=0', false, 2); ?>"
		   class="btn btn-success btn-small"><i
				class="icon-plus"></i>
			<?php echo JText::_('COM_SHETRADES_VERIFIERS_ADD_ITEM'); ?></a>
	<?php endif; ?><br /><br />
</div>


    
<?php
  } else {?>
    <div class="front-end-list col-xs-12">  
  <button class="btn btn-large btn-primary" type="button" onclick="window.location.href = '<?php echo JRoute::_('joomla/users-component/registration-form', false, 2); ?>';"><?php echo JText::_('Create your Account'); ?></button><br /><br />  
</div>
<?php
  }
?>
<!-- end show login button once logged in -->

<script type="text/javascript">
  if (typeof jQuery == 'undefined') {
    var headTag = document.getElementsByTagName("head")[0];
    var jqTag = document.createElement('script');
    jqTag.type = 'text/javascript';
    jqTag.src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';
    jqTag.onload = jQueryCode;
    headTag.appendChild(jqTag);
  } else {
    jQueryCode();
  }

  function jQueryCode() {
    jQuery('.delete-button').click(function () {
      var item_id = jQuery(this).attr('data-item-id');
      <?php if($canDelete): ?>
      if (confirm("<?php echo JText::_('COM_SHETRADES_DELETE_MESSAGE'); ?>")) {
        window.location.href = '<?php echo JRoute::_('index.php?option=com_members&task=businessinfoform.remove&id=', false, 2) ?>' + item_id;
      }
      <?php endif; ?>

    });
  }
  jQuery('document').ready(function(){
    jQuery('.rangeslider').change(function(){
      var rangeval = jQuery(this).val();
      jQuery('span.range strong').text(rangeval);
    });
    jQuery('.empslider').change(function(){
      var empval = jQuery(this).val();
      jQuery('span.employeenum strong').text(empval);
    });
  });

</script>