<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHTML::_('script', 'system/multiselect.js', false, true);
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_shetrades/assets/css/list.css');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$ordering   = ($listOrder == 'a.ordering');
$canCreate  = $user->authorise('core.create', 'com_shetrades');
$canEdit    = $user->authorise('core.edit', 'com_shetrades');
$canCheckin = $user->authorise('core.manage', 'com_shetrades');
$canChange  = $user->authorise('core.edit.state', 'com_shetrades');
$canDelete  = $user->authorise('core.delete', 'com_shetrades');
//Check if someone has done a search
$search_home = JFactory::getApplication()->input->post->getArray();
$search_term = $search_home['search'];
?>

<div class="page-header">
  <h1> Business Profiles </h1>
</div>
<div class="front-end-list row">
  <div class="col-sm-12 col-lg-12 col-md-12">
    <form role="form" action="index.php?option=com_shetrades&amp;view=businessinfos&amp;Itemid=513" method="post">
      <div class="form-group input-group">
        <input type="text" class="form-control" name="search" placeholder="Find suppliers and partners" value="<?php echo $search_term; ?>" required>
        <span class="input-group-btn">
          <button name="search_btn" type="submit" class="btn btn-default" title="Search"><span class="fa fa-search"></span></button>
        </span>
      </div>
      <?php if(!empty($search_term)){ ?>
      <p class="result_bar">Total: <span><?php echo count($this->items); ?></span> results found</p>
      <?php } ?>  
    </form>
  </div>
  <?php foreach ($this->items as $i => $item) : ?>
  <?php $canEdit = $user->authorise('core.edit', 'com_shetrades'); ?>
  <?php if (!$canEdit && $user->authorise('core.edit.own', 'com_shetrades')): ?>
  <?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
  <?php endif; ?>
  
  <!-- begin panel -->
  <div class="col-sm-4 col-lg-4 col-md-4">
    <div class="thumbnail">
      <a href="<?php echo JRoute::_('index.php?option=com_shetrades&view=businessinfo&id=' . (int) $item->id); ?>">
        <div class="col-sm-3">
        <?php if($item->logo){ ?>
          <img src="images/<?php echo $item->logo; ?>" alt="<?php echo $this->escape($item->name); ?>" class="img-responsive">
        <?php }else{ ?>
          <img src="images/no-profile-picture.png" alt="<?php echo $this->escape($item->name); ?>" class="img-responsive">
        <?php } ?>
        </div>
        <div class="col-sm-9 company-description">
          <?php
            if(strlen($item->name) < 30){
              $name = $item->name;
            }else{
              $name = substr($item->name,0,30). '....';
            }
          ?>
          <p><?php echo $this->escape($name); ?></p>
          <?php if($item->city){echo $this->escape($item->city). '<br/>';} ?>
          <?php if($item->country){echo $this->escape($item->country);} ?>
        </div>
      </a>
    </div>
  </div>
  <!-- end panel -->
  <?php endforeach; ?>
</div>
<!-- Show login button once logged in -->
<?php   
  $user =& JFactory::getUser();   
  $user_id = $user->get('id');   
  if ($user_id)   
  {   
  ?>
<div class="front-end-list col-xs-12">
  <?php if ($canCreate): ?>
  <button class="btn btn-large btn-primary" type="button" onclick="window.location.href = '<?php echo JRoute::_('index.php?option=com_shetrades&task=businessinfoform.edit&id=0', false, 2); ?>';"><?php echo JText::_('Add your Business'); ?></button><br /><br />    <?php endif; ?>
</div>
<?php
  } else {?>
	  <div class="front-end-list col-xs-12">
  
  <button class="btn btn-large btn-primary" type="button" onclick="window.location.href = '<?php echo JRoute::_('joomla/users-component/registration-form', false, 2); ?>';"><?php echo JText::_('Create your Account'); ?></button><br /><br />  
</div><?php
  }
?>
<!-- end show login button once logged in -->
<?php /*<table>
  <tr>
    <td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>"><?php echo $this->pagination->getListFooter(); ?></td>
    <td><?php echo $this->loadTemplate('filter'); ?></td>
  </tr>
</table>*/?>
<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		var headTag = document.getElementsByTagName("head")[0];
		var jqTag = document.createElement('script');
		jqTag.type = 'text/javascript';
		jqTag.src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';
		jqTag.onload = jQueryCode;
		headTag.appendChild(jqTag);
	} else {
		jQueryCode();
	}

	function jQueryCode() {
		jQuery('.delete-button').click(function () {
			var item_id = jQuery(this).attr('data-item-id');
			<?php if($canDelete): ?>
			if (confirm("<?php echo JText::_('COM_SHETRADES_DELETE_MESSAGE'); ?>")) {
				window.location.href = '<?php echo JRoute::_('index.php?option=com_shetrades&task=businessinfoform.remove&id=', false, 2) ?>' + item_id;
			}
			<?php endif; ?>

		});
	}

</script>