<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;
require_once JPATH_COMPONENT.'/library/googlemapapi.class.php';
require_once JPATH_COMPONENT.'/helpers/members.php';
JHtml::_('behavior.tooltip');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_members/assets/css/list.css');

$businesses = ShetradesFrontendHelper::getBusinessesForMap();

// map library reference
//http://www.phpinsider.com/php/code/GoogleMapAPI/
$map = new GoogleMapAPI('map');
//$map->setAPIKey('ABQIAAAAxp5FF-A0RhHOnnTBwrlRbx');

// loop through the list and create some map markers
/*
echo '<pre>';
count($businesses);
print_r($businesses);
echo '</pre>';
*/
foreach ($businesses as $item) {
  $map_address = "$item->city".", "."$item->country"; 
  $map->addMarkerByAddress($item->city.', '.$item->country,$item->name,'<h3>'.$item->name.'</h3>');
}
// add map scripts to page

$document->addScriptDeclaration($map->printHeaderJS());
$document->addScriptDeclaration($map->printMapJS());

// more map options
$map->setWidth('100%'); 
$map->setHeight('600px');
$map->setMapType('map');
?>

<div class="page-header">
  <h1> Business Profiles - Map</h1>
</div>
<div class="row">
  <?php
  $map->printMap();
  $map->printOnLoad();
  ?>
</div>
