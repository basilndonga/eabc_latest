<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_members', JPATH_SITE);
$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::base() . '/components/com_members/assets/css/form.css');
$doc->addScript(JUri::base() . '/components/com_members/assets/js/form.js');
?>

<script type="text/javascript">
	if (jQuery === 'undefined') {
		document.addEventListener("DOMContentLoaded", function (event) {
			jQuery('#form-accreditation').submit(function (event) {
				
			});

			
		});
	} else {
		jQuery(document).ready(function () {
			jQuery('#form-accreditation').submit(function (event) {
				
			});

			
		});
	}
</script>

<div class="accreditation-edit front-end-edit">
	<?php if (!empty($this->item->id)): ?>
		<h1>Edit <?php echo $this->item->id; ?></h1>
	<?php else: ?>
		<h1>Add</h1>
	<?php endif; ?>

	<form id="form-accreditation" action="<?php echo JRoute::_('index.php?option=com_members&task=accreditation.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
						<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<li><?php echo $this->form->getLabel('status'); ?>
				<?php echo $this->form->getInput('status'); ?></li>
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php if(empty($this->item->created_by)){ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

				<?php } 
				else{ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

				<?php } ?>				<li><?php echo $this->form->getLabel('biz_id'); ?>
				<?php echo $this->form->getInput('biz_id'); ?></li>
				<li><?php echo $this->form->getLabel('accreditation_id'); ?>
				<?php echo $this->form->getInput('accreditation_id'); ?></li>
				<li><?php echo $this->form->getLabel('comment'); ?>
				<?php echo $this->form->getInput('comment'); ?></li>
				<li><?php echo $this->form->getLabel('accreditation_date'); ?>
				<?php echo $this->form->getInput('accreditation_date'); ?></li>
				<div class="width-100 fltlft" <?php if (!JFactory::getUser()->authorise('core.admin','shetrades')): ?> style="display:none;" <?php endif; ?> >
                <?php echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
                <?php echo JHtml::_('sliders.panel', JText::_('ACL Configuration'), 'access-rules'); ?>
                <fieldset class="panelform">
                    <?php echo $this->form->getLabel('rules'); ?>
                    <?php echo $this->form->getInput('rules'); ?>
                </fieldset>
                <?php echo JHtml::_('sliders.end'); ?>
            </div>
				<?php if (!JFactory::getUser()->authorise('core.admin','shetrades')): ?>
                <script type="text/javascript">
                    jQuery('#rules select').each(function(){
                       var option_selected = jQuery(this).find(':selected');
                       var input = jQuery('<input>');
                       input.attr('type', 'hidden');
                       input.attr('name', jQuery(this).attr('name'));
                       input.attr('value', option_selected.val());
                       jQuery('form-accreditation').append(input);
                    });
                </script>
             <?php endif; ?>
		<div class="button-div">
			<?php if ($this->canSave): ?>
				<button type="submit" class="validate"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
			<?php endif; ?>

			<a href="<?php echo JRoute::_('index.php?option=com_members&task=accreditationform.cancel'); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
		</div>

		<input type="hidden" name="option" value="com_members" />
		<input type="hidden" name="task" value="accreditationform.save" />
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>
