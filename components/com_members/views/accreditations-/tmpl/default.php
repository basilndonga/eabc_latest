<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHTML::_('script', 'system/multiselect.js', false, true);
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_members/assets/css/list.css');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$ordering   = ($listOrder == 'a.ordering');
$canCreate  = $user->authorise('core.create', 'com_members');
$canEdit    = $user->authorise('core.edit', 'com_members');
$canCheckin = $user->authorise('core.manage', 'com_members');
$canChange  = $user->authorise('core.edit.state', 'com_members');
$canDelete  = $user->authorise('core.delete', 'com_members');
?>

<form action="<?php echo JRoute::_('index.php?option=com_members&view=accreditations'); ?>" method="post"
	name="adminForm" id="adminForm">

	
	<table class="front-end-list">
		<thead>
		<tr>
			
				<th class="align-left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_ACCREDITATIONS_STATUS', 'a.status', $listDirn, $listOrder); ?>
				</th>

				<th class="align-left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_ACCREDITATIONS_BIZ_ID', 'a.biz_id', $listDirn, $listOrder); ?>
				</th>

				<th class="align-left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_ACCREDITATIONS_ACCREDITATION_ID', 'a.accreditation_id', $listDirn, $listOrder); ?>
				</th>

				<th class="align-left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_ACCREDITATIONS_COMMENT', 'a.comment', $listDirn, $listOrder); ?>
				</th>

				<th class="align-left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_ACCREDITATIONS_ACCREDITATION_DATE', 'a.accreditation_date', $listDirn, $listOrder); ?>
				</th>

			<?php if (isset($this->items[0]->state)) : ?>
				
			<?php endif; ?>

			<?php if (isset($this->items[0]->id)) : ?>
				<th class="nowrap align-left">
					<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
				</th>
			<?php endif; ?>

							<?php if ($canEdit || $canDelete): ?>
					<th class="align-center">
				<?php echo JText::_('COM_SHETRADES_ACCREDITATIONS_ACTIONS'); ?>
				</th>
				<?php endif; ?>

		</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->items as $i => $item) : ?>
			<?php $canEdit = $user->authorise('core.edit', 'com_members'); ?>

							<?php if (!$canEdit && $user->authorise('core.edit.own', 'com_members')): ?>
					<?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
				<?php endif; ?>


			<tr class="row<?php echo $i % 2; ?>">
				
					<td>
						<?php echo $item->status; ?>
					</td>

					<td>
						<?php echo $item->biz_id; ?>
					</td>

					<td>
						<?php echo $item->accreditation_id; ?>
					</td>

					<td>
					<?php if (isset($item->checked_out) && $item->checked_out) : ?>
						<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'accreditations.', $canCheckin); ?>
					<?php endif; ?>
					<a href="<?php echo JRoute::_('index.php?option=com_members&view=accreditation&id=' . (int) $item->id); ?>">

						<?php echo $this->escape($item->comment); ?>
					</a>
					</td>

					<td>
						<?php echo $item->accreditation_date; ?>
					</td>

				<?php if (isset($this->items[0]->state)) : ?>
					
				<?php endif; ?>
				<?php if (isset($this->items[0]->id)) : ?>
					<td class="align-left">
						<?php echo (int) $item->id; ?>
					</td>
				<?php endif; ?>

								<?php if ($canEdit || $canDelete): ?>
					<td class="align-center">
						<?php if ($canEdit): ?>
							<button onclick="window.location.href = '<?php echo JRoute::_('index.php?option=com_members&task=accreditationform.edit&id=' . $item->id, false, 2); ?>';" class="btn btn-mini" type="button"><?php echo JText::_('COM_SHETRADES_ACCREDITATIONS_EDIT'); ?></button>
						<?php endif; ?>
						<?php if ($canDelete): ?>
							<button data-item-id="<?php echo $item->id; ?>" class="delete-button" type="button"><?php echo JText::_('COM_SHETRADES_ACCREDITATIONS_DELETE'); ?></button>
						<?php endif; ?>
					</td>
				<?php endif; ?>

			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

	<?php if ($canCreate): ?>
		<button type="button"
			onclick="window.location.href = '<?php echo JRoute::_('index.php?option=com_members&task=accreditationform.edit&id=0', false, 2); ?>';"><?php echo JText::_('COM_SHETRADES_ADD_ITEM'); ?></button>
	<?php endif; ?>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		var headTag = document.getElementsByTagName("head")[0];
		var jqTag = document.createElement('script');
		jqTag.type = 'text/javascript';
		jqTag.src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';
		jqTag.onload = jQueryCode;
		headTag.appendChild(jqTag);
	} else {
		jQueryCode();
	}

	function jQueryCode() {
		jQuery('.delete-button').click(function () {
			var item_id = jQuery(this).attr('data-item-id');
			<?php if($canDelete): ?>
			if (confirm("<?php echo JText::_('COM_SHETRADES_DELETE_MESSAGE'); ?>")) {
				window.location.href = '<?php echo JRoute::_('index.php?option=com_members&task=accreditationform.remove&id=', false, 2) ?>' + item_id;
			}
			<?php endif; ?>

		});
	}

</script>