<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_shetrades', JPATH_SITE);
$doc = JFactory::getDocument();
$doc->addStyleSheet(JUri::base() . '/components/com_shetrades/assets/css/form.css');
$doc->addStyleSheet(JUri::base() . '/components/com_shetrades/assets/css/bootstrap-multiselect.css');
$doc->addScript(JUri::base() . '/components/com_shetrades/assets/js/form.js');
$doc->addScript(JUri::base() . '/components/com_shetrades/assets/js/bootstrap-multiselect.js');
?>
<script type="text/javascript">
	if (jQuery === 'undefined') {
		document.addEventListener("DOMContentLoaded", function (event) {
			jQuery('#form-businessinfo').submit(function (event) {
				
			});

			
		});
	} else {
		jQuery(document).ready(function () {
			jQuery('.multiselect').multiselect();
			jQuery('#form-businessinfo').submit(function (event) {
				
			});

			
		});
	}
</script>

<div class="businessinfo-edit front-end-edit">
  <?php if (!empty($this->item->id)): ?>
  <h1>Edit your <?php echo $this->item->id; ?> profile</h1>
  <?php else: ?>
  <h1>Create your business profile</h1>
  <?php endif; ?>
  <div class="container-fluid">  
       <form id="form-businessinfo" action="<?php echo JRoute::_('index.php?option=com_shetrades&task=businessinfo.save'); ?>" method="post" class="form-validate" enctype="multipart/form-data">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home">Main Information</a></li>
      <li><a data-toggle="tab" href="#menu1">Company Details</a></li>
      <li><a data-toggle="tab" href="#menu2">Production Capacity & Marketing</a></li>
      <li><a data-toggle="tab" href="#menu3">Operations</a></li>
    </ul>
    <div class="tab-content" id="businessinformation">
      <div id="home" class="tab-pane fade in active"> 
        <!-- start form -->
        <p class="lead">Maximise your opportunity to create new business connections by completing your company’s profile accurately and fully</p>
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="exampleInputPassword1"><span class="star">&nbsp;*&nbsp;</span>Company Name</label>
              <input type="text" class="form-control" name="jform[name]" id="jform_name" required="required">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_company_rep"><span class="star">&nbsp;*&nbsp;</span>Name of Company representative</label>
              <input type="text" class="form-control" name="jform[company_rep]" id="jform_company_rep">
            </div>
          </div>
        </div>
        <!-- fluid form -->
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group multipleselect">
              <label for="jform_member"><span class="star">&nbsp;*&nbsp;</span>Request verification if your company is a member of the following networks </label>
              <select class="required form-control multiselect hidden" name="jform[member][]" multiple="multiple" id="member">
                <option value="0">Select</option>
                <?php 
                foreach ($this->accreditors as $accreditor){
                    echo '<option value="'.$accreditor['userid'].'">'.$accreditor['name'].'</option>';
                }
                ?>
              </select>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_title"><span class="star">&nbsp;*&nbsp;</span>Ms./Mr. </label>
              <select class="required form-control" name="jform[title]" id="jform_title">
                <option value="Ms">Ms</option>
                <option value="Mrs">Mrs</option>
                <option value="Mr">Mr</option>
              </select>
            </div>
          </div>
        </div>
        <!-- fluid form -->
        
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_company_bo"><span class="star">&nbsp;*&nbsp;</span>What does your company offer and/or buy?</label>
              <?php 
			  $db = JFactory::getDbo(); 
				 $query = $db->getQuery(true); 
				 $query->select('name, id')->from($db->quoteName('#__shetrades_service'));
				 $db->setQuery($query); 
				 $services = $db->loadObjectList(); 
				 //print_r( $countries);    // will equal the retrieved value of nicename, id"
			   ?>
              <select class="required form-control" name="jform[company_bo]" id="jform_company_bo">
                <option value="">Select Service</option>
				<?php 
				   foreach($services as $service){
					   echo '<option value="'.$service->id.'">'.$service->name.'</option>';
					   }
				?>                
                </select>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_job_title">Job Title</label>
              <input type="text" class="form-control" name="jform[job_title]" id="jform_job_title" >
            </div>
          </div>
        </div>
        <!-- fluid form -->
        
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_service_bo"><span class="star">&nbsp;*&nbsp;</span>If your company is in the services sector, what does your company offer and/or buy? </label>
              <?php 
			  $db = JFactory::getDbo(); 
				 $query = $db->getQuery(true); 
				 $query->select('name, product_code')->from($db->quoteName('#__shetrades_product'));
				 $db->setQuery($query); 
				 $services_sectors = $db->loadObjectList(); 
				 //print_r( $countries);    // will equal the retrieved value of nicename, id"
			   ?>
              <select class="required form-control" name="jform[service_bo]" id="jform_service_bo">
                <option value="">Select Product/Service</option>
				<?php 
				   foreach($services_sectors as $services_sector){
					   echo '<option value="'.$services_sector->product_code.'">'.$services_sector->name.'</option>';
					   }
				?>                
                </select>
            </div>
          </div>
          <div class="col-sm-6">            
            <div class="form-group">
              <label for="jform_headed_by_woman">Is your company headed by a woman?</label>
              <select class="required form-control" name="jform[headed_by_woman]" id="jform_headed_by_woman">
                <option value="1">Yes</option>
                <option value="0">No</option>
              </select>
            </div>
          </div>
        </div>
        <!-- fluid form -->
        
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_company_address"><span class="star">&nbsp;*&nbsp;</span>Company Postal address and Zip Code</label>
              <textarea name="jform[company_address]" id="jform_company_address" class="required form-control" ></textarea>
            </div>
          </div>
          <div class="col-sm-6">
          <div class="form-group">
              <label for="jform_perc_owned_by_woman">% of the business owned by Women</label>
              <input type="text" class="form-control" name="jform[perc_owned_by_woman]" id="jform_perc_owned_by_woman" >
            </div>
            <div class="form-group">
              <label for="jform_no_of_employees"><span class="star">&nbsp;*&nbsp;</span>Number of Employees</label>
              <input type="text" class="form-control" name="jform[no_of_employees]" id="jform_no_of_employees">
            </div>
          </div>
        </div>
        <!-- fluid form -->
        
        <ul class="pager">
          <li class="next"><a href="#" onclick="jQuery('.nav-tabs a[href=#menu1]').tab('show');">Next <span aria-hidden="true">→</span></a></li>
        </ul>
      </div>
      <div id="menu1" class="tab-pane fade">
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_city"><span class="star">&nbsp;*&nbsp;</span>City</label>
              <input type="text" class="form-control" name="jform[city]" id="jform_city" required="required">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_country">Country</label>
              <?php 
			  $db = JFactory::getDbo(); 
				 $query = $db->getQuery(true); 
				 $query->select('nicename, id')->from($db->quoteName('#__shetrades_country'));
				 $db->setQuery($query); 
				 $countries = $db->loadObjectList(); 
				 //print_r( $countries);    // will equal the retrieved value of nicename, id"
			   ?>
              <select class="required-entry form-control" name="jform[country]" id="jform_country">
                <option value="">Select Country</option>
				<?php 
				   foreach($countries as $country){
					   echo '<option value="'.$country->id.'">'.$country->nicename.'</option>';
					   }
				?>                
                </select>
             </div>
          </div>
        </div>
        <!-- fluid form -->
        
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_phone"><span class="star">&nbsp;*&nbsp;</span>Telephone</label>
              <input type="text" class="form-control" name="jform[phone]" id="jform_phone" required="required">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_skype">Skype</label>
              <input type="text" class="form-control" name="jform[skype]" id="jform_skype" >
            </div>
          </div>
        </div>
        <!-- fluid form -->
        
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_fax">Fax</label>
              <input type="text" class="form-control" name="jform[fax]" id="jform_fax">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_year_est"><span class="star">&nbsp;*&nbsp;</span>Year of Establishment</label>
              <input type="text" class="form-control" name="jform[year_est]" id="jform_year_est">
            </div>
          </div>
        </div>
        <!-- fluid form -->
        
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_email"><span class="star">&nbsp;*&nbsp;</span>E-mail</label>
              <input type="email" class="form-control" name="jform[email]" id="jform_email">
            </div>
          </div>
          <div class="col-sm-6">
            <?php /*?><div class="form-group">
              <label for="jform_no_of_employees"><span class="star">&nbsp;*&nbsp;</span>Number of Employees</label>
              <input type="text" class="form-control" name="jform[no_of_employees]" id="jform_no_of_employees">
            </div><?php */?>
            <div class="form-group">
              <label for="jform_location"><span class="star">&nbsp;*&nbsp;</span>Location of your offices (Cities)</label>
              <input type="text" class="form-control" name="jform[location]" id="jform_location">
            </div>
          </div>
        </div>
        <!-- fluid form -->
        
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_no_of_female">Number of Female Employees</label>
              <input type="text" class="form-control" name="jform[no_of_female]" id="jform_no_of_female" >
            </div>
            <div class="form-group">
              <label for="jform_location">Website</label>
              <input type="text" class="form-control" name="jform[www]" id="jform_www">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_certification">Certifications</label><br />
              <textarea name="jform[certification]" id="jform_certification" class="form-control"></textarea>
            </div>            
          </div>
        </div>
        <!-- fluid form -->
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-12">
            <div class="form-group">
              <label for="jform_fax">Social Media Profiles</label>
            </div>
          </div>
          <div class="col-sm-12">
              <div class="col-sm-6 col-md-4">
              <div class="form-group">
                <label for="jform_facebook">Facebook</label>
                <input type="text" class="form-control" name="jform[facebook]" id="jform_facebook">
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
              <div class="form-group">
                <label for="jform_twitter">Twitter</label>
                <input type="text" class="form-control" name="jform[twitter]" id="jform_twitter">
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
              <div class="form-group">
                <label for="jform_linkedin">Linkedin</label>
                <input type="text" class="form-control" name="jform[linkedin]" id="jform_linkedin">
                </div>
              </div>
          </div>
        </div>
        <!-- fluid form -->
        <ul class="pager">
          <li class="previous"><a href="#" onclick="jQuery('.nav-tabs a[href=#home]').tab('show');"><span aria-hidden="true">←</span> Previous</a></li>
          <li class="next"><a href="#" onclick="jQuery('.nav-tabs a[href=#menu2]').tab('show');" <span aria-hidden="true">→</span>Next</a></li>
        </ul>
      </div>
      <div id="menu2" class="tab-pane fade">
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_annual_revenue_usd">Annual value of sales in USD for most recent financial year</label>
              <input type="text" class="form-control" name="jform[annual_revenue_usd]" id="jform_annual_revenue_usd" >
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_annual_revenue_usd_13">Annual value of exports for most recent financial year </label>
              <input type="text" class="form-control" name="jform[annual_revenue_usd_13]" id="jform_annual_revenue_usd_13" >
            </div>
          </div>
        </div>
        <!-- fluid form -->
        
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_annual_values_exports">How many years have you been exporting?</label>
              <input type="text" class="form-control" name="jform[annual_values_exports]" id="jform_annual_values_exports">
            </div>
          </div>
        </div>
        <!-- fluid form -->
        
        <!-- fluid form -->
        
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="export_countries">Which countries do you export to?</label>
              <input type="text" class="form-control" name="jform[export_countries]" id="jform_export_countries" >
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Lead Time </label>
              <input type="text" class="form-control" name="jform[export_operations]" id="jform_export_operations">
            </div>
          </div>
          <div class="col-sm-6">
            <?php /*?><div class="form-group">
              <label for="exampleInputEmail1"><span class="star">&nbsp;*&nbsp;</span>Please indicate your primary customers</label>
              <textarea name="jform[primary_customers]" id="jform_primary_customers" class="form-control"></textarea>
            </div><?php */?>
          </div>
          
        </div>
        <!-- fluid form --> 
        <ul class="pager">
            <li class="previous"><a href="#" onclick="jQuery('.nav-tabs a[href=#menu1]').tab('show');"><span aria-hidden="true">←</span> Previous</a></li>
            <li class="next"><a href="#" onclick="jQuery('.nav-tabs a[href=#menu3]').tab('show');"><span aria-hidden="true">→</span> Next</a></li>
          </ul>
      </div>
      <div id="menu3" class="tab-pane fade">
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_logos">Upload companies logo</label>
              <input type="file" name="jform[logos]" id="jform_logos">
              <label for="jform_logos">Upload profile image</label>
              <input type="file" name="jform[profile]" id="jform_profile">
              <label for="jform_logos">More images</label>
              <input id="input-2" class="file" multiple="true" data-show-upload="false" data-show-caption="true" type="file">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="jform_other_photos">Upload high quality pictures of your products / services</label>
              <input type="file" name="jform[other_photos]" id="jform_other_photos" >
              <label for="jform_other_photos_facilities">Upload high quality pictures of your facilities</label>
              <input type="file" name="jform[other_photos]" id="jform_other_photos_facilities" >
            </div>
          </div>
        </div>
        <!-- fluid form -->
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-12 termsandconditions">
            <div class="checkbox">
              <input type="checkbox" name="jform[delivery_terms]" id="jform_delivery_terms" required="required">I have read and understood the Terms and Conditions
            </div>
          </div>
        </div>
        <!-- fluid form -->
        
        
        <div class="row-fluid show-grid"><!-- fluid form -->
          <div class="col-sm-12">
            <div class="form-group">
              <label for="jform_company_description">Company Description</label>
              <textarea class="form-control" name="jform[company_desc]" id="jform_company_desc" class="form-control"></textarea>
            </div>
          </div>
        </div>
        <!-- fluid form -->
        <ul class="pager">
          <li class="previous"><a href="#" onclick="jQuery('.nav-tabs a[href=#menu2]').tab('show');" <span aria-hidden="true">←</span>Previous</a></li>
        </ul>
        
        <div class="button-div">
      <?php if ($this->canSave): ?>
      <button type="submit" class="validate btn btn-default"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
      <?php endif; ?>
      <a class="btn btn-danger" href="<?php echo JRoute::_('index.php?option=com_shetrades&task=businessinfoform.cancel'); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a> </div>
    <input type="hidden" name="option" value="com_shetrades" />
    <input type="hidden" name="task" value="businessinfoform.save" />
    <?php echo JHtml::_('form.token'); ?>
        </form>
      </div>
    </div>
  </div>
</div>