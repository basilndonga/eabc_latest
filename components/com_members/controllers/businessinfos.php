<?php
/**
 * @version     1.0.0
 * @package     com_members
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Businessinfos list controller class.
 */
class MembersControllerBusinessinfos extends MembersController
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function &getModel($name = 'Businessinfos', $prefix = 'MembersModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
	public function getCountries(){
		$model = parent::getModel('Countries', 'MembersModel', array('ignore_request' => true));
		return $model->getItems();

	}
}