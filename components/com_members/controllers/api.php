<?php
/**
 * @version     1.0.0
 * @package     com_members
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */

// No direct access.
defined('_JEXEC') or die;
jimport('joomla.environment.response');

//JResponse::setHeader('Access-Control-Allow-Origin','*',true);
//JResponse::setHeader('Access-Control-Allow-Headers','Authorization, Origin, X-Requested-With, Content-Type, Accept',true);
//header('Content-Type: application/json');
//header("Access-Control-Allow-Origin: *");
//header("Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept");
//error_reporting(0);
require_once JPATH_COMPONENT.'/controller.php';
require_once JPATH_COMPONENT.'/helpers/members.php';
require_once JPATH_COMPONENT.'/helpers/accreditors.php';
require_once JPATH_COMPONENT.'/helpers/notifications.php';
require_once JPATH_COMPONENT.'/helpers/match.php';
require_once JPATH_COMPONENT.'/helpers/certifications.php';
require_once JPATH_COMPONENT.'/helpers/messaging.php';

class MembersControllerApi extends ShetradesController
{ 
    
    /**
     * Proxy for getModel.
     * @since   1.6
     */
    public function &getModel($name = 'Businessinfos', $prefix = 'MembersModel', $config = array())
    {
        $model = parent::getModel($name, $prefix, array('ignore_request' => true));
        return $model;
    }
    /*
    * 
    * Test if the json api works
    * index.php?option=com_members&task=api.test
    */
    function test(){
 
        $userinfo = array(
            'name' => 'John Doe',
            'dob' => '1970-12-25',
            'age' => 25
        );
        echo json_encode($userinfo);
        jexit();    
        
    }
    function getuser(){
        $user_id = $this->input->get('user_id');
        $user = JFactory::getUser($user_id);
        echo json_encode($user);
        jexit();
    }
    /*
    * Register a user remotely from the mobile app
    * Accepts a JSON formatted string with: name,email,password 
    * {"name":"john doe","email":"jdoe@gbc.co.ke","password":"xyz294&_T"}
    * check if the source is facebook or registration
    */
    function registeruser(){
        jimport('joomla.input.json');
        jimport('joomla.user.helper');
        $jsoninput = new JInputJSON();
        $user = new JUser;
        $config = JFactory::getConfig();
        $authorize = JFactory::getACL();
        $rawdata = $jsoninput->getRaw();
        $data = $jsoninput->getArray();
        $userdata = array(
            "name"=>$data['name'], 
            "username"=>$data['email'], 
            "password"=>$data['password'],
            "email"=>$data['email']
        );
        $source = $this->input->get('source');
        //print_r($data);
        //echo $rawdata;
        $usersConfig = JComponentHelper::getParams('com_users');
        $newUsertype = $usersConfig->get('new_usertype');
        
        //echo $newUsertype; // 2 for registered
        $user->set('groups', array($newUsertype));
        $user->set('usertype', 'deprecated');
        //Write to database
        if(!$user->bind($userdata)) {
            //throw new Exception("Could not bind data. Error: " . $user->getError());
            echo 'Could not bind data. Error:' . $user->getError();
        }
        if ($source == 'registration'){
            if (!$user->save()) {
                header('Content-Type: application/json');
                //throw new Exception("Could not save user. Error: " . $user->getError());
                echo json_encode('{"status": "error","message": "Could not save user. Error '.$user->getError().' The email you entered has already being registered","id": "'.$user->id.'"}');
            }
            else{
                //$this->sendEmailtoRegisteredUser($data);
                header('Content-Type: application/json');
                echo json_encode('{"status":"success","message": "User Created","id": "'.$user->id.'","email":"'.$user->email.'","name":"'.$user->name.'"}');
            }
        }
        // Doing some more checks for facebook logins to prevent (email exists) errors when registering
        // 
        elseif ($source == 'facebook'){
            $user_exists = JUserHelper::getUserId($data['email']); // this returns an id or zero if not  found
            // check if the email exists first, then return the user details
            if($user_exists > 0){
                // fetch the user data, $user_exists conatins the id of the user if not zero
                $fbuser = JFactory::getUser($user_exists);
                echo json_encode('{"status":"success","message": "User Created","id": "'.$fbuser->id.'","email":"'.$fbuser->email.'","name":"'.$fbuser->name.'"}');  

            }
            // returned zero, let's try to save the user
            else {
                if (!$user->save()) {
                    header('Content-Type: application/json');
                    //throw new Exception("Could not save user. Error: " . $user->getError());
                    echo json_encode('{"status": "error","message": "Could not save user. Error '.$user->getError().' ","id": "'.$user->id.'"}');
                }
                else{
                    header('Content-Type: application/json');
                    echo json_encode('{"status":"success","message": "User Created","id": "'.$user->id.'","email":"'.$user->email.'","name":"'.$user->name.'"}');
                }
            }
        }
        else {
            echo json_encode('{"status":"error","message";"source parameter was not found in registration request"}');
        }
    
        jexit();

    }
    function retrievepass(){
        jimport('joomla.input.json');
        jimport('joomla.user.helper');
        $jsoninput = new JInputJSON();
        $data = $jsoninput->getArray();
        // create a random password
        $temp_pass = JUserHelper::genRandomPassword(8);

        $user_id = JUserHelper::getUserId($data['email']);
        $user = JFactory::getUser($user_id);
        //print_r($user);
        $password = array('password' => $temp_pass, 'password2' => $temp_pass);
        if(!$user->bind($password)){
            //die('Could not bind data. Error: '.$user->getError());
            echo json_encode('{"status": "error","message":"Could not bind user data, an error occurred"}');
        }
        if(!$user->save()){
            //die('Could not save user. Error: '.$user->getError());
            echo json_encode('{"status": "error","message":"Could not reset your password, an error occurred"}');
        }
        else {
            // successfully set new password, send an email
            $message = 'Your Password has been reset to '.$temp_pass;
            $this->notifyPswdChange($data['email'],$message);
            echo json_encode('{"status": "success","message":"An email has been sent to '.$data['email'].' with the password."}');
        }
        /**/
        jexit();
    }
    function notifyPswdChange($email,$message){
        $app = JFactory::getApplication();

        $mailfrom = $app->get('mailfrom');
        $fromname = $app->get('fromname');
        $sitename = $app->get('sitename');
        $subject = 'Password Reset';

        $style = 'font-family:arial;font-size:12px;';           
        // Prepare email body
        $body = '<h2 style="font-family:arial;font-size:13px;padding:5px 0px;border-bottom:solid 1px #777">Password Reset</h2>';
        $body .= '<p style="font-family:arial;font-size:12px">'.$message.'</p>';
        $body .= '<p style="font-family:arial;font-size:12px"></p>';
        
        $mail = JFactory::getMailer();
        $mail->addRecipient($email);
        //$mail->addReplyTo(array($email, $name));
        $mail->setSender(array('noreply@gbc.co.ke', $fromname));
        $mail->setSubject($sitename.': '.$subject);
        $mail->MsgHTML($body);
        $sent = $mail->Send();

        return $sent;

    }
    function sendEmailtoRegisteredUser($data){
        $app = JFactory::getApplication();

        $mailfrom = $app->get('mailfrom');
        $fromname = $app->get('fromname');
        $sitename = $app->get('sitename');
        
        //$email      = JstringPunycode::emailToPunycode($email);
        $subject  = 'Registration on SheTrades';
        //$message  = $data['message'];
        $name       = $data['name'];
        $email     = $data['email'];

        $style = 'font-family:arial;font-size:12px;';           
        // Prepare email body
        $prefix = JText::sprintf('COM_CONTACT_ENQUIRY_TEXT', JUri::base());
        $body = '<h2 style="font-family:arial;font-size:13px;padding:5px 0px;border-bottom:solid 1px #777">User Registration</h2>';
        $body .= '<p style="font-family:arial;font-size:12px">Dear '.$name.'</p>';
        $body .= '<p style="font-family:arial;font-size:12px">You have successfully registered as a user at Shetrades</p>';
        $body .= '<p style="font-family:arial;font-size:12px"></p>';
        
        $mail = JFactory::getMailer();
        $mail->addRecipient($email);
        //$mail->addReplyTo(array($email, $name));
        $mail->setSender(array('noreply@gbc.co.ke', $fromname));
        $mail->setSubject($sitename.': '.$subject);
        $mail->MsgHTML($body);
        $sent = $mail->Send();

        return $sent;

    }
    /*
    * Login user remotely, from app or elsewhere
    * Accepts a json object only containing email and password
    * Joomla does the rest of the heavy-lifting for us
    */
    function loginuser(){
        jimport('joomla.input.json');
        jimport('joomla.user.helper');
        $jsoninput = new JInputJSON();
        $rawdata = $jsoninput->getRaw();
        $data = $jsoninput->getArray();
        $userdata = array( 
            "username"=>$data['email'], 
            "password"=>$data['password'],
        );
        
        //print_r($data);
        //echo $rawdata;

        $login = JFactory::getApplication()->login($userdata);
        if ($login){
            $user = JFactory::getUser();
            //var_dump($user);
            echo json_encode('{"status":"success","message": "Logged in successfully","id": "'.$user->id.'","email":"'.$user->email.'","name":"'.$user->name.'"}');
        }
        else {
            echo json_encode('{"status":"error","message": "Could not log you in, please check if your email and password are correct"}');
        }
        
        jexit();

    }
   
    /*
    * 
    * Request for all businesses,
    * index.php?option=com_members&task=api.listbiz
    * TODO: add a pagination token so we get only at least 20 items at time instead of fetching the whole database
    */
    function listbiz(){
        $bizmodel = parent::getModel('Businessinfos', 'MembersModel', array('ignore_request' => true)); 
        //$bizmodel->setState('filter.search','Test Sunday');
        $items = $bizmodel->getItems();
        //print_r($items);
        echo json_encode($items);

        jexit();
            
    }
    function uploadbizimages_old(){
        $bizmodel = parent::getModel('Businessinfo', 'MembersModel', array('ignore_request' => true));
        $bizid = $this->input->get('biz_id');

        $logo = $this->input->files->get('logo');
        $profile = $this->input->files->get('profile');
        $product_services_img = $this->input->files->get('product_services');
        $facilities_img = $this->input->files->get('facilities');

        $bizmodel->uploadimg($logo, $bizid, 'logo');
        $bizmodel->setNewProfilePicture($profile, $bizid);

        $bizmodel->uploadimg($product_services_img, $bizid, 'product_services_img');
        $bizmodel->uploadimg($facilities_img, $bizid, 'facilities_img');
        
        /*if ($bizmodel->setNewProfilePicture($profile, $bizid)){
            echo json_encode('{"status":"success","message": "Images successfully uploaded"}');
        }
        else {
            echo json_encode('{"status":"error","message": "Images not uploaded"}');
        }
        */
        //jexit();
    }
    function uploadbizimages(){
        $bizmodel = parent::getModel('Businessinfo', 'ShetradesModel', array('ignore_request' => true));
        $bizid = $this->input->get('biz_id');

        $logo = $this->input->get('logo',null,'string');
        $profile = $this->input->get('profile',null,'string');
        $product_services_img = $this->input->get('product_service',null,'string');
        $facilities_img = $this->input->get('facilities',null,'string');
        if (isset($profile)){
            //$bizmodel->uploadimg($logo, $bizid, 'profile');
            $bizmodel->setNewProfilePicture($profile, $bizid);
        }
        if (isset($logo)){
            $bizmodel->uploadimg($logo, $bizid, 'logo');
        }
        if (isset($product_services_img)){
            $bizmodel->uploadimg($product_services_img, $bizid, 'product_services_img');
        }
        if (isset($facilities_img)){
            $bizmodel->uploadimg($facilities_img, $bizid, 'facilities_img');
        }

        //jexit();
    }
    /*
    *
    * Request one business -> action param defines what we want to do with it
    * actions::-> show,delete,edit,like,notify,match,
    * TODO: - Work on these actions on the model side
    */

    function biz(){
        $bizmodel = parent::getModel('Businessinfo', 'ShetradesModel', array('ignore_request' => true));
        //$input = JFactory::getInput();
        $bizid = $this->input->get('biz_id');
        $userid = $this->input->get('user_id');
        $action = $this->input->get('action');
        switch ($action) {
            case 'show':
                $item = $bizmodel->getData($bizid);
                //print_r($item);
                echo json_encode($item);
                jexit();
                break;
            case 'edit':
                $do = $bizmodel->edit($bizid);
                if ($do):
                    $message = '{message:"edited successfully"}';
                    echo json_encode($message);
                endif;
                jexit();
                break;
            case 'delete':
                $do = $bizmodel->delete($bizid);
                if ($do):
                   $message = '{"message":"deleted successfully"}';
                   echo json_encode($message);
                endif;
                jexit();
                break;
            case 'notify':
                $do = $bizmodel->notify($bizid);
                if ($do):
                   $message = '{"message":"notification sent"}';
                   echo json_encode($message);
                endif;
                jexit();
                break;
            case 'like':
                $do = $bizmodel->like($bizid,$userid);
                if ($do):
                   $message = '{"message":"liked successfully"}';
                   echo json_encode($message);
                endif;
                jexit();
                break;
            case 'match':
                $fav_id = $this->input->get('favorite_id');
                $do = $bizmodel->match($fav_id,$bizid,$userid);
                if ($do){
                    $message = '{"status":"success","message":"matched successfully"}';
                    echo json_encode($message);
                }
                else {
                    $message = '{"status":"error","message":"unable to match"}';
                    echo json_encode($message);
                }
                jexit();
                break;
            default: 
                // show message that action is needed
                echo json_encode('{"default": "no action"}');
            break;
        }

        
    }
    /*
    *
    *
    */
    function bizmessage(){
        $id = $this->input->get('id');
        $action = $this->input->get('action');
        switch ($action) {
            case 'delete':
                break;
            case 'markdelivered':
                $do = ShetradesHelperNotifications::markBizMessageDelivered($id);
                if ($do){
                    $message = '{"status":"success","message":"notification marked as delivered"}';
                    echo json_encode($message);
                }
                else {
                    $message = '{"status":"error","message":"unable to mark as delivered"}';
                    echo json_encode($message);
                }
                jexit();
                break;
            default: 
                // show message that action is needed
                echo json_encode('{"default": "no action"}');
            break;
        }

    }
    function buyermessage(){
        $id = $this->input->get('id');
        $action = $this->input->get('action');
        switch ($action) {
            case 'delete':
                break;
            case 'markdelivered':
                $do = ShetradesHelperNotifications::markBuyerMessageDelivered($id);
                if ($do){
                    $message = '{"status":"success","message":"notification marked as delivered"}';
                    echo json_encode($message);
                }
                else {
                    $message = '{"status":"error","message":"unable to mark as delivered"}';
                    echo json_encode($message);
                }
                jexit();
                break;
            default: 
                // show message that action is needed
                echo json_encode('{"default": "no action"}');
            break;
        }

    }
    function fetchbizmessages(){
        $bizid = $this->input->get('biz_id');
        $notifs = ShetradesHelperNotifications::getBizMessages($bizid);
        //print_r($notifs);
        header('Cache-Control: no-cache'); 
        echo json_encode($notifs);
        jexit();

    }
    function fetchbuyermessages(){
        $userid = $this->input->get('user_id');
        $notifs = ShetradesHelperNotifications::getBuyerMessages($userid);
        //print_r($notifs);
        header('Cache-Control: no-cache'); 
        echo json_encode($notifs);
        jexit();

    }
    /*
    * Server side real-time notifications
    * Client needs a js sse listener
    */
    function streamnotifications(){    
        $type = $this->input->get('type');
        if ($type == 'buyer'){
            $userid = $this->input->get('user_id');
            $notifs = ShetradesHelperNotifications::getBuyerMessages($userid);
            $res =json_encode($notifs);
            header('Content-Type: text/event-stream');
            header('Cache-Control: no-cache');         
            echo "data:{$res}\n\n";
            flush();
        }
        if ($type == 'biz'){ 
            $bizid = $this->input->get('biz_id');
            $notifs = ShetradesHelperNotifications::getBizMessages($bizid);  
            $res =json_encode($notifs);
            header('Content-Type: text/event-stream');
            header('Cache-Control: no-cache');
            echo "data:{$res}\n\n";
            flush();
        }
        // exit joomla
        jexit();
    }
    /*
    * Search for a business or businesses given some keywords to search
    * $searchmethod is simple or advanced.
    * - Advanced Search filters the results by multiple params
    * - Simple Search just searches the business name
    * - search params will be in the query string
    */
    function searchbiz(){
        $bizmodel = parent::getModel('Businessinfos', 'ShetradesModel', array('ignore_request' => true)); 
        $searchmethod = $this->input->get('method');
        switch ($searchmethod){
            case 'simple':
                $keywords = $this->input->get('keywords');
                $bizmodel->setState('filter.search',$keywords);
                $items = $bizmodel->getItems(); 
                echo json_encode($items);
                jexit();

            break;
            case 'advanced':
                 $searchparams = $this->input->getArray(
                    array('country'=>'','sell_services' =>'','sell_products'=>'','buy_services' =>'','buy_products'=>'','percent_women_owned'=>'', 'verified'=>'','exporting_experience'=>'')
                 );
                 if ($searchparams['country'] != 'all'){
                    $bizmodel->setState('filter.country',$searchparams['country']);
                 }
                 
                 $bizmodel->setState('filter.perc_owned_by_woman',$searchparams['percent_women_owned']);
                 $bizmodel->setState('filter.no_of_employees',$searchparams['employees']);
                 $bizmodel->setState('filter.year_of_exports',$searchparams['exporting_experience']);
                 //$bizmodel->setState('filter.verified',$searchparams['verified']);
                 
                 //print_r($searchparams);
                 //print_r($bizmodel->getState());
                 $items = $bizmodel->getItems();
                 $searcheditems = array();
                 //print_r(array$items);
                 // because we can't set the state of products and services to search in the model, let's do our own filtering here
                 foreach ($items as $item){
                    $item_arr = get_object_vars($item); // format to array to enable us search peacefully
                    $services_o = $this->search_in_array($searchparams['sell_services'], $item_arr['services_offering']); 
                    $products_o = $this->search_in_array($searchparams['sell_products'], $item_arr['products_offering']);
                    $services_b = $this->search_in_array($searchparams['buy_services'], $item_arr['services_buying']); 
                    $products_b = $this->search_in_array($searchparams['buy_products'], $item_arr['products_buying']);
       
                    if ($services_o !== null || $products_o !== null || $services_b !== null || $products_b == null) {
                        array_push($searcheditems, $item);
                    }
                    
                 } 
                 //print_r($searcheditems);
                 //print_r($items);
                 //header('Content-Type: application/json');
                 echo json_encode($searcheditems);
                 jexit();
            break;
            deault:
            break;
        }       

    }
    function searcharray($value, $key, $array) {
       foreach ($array as $k => $val) {
           if ($val[$key] == $value) {
               return $k;
           }
       }
       return 'not found';
    }
    function search_in_array($value, $array) {
       if ($value == 'any'){
            return '1';
       }
       else {
            foreach ($array as $k => $val) {
               if ($val == $value) {
                   return $k;
               }
               else {
                   return null;
               }
            }
            //return null;
       }
       
    }
    function registerverifier(){
        jimport('joomla.input.json');
        $jsoninput = new JInputJSON();
        $data = $jsoninput->getArray();
        //print_r($data);
        $registered = ShetradesHelperAccreditors::register($data);
        if ($registered){
            echo json_encode('{"status":"success"}');
        }
        else {
            echo json_encode('{"status":"error"}');
        }   
        jexit();


    }

    /*
    * register a business remotely
    * saves and returns the id as specified by JTable, we can use this id for further inserts
    */

    function registerbiz(){
        jimport('joomla.input.json');
        $jsoninput = new JInputJSON();
        $rawdata = $jsoninput->getRaw();
        $data = $jsoninput->getArray();
    
        $bizmodel = parent::getModel('Businessinfo', 'ShetradesModel', array('ignore_request' => true));
        
        $saved = $bizmodel->remoteSave($data);
        if ($saved){
            echo json_encode('{"status": "success","message": "business saved successfully","id": "'.$saved.'"}');
        }
        else {
            echo json_encode('{"status": "error","message": "business was not saved","id": "'.$saved.'"}');
        }
        
        jexit();
        
    }
    /*
    * Fetch all products
    */
    function fetchproducts(){
        $model = parent::getModel('Products', 'ShetradesModel', array('ignore_request' => true));
        $model->setState('list.limit','100');
        $items = $model->getItems();
        //print_r($items);
        echo json_encode($items);

        jexit();
    }
    /*
    * Fetch all services
    */
    function fetchservices(){
        $model = parent::getModel('Services', 'ShetradesModel', array('ignore_request' => true));
        //$model->setState('list.limit','50');
        $items = $model->getItems();
        //print_r($items);
        echo json_encode($items);

        jexit();
    }
    /*
    * Fetch all countries
    */
    function fetchcountries(){
        $model = parent::getModel('Countries', 'ShetradesModel', array('ignore_request' => true));
        //$model->setState('list.limit','50');
        $items = $model->getItems();
        //print_r($items);
        echo json_encode($items);

        jexit();
    }
    /*
    * Fetch all accreditors
    * users in accreditors group
    * Heavy lifting moved to Helper class
    */
    
    function fetchaccreditors(){
        // this will return an array containing accreditors
        $accreditors = ShetradesHelperAccreditors::getAccreditors();
        echo json_encode($accreditors);
        jexit();
    }
    function fetchcertifications(){
        // this will return an array 
        $certs = ShetradesHelperCertifications::getCertificates();
        echo json_encode($certs);
        jexit();
    }
    /*
    * Function to test html5 server sent events
    */
    function updateview(){
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $time = date('r');
        echo "data: The server time is: {$time}\n\n";
        flush();
        //jexit();
    }
    function getmatches(){
        $user_id = $this->input->get('user_id');
        $matches = ShetradesHelperMatch::getmatches($user_id);
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        echo "data:{$matches}\n\n";
        flush();
        jexit();
    }
    function match(){
        $action = $this->input->get('action');
        $id = $this->input->get('match_id');
        if ($action == 'seen'){
            $do = ShetradesHelperMatch::markAsSeen($id);
            if ($do){
                echo json_encode('{"status":"success","message": "marked successfully as seen"}');
            }
            else {
                echo json_encode('{"status":"error","message": "unable to mark as seen"}');
            }

        }
        jexit();

    }
    function remove_account(){
        $user_id = $this->input->get('user_id');
        echo json_encode('{"status":"success","message": "your account has successfully been deleted"}');
        jexit();
    }
    /*
    * Messaging System 
    */
    function logmessage(){
        jimport('joomla.input.json');
        $jsoninput = new JInputJSON();
        $rawdata = $jsoninput->getRaw();
        $data = $jsoninput->getArray();
        $save = ShetradesHelperMessaging::saveMessage($data);
        if ($save){
            echo json_encode('{"status":"success","message": "message sent successfully"}');
        }
        else {
            echo json_encode('{"status":"error","message": "message not sent"}');
        }
        jexit();

    }
    /* 
    * By specifying the recipient in a thread, we'll get only the messages that the user is entitled to receive, 
    * and not the ones he's also sent
    * returns only unread messages
    */
    function streamchat(){
        $thread = $this->input->get('thread');
        $recipient = $this->input->get('recipient');
        $chat = ShetradesHelperMessaging::getUnreadConversation($recipient,$thread);
        $res = json_encode($chat);
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        echo "data:{$res}\n\n";
        flush();
        jexit();
    }
    /*
    * inbound, received messages
    */
    function fetch_user_in_messages(){
        $user_id = $this->input->get('user_id');
        $messages = ShetradesHelperMessaging::getInbox($user_id);
        $res = json_encode($messages);
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        echo "data:{$res}\n\n";
        flush();
        jexit();

    }
    /*
    * Fetching a single thread, using the thread column e.g 852_854
    * the first number shows the initiator of the thread, and the second the other person in the thread
    * two users can only maintain one conversation thread, with the recipient and sender being swapped as appropriate in the tree
    */
    function fetchconversation(){
        $thread = $this->input->get('thread');
        //$recipient = $this->input->get('recipient');
        //$sender = $this->input->get('sender');
        $messages = ShetradesHelperMessaging::getConversation($thread);
        echo json_encode($messages);
        jexit();

    }
    /*
    * outbound, sent messages
    */
    function fetch_user_out_messages(){
        $user_id = $this->input->get('user_id');
        $messages = ShetradesHelperMessaging::getOutbox($user_id);
        echo json_encode($messages);
        jexit();

    }
    function fetchuserbiz(){
        $user_id = $this->input->get('user_id');
        $mybiz = ShetradesFrontendHelper::getUserBusinesses($user_id);
        echo json_encode($mybiz);
        jexit();
    }
    function markchatmessagereceived(){
        $id = $this->input->get('id');
        $update = ShetradesHelperMessaging::markReceived($id);
        jexit();
    }
    function generateusers(){
        $items = ShetradesFrontendHelper::generateUsersforBiz();
        echo '<pre>';
        print_r($items);
        echo '</pre>';
        jexit();
    }

// end of class
}