var $$ = jQuery.noConflict();
var ServerUri = 'gbc.me.ke/eabc_latest';

var myApp;
myApp = myApp || (function () {
    var indicator = $$('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
    var msgpopup = $$('<div class="modal hide" id="sendMessage" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Modal Form comes here</h1></div><div class="modal-body"></div></div></div>');
    return {
        showIndicator: function() {
            indicator.modal();
        },
        hideIndicator: function () {
            indicator.modal('hide');
        },
        showMessageModal: function (){
            msgpopup.modal();
        }

    };
})();

$$(document).ready(function(){
  $$('.add-to-fav').on('click', function(){
	  console.log('fav button clicked');
	  var bizid = $$(this).data('biz_id');
	  var userid = $$(this).data('user_id');
	  $$.post(ServerUri+'index.php?option=com_members&task=api.biz&action=like&biz_id='+bizid+'&user_id='+userid,function(data){
	         console.log(JSON.parse(data));
	         // add this to favorites list
	         //storeFavs(profile);
	  });
  });
  
  // handle mark as read, and change the status on server to delivered
  $$('.biz-match-dismiss').on('click',function(){
     var nid = $$(this).data('id');
     $$('.buyer-notifs > #list-group-item-'+nid).hide('slow');
     markBuyerMessageDelivered(nid);
  })
  $$('.biz-like-connect').on('click',function(){
     var nid = $$(this).data('id');
     var url = $$(this).data('url');
     $$.post(url,function(result){
        var data = JSON.parse(result);
        var resp = JSON.parse(data)
        if (resp.status == 'success'){
          markBizMessageDelivered(nid);
          $$('#biz-like-buttons-'+nid).html('<div class="alert alert-success" role="alert"><i class="fa fa-check"></i> Successfully Connected</div>');
        }
     })
  })
  $$('.biz-like-dismiss').on('click',function(){
     var nid = $$(this).data('id');
     markBizMessageDelivered(nid);
     // remove the notification from view
     $$('.biz-notifs > #list-group-item-'+nid).hide('slow');
  })
  
  // send msg to matched biz
  $$('.send-message-to-match').on('click',function(){
   var dataset = $$(this).data();
   markBuyerMessageDelivered(dataset.id);
   console.log(dataset);  
   messagePopup(dataset); 
  })

  $$('.send-msg-from-thread').on('click',function(){
   var dataset = $$(this).data();
   //markBuyerMessageDelivered(dataset.id);
   console.log(dataset);  
   var message = $$('#thread-reply-input').val();
   sendMessage(dataset.sender,dataset.recipient,message,function(){
       //console.log('callback here');
       location.reload();
   }); 
  })






})

// functions
function messagePopup(dataset){
 var mymodal = '<div class="modal fade" tabindex="-1" role="dialog">';
  mymodal +='<div class="modal-dialog">';
    mymodal +='<div class="modal-content">';
      mymodal +='<div class="modal-header">';
        mymodal +='<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        mymodal +='<h4 class="modal-title">Send Message to: '+dataset.bizname+'</h4>';
      mymodal +='</div>';
      mymodal +='<div class="modal-body">';
        mymodal +='<textarea placeholder="write your message here..." id="send-match-message-input" rows="10" style="width:100%;height:150px;"></textarea>';
      mymodal +='</div>';
      mymodal +='<div class="modal-footer">';
        mymodal +='<button type="button" class="btn btn-primary modal-send-msg">Send Message</button>';
      mymodal +='</div>';
    mymodal +='</div>';
  mymodal +='</div>';
mymodal +='</div>';

 $$(mymodal).modal();
 $$(document).on('click','.modal-send-msg',function(){
   var message = $$('#send-match-message-input').val();
   sendMessage(dataset.sender,dataset.recipient,message);
 })
}
function sendMessage(sender_id,receiver_id,message_text,callBack){
   // close all modals
   $$('.modal').modal('hide');
   var obj_message = {sender: sender_id,receiver : receiver_id,message : message_text};
   var json_message = JSON.stringify(obj_message);
   $$.ajax({
      url: ServerUri+'index.php?option=com_members&task=api.logmessage',
      method: 'POST',
      data: json_message,
      dataType: 'text',
      crossDomain: true,
      beforeSend: function(xhr){
        myApp.showIndicator();
      },
      error: function(xhr,status){
        myApp.hideIndicator();
        if(xhr.status == 500){
           alert('The server returned an error 500');
        }
        else if(xhr.status == 404) {
           alert('The server returned an error 404');
        }
        else {
           alert('Error: '+xhr.responseText);
        }        
      },
      complete: function(xhr,status){
        myApp.hideIndicator();
      },
      success: function(result, status, xhr){
        // had to parse this twice to make javascript happy
        var resp = JSON.parse(JSON.parse(result));
        if(resp instanceof Object) {    
            // alert the message from json response
             
            if (callBack) {
               callBack();
            }
            else {
               alert(resp.message);
            }                  
        }
        else {
          //seems response isnot an object, the server returned some bad data!
          alert('response could not be parsed');
          //console.log('response could not be parsed');
        }         
      }
    });
  // end ajax
  
}
function markBizMessageDelivered(key){
  $$.ajax({
      url: ServerUri+'index.php?option=com_members&task=api.bizmessage&action=markdelivered&id='+key,
      method: 'POST',
      dataType: 'text',
      crossDomain: true,
      beforeSend: function(xhr){
        myApp.showIndicator();
      },
      error: function(xhr,status){
        myApp.hideIndicator();
        if(xhr.status == 500){
           alert('The server returned an error 500');
        }
        else if(xhr.status == 404) {
           alert('The server returned an error 404');
        }
        else {
           alert('Error: '+xhr.responseText);
        }        
      },
      complete: function(xhr,status){
        myApp.hideIndicator();
      },
      success: function(result, status, xhr){
        // had to parse this twice to make javascript happy
        var resp = JSON.parse(JSON.parse(result)); 
        if(resp instanceof Object) {    
            // alert the message from json response
            //alert(resp.message);                   
        }
        else {
          //seems response is not an object, the server returned some bad data!
          alert('response could not be parsed');
          //console.log('response could not be parsed');
        }         
      }
    });
  // end ajax
}

function markBuyerMessageDelivered(key){
    $$.ajax({
      url: ServerUri+'index.php?option=com_members&task=api.buyermessage&action=markdelivered&id='+key,
      method: 'POST',
      dataType: 'text',
      crossDomain: true,
      beforeSend: function(xhr){
        myApp.showIndicator();
      },
      error: function(xhr,status){
        myApp.hideIndicator();
        if(xhr.status == 500){
           alert('The server returned an error 500');
        }
        else if(xhr.status == 404) {
           alert('The server returned an error 404');
        }
        else {
           alert('Error: '+xhr.responseText);
        }        
      },
      complete: function(xhr,status){
        myApp.hideIndicator();
      },
      success: function(result, status, xhr){
        // had to parse this twice to make javascript happy
        var resp = JSON.parse(JSON.parse(result)); 
        if(resp instanceof Object) {    
            // alert the message from json response
            //alert(resp.message);                   
        }
        else {
          //seems response isnot an object, the server returned some bad data!
          alert('response could not be parsed');
          //console.log('response could not be parsed');
        }         
      }
    });
  // end ajax

}
