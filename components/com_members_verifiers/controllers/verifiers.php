<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Shetrades_verifiers
 * @author     GreenBell Communications <michael@buluma.me.ke>
 * @copyright  Copyright (C) 2015. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * Verifiers list controller class.
 *
 * @since  1.6
 */
class Members_verifiersControllerVerifiers extends Members_verifiersController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Verifiers', $prefix = 'Members_verifiersModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
