<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Shetrades_verifiers
 * @author     GreenBell Communications <michael@buluma.me.ke>
 * @copyright  Copyright (C) 2015. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');

/**
 * com_members_verifiers model.
 *
 * @since  1.6
 */
class Members_verifiersModelVerifierForm extends JModelForm
{
	private $item = null;

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return void
	 *
	 * @since  1.6
	 */
	protected function populateState()
	{
		$app = JFactory::getApplication('com_members_verifiers');

		// Load state from the request userState on edit or from the passed variable on default
		if (JFactory::getApplication()->input->get('layout') == 'edit')
		{
			$id = JFactory::getApplication()->getUserState('com_members_verifiers.edit.verifier.id');
		}
		else
		{
			$id = JFactory::getApplication()->input->get('id');
			JFactory::getApplication()->setUserState('com_members_verifiers.edit.verifier.id', $id);
		}

		$this->setState('verifier.id', $id);

		// Load the parameters.
		$params       = $app->getParams();
		$params_array = $params->toArray();

		if (isset($params_array['item_id']))
		{
			$this->setState('verifier.id', $params_array['item_id']);
		}

		$this->setState('params', $params);
	}

	/**
	 * Method to get an ojbect.
	 *
	 * @param   integer  $id  The id of the object to get.
	 *
	 * @return Object|boolean Object on success, false on failure.
	 *
	 * @throws Exception
	 */
	public function &getData($id = null)
	{
		if ($this->item === null)
		{
			$this->item = false;

			if (empty($id))
			{
				$id = $this->getState('verifier.id');
			}

			// Get a level row instance.
			$table = $this->getTable();

			// Attempt to load the row.
			if ($table !== false && $table->load($id))
			{
				$user = JFactory::getUser();
				$id   = $table->id;
				$canEdit = $user->authorise('core.edit', 'com_members_verifiers') || $user->authorise('core.create', 'com_members_verifiers');

				if (!$canEdit && $user->authorise('core.edit.own', 'com_members_verifiers'))
				{
					$canEdit = $user->id == $table->created_by;
				}

				if (!$canEdit)
				{
					throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'), 500);
				}

				// Check published state.
				if ($published = $this->getState('filter.published'))
				{
					if ($table->state != $published)
					{
						return $this->item;
					}
				}

				// Convert the JTable to a clean JObject.
				$properties  = $table->getProperties(1);
				$this->item = JArrayHelper::toObject($properties, 'JObject');
			}
		}

		return $this->item;
	}

	/**
	 * Method to get the table
	 *
	 * @param   string  $type    Name of the JTable class
	 * @param   string  $prefix  Optional prefix for the table class name
	 * @param   array   $config  Optional configuration array for JTable object
	 *
	 * @return  JTable|boolean JTable if found, boolean false on failure
	 */
	public function getTable($type = 'Verifier', $prefix = 'Members_verifiersTable', $config = array())
	{
		$this->addTablePath(JPATH_ADMINISTRATOR . '/components/com_members_verifiers/tables');

		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Get an item by alias
	 *
	 * @param   string  $alias  Alias string
	 *
	 * @return int Element id
	 */
	public function getItemIdByAlias($alias)
	{
		$table = $this->getTable();

		$table->load(array('alias' => $alias));

		return $table->id;
	}

	/**
	 * Method to check in an item.
	 *
	 * @param   integer  $id  The id of the row to check out.
	 *
	 * @return  boolean True on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function checkin($id = null)
	{
		// Get the id.
		$id = (!empty($id)) ? $id : (int) $this->getState('verifier.id');

		if ($id)
		{
			// Initialise the table
			$table = $this->getTable();

			// Attempt to check the row in.
			if (method_exists($table, 'checkin'))
			{
				if (!$table->checkin($id))
				{
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Method to check out an item for editing.
	 *
	 * @param   integer  $id  The id of the row to check out.
	 *
	 * @return  boolean True on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function checkout($id = null)
	{
		// Get the user id.
		$id = (!empty($id)) ? $id : (int) $this->getState('verifier.id');

		if ($id)
		{
			// Initialise the table
			$table = $this->getTable();

			// Get the current user object.
			$user = JFactory::getUser();

			// Attempt to check the row out.
			if (method_exists($table, 'checkout'))
			{
				if (!$table->checkout($user->get('id'), $id))
				{
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Method to get the profile form.
	 *
	 * The base form is loaded from XML
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return    JForm    A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_members_verifiers.verifier', 'verifierform', array(
			'control'   => 'jform',
			'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return    mixed    The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_members_verifiers.edit.verifier.data', array());

		if (empty($data))
		{
			$data = $this->getData();
		}

		

		return $data;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data
	 *
	 * @return bool
	 *
	 * @throws Exception
	 * @since 1.6
	 */
	public function save($data)
	{
		$id    = (!empty($data['id'])) ? $data['id'] : (int) $this->getState('verifier.id');
		$state = (!empty($data['state'])) ? 1 : 0;
		$user  = JFactory::getUser();

		if ($id)
		{
			// Check the user can edit this item
			$authorised = $user->authorise('core.edit', 'com_members_verifiers') || $authorised = $user->authorise('core.edit.own', 'com_members_verifiers');

			if ($user->authorise('core.edit.state', 'com_members_verifiers') !== true && $state == 1)
			{
				// The user cannot edit the state of the item.
				$data['state'] = 0;
			}
		}
		else
		{
			// Check the user can create new items in this section
			$authorised = $user->authorise('core.create', 'com_members_verifiers');

			if ($user->authorise('core.edit.state', 'com_members_verifiers') !== true && $state == 1)
			{
				// The user cannot edit the state of the item.
				$data['state'] = 0;
			}
		}

		if ($authorised !== true)
		{
			throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'), 403);
		}

		$table = $this->getTable();

		if ($table->save($data) === true)
		{
			//send email
		        $app = JFactory::getApplication();	  
		        $db = JFactory::getDBO();
                        $app_data = JFactory::getApplication()->input;
                        $data_info = $app_data->post->getArray();
				
			$mailfrom = $app->get('mailfrom');
			$fromname = $app->get('fromname');
			$sitename = $app->get('sitename');
			
			$name		= $data_info['jform']['org_name'];
			$email	= JstringPunycode::emailToPunycode($data_info['jform']['email']);
			//$email		= 'michael@buluma.me.ke';
			$country	= $data_info['jform']['country'];
			$representative	= $data_info['jform']['shetrades_rep'];
			$type_of_org	= $data_info['jform']['org_type'];
			$subject	= 'Verifier Sign up: ' .$name;
			$website	= $data_info['jform']['website'];
			$ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
			$link_back = JUri::base().'index.php?option=com_members_verifiers&task=verifier.edit&id='.$data['id'];
			
			$style = 'font-family:arial;font-size:12px;';			

			// Prepare email body
			$prefix = JText::sprintf('COM_CONTACT_ENQUIRY_TEXT', JUri::base());
			$body = '<p style="font-family:Helvetica;font-size:12px">Dear admin,</p>';
			$body .= '<p style="font-family:Helvetica;font-size:12px">A new verifier has siged up on EABC. Please find the details below.</p>';
			$body .= '<table border="1" cellpadding="4" style="border-collapse:collapse;font-family:Helvetica;font-size:12px">';//end table
			
			$body .= '<tr><td><strong>Name of Organisation</strong></td><td>' .$name. '</td></tr>';//Name of Organisation
			$body .= '<tr><td><strong>Representative at EABC</strong></td><td>' .$representative. '</td></tr>';//Representative at EABC
			$body .= '<tr><td><strong>Email</strong></td><td>' .$email. '</td></tr>';//Email
			$body .= '<tr><td><strong>Type of organisation</strong></td><td>' .$type_of_org. '</td></tr>';//Type of organisation
			$body .= '<tr><td><strong>Country</strong></td><td>' .$country. '</td></tr>';//Country
			$body .= '<tr><td><strong>Website</strong></td><td>' .$website. '</td></tr>';//Website
			$body .= '<tr><td><strong>IP</strong></td><td>' .$ip. '</td></tr>';//IP
			//$body .= '<tr><td><strong>Link</strong></td><td>' .$link_back. '</td></tr>';//IP
			$body .= '</table>';
			$body .= '<br >';
			//$body .= '<a href="#" style="font-family:Helvetica;font-size:12px">Verify this business</a>';
			//$body .= '<a href="index.php?option=com_shetrades_verifiers&task=verifier.edit&id='.$data['id'].'" style="font-family:Helvetica;font-size:12px">Verify this business</a>';
			
			

			$mail = JFactory::getMailer();
			$mail->addRecipient('mbuluma@gbc.co.ke');
			//$mail->addReplyTo(array($email, $name));
			$mail->setSender(array($mailfrom, $fromname));

			$mail->setSubject($sitename. ' '.$subject);
			//$mailer->isHTML(true);
			$mail->MsgHTML($body);
			$sent = $mail->Send();
		        //end send email
			return $table->id;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Method to delete data
	 *
	 * @param   array  $data  Data to be deleted
	 *
	 * @return bool|int If success returns the id of the deleted item, if not false
	 *
	 * @throws Exception
	 */
	public function delete($data)
	{
		$id = (!empty($data['id'])) ? $data['id'] : (int) $this->getState('verifier.id');

		if (JFactory::getUser()->authorise('core.delete', 'com_members_verifiers') !== true)
		{
			throw new Exception(403, JText::_('JERROR_ALERTNOAUTHOR'));
		}

		$table = $this->getTable();

		if ($table->delete($data['id']) === true)
		{
			return $id;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Check if data can be saved
	 *
	 * @return bool
	 */
	public function getCanSave()
	{
		$table = $this->getTable();

		return $table !== false;
	}
	
}
