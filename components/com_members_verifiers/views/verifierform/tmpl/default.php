<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Shetrades_verifiers
 * @author     GreenBell Communications <michael@buluma.me.ke>
 * @copyright  Copyright (C) 2015. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_members_verifiers', JPATH_SITE);
$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . '/components/com_members_verifiers/assets/js/form.js');

/**/
?>
<script type="text/javascript">
	if (jQuery === 'undefined') {
		document.addEventListener("DOMContentLoaded", function (event) {
			jQuery('#form-verifier').submit(function (event) {
				
			});

			
		});
	} else {
		jQuery(document).ready(function () {
			jQuery('#form-verifier').submit(function (event) {
				
			});

			
		});
	}
</script>

<div class="verifier-edit front-end-edit">
	<?php if (!empty($this->item->id)): ?>
		<h1>Edit <?php echo $this->item->id; ?></h1>
	<?php else: ?>
		<h1><?php echo JText::_('COM_SHETRADES_VERIFIERS_ADD_ITEM'); ?></h1>
	<?php endif; ?>

	<form id="form-verifier"
		  action="<?php echo JRoute::_('index.php?option=com_members_verifiers&task=verifier.save'); ?>"
		  method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
		
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />

	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />

	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

	<?php if(empty($this->item->created_by)): ?>
		<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />
	<?php else: ?>
		<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />
	<?php endif; ?>
	
	<div class="form-group row">
        <label for="jform_org_name" class="col-sm-3 form-control-label">Name of Organisation<span class="star">&nbsp;*</span></label>
        <div class="col-sm-9">
          <?php echo $this->form->getInput('org_name'); ?>
        </div>
      </div>
      
      <div class="form-group row">
        <label for="jform_org_name" class="col-sm-3 form-control-label"><?php echo $this->form->getLabel('shetrades_rep'); ?></label>
        <div class="col-sm-9">
          <?php echo $this->form->getInput('shetrades_rep'); ?>
        </div>
      </div>
      
      <div class="form-group row">
        <label for="jform_org_name" class="col-sm-3 form-control-label"><?php echo $this->form->getLabel('email'); ?></label>
        <div class="col-sm-9">
          <?php echo $this->form->getInput('email'); ?>
        </div>
      </div>
      
      <div class="form-group row">
        <label for="jform_org_name" class="col-sm-3 form-control-label"><?php echo $this->form->getLabel('org_type'); ?></label>
        <div class="col-sm-9">
          <?php echo $this->form->getInput('org_type'); ?>
        </div>
      </div>
      
      <div class="form-group row">
        <label for="jform_org_name" class="col-sm-3 form-control-label"><?php echo $this->form->getLabel('country'); ?></label>
        <div class="col-sm-9">
          <?php echo $this->form->getInput('country'); ?>
        </div>
      </div>
      
      <div class="form-group row">
        <label for="jform_org_name" class="col-sm-3 form-control-label"><?php echo $this->form->getLabel('website'); ?></label>
        <div class="col-sm-9">
          <?php echo $this->form->getInput('website'); ?>
        </div>
      </div>
		<div class="control-group">
			<div class="controls">

				<?php if ($this->canSave): ?>
					<button type="submit" class="validate btn btn-primary">
						<?php echo JText::_('JSUBMIT'); ?>
					</button>
				<?php endif; ?>
				<a class="btn"
				   href="<?php echo JRoute::_('index.php?option=com_members_verifiers&task=verifierform.cancel'); ?>"
				   title="<?php echo JText::_('JCANCEL'); ?>">
					<?php echo JText::_('JCANCEL'); ?>
				</a>
			</div>
		</div>

		<input type="hidden" name="option" value="com_members_verifiers"/>
		<input type="hidden" name="task"
			   value="verifierform.save"/>
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>
