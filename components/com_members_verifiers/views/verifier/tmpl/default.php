<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Shetrades_verifiers
 * @author     GreenBell Communications <michael@buluma.me.ke>
 * @copyright  Copyright (C) 2015. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_members_verifiers');
if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_members_verifiers')) {
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>
<?php if ($this->item) : ?>

	<div class="item_fields">
		<table class="table">
			<tr>
			<th><?php echo JText::_('COM_SHETRADES_VERIFIERS_FORM_LBL_VERIFIER_ID'); ?></th>
			<td><?php echo $this->item->id; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_VERIFIERS_FORM_LBL_VERIFIER_STATE'); ?></th>
			<td>
			<i class="icon-<?php echo ($this->item->state == 1) ? 'publish' : 'unpublish'; ?>"></i></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_VERIFIERS_FORM_LBL_VERIFIER_CREATED_BY'); ?></th>
			<td><?php echo $this->item->created_by_name; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_VERIFIERS_FORM_LBL_VERIFIER_ORG_NAME'); ?></th>
			<td><?php echo $this->item->org_name; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_VERIFIERS_FORM_LBL_VERIFIER_SHETRADES_REP'); ?></th>
			<td><?php echo $this->item->shetrades_rep; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_VERIFIERS_FORM_LBL_VERIFIER_EMAIL'); ?></th>
			<td><?php echo $this->item->email; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_VERIFIERS_FORM_LBL_VERIFIER_ORG_TYPE'); ?></th>
			<td><?php echo $this->item->org_type; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_VERIFIERS_FORM_LBL_VERIFIER_COUNTRY'); ?></th>
			<td><?php echo $this->item->country; ?></td>
</tr>
<tr>
			<th><?php echo JText::_('COM_SHETRADES_VERIFIERS_FORM_LBL_VERIFIER_WEBSITE'); ?></th>
			<td><?php echo $this->item->website; ?></td>
</tr>

		</table>
	</div>
	<?php if($canEdit && $this->item->checked_out == 0): ?>
		<a class="btn" href="<?php echo JRoute::_('index.php?option=com_members_verifiers&task=verifier.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_SHETRADES_VERIFIERS_EDIT_ITEM"); ?></a>
	<?php endif; ?>
								<?php if(JFactory::getUser()->authorise('core.delete','com_members_verifiers')):?>
									<a class="btn" href="<?php echo JRoute::_('index.php?option=com_members_verifiers&task=verifier.remove&id=' . $this->item->id, false, 2); ?>"><?php echo JText::_("COM_SHETRADES_VERIFIERS_DELETE_ITEM"); ?></a>
								<?php endif; ?>
	<?php
else:
	echo JText::_('COM_SHETRADES_VERIFIERS_ITEM_NOT_LOADED');
endif;
?>

<div class="page-header">
  <h1>Businesses affiliated to this organisation</h1>
</div>

<div class="table-responsive">
<table class="table table-striped" id="verifierBizList">
  <thead>
    <tr>
      <th width="1%" class="nowrap center hidden-phone"> Biz ID</th>
      <th class=''> <?php echo 'Name'; ?> </th>
      <th class=''> <?php echo 'Verified'; ?> </th>
    </tr>
  </thead>
  <!-- end head -->
  <tbody>
    <?php foreach($this->businessList() as $biz) : ?>
    <tr class="row<?php echo $i % 2; ?>">
      <td class="align-left"><?php echo (int) $biz->id; ?></td>
      <td><?php echo $biz->name; ?></td>
      
      <td class="center">
        <?php if ($biz->verified == '0') : ?>
        <a class="btn btn-micro" href="<?php echo JRoute::_('index.php?option=com_members_verifiers&task=verifier.verify_biz&id='.$biz->id); ?>">
        No
        </a>
        <?php else: ?>
        <?php echo 'Yes' ?>
        <?php endif; ?>
      </td>
      
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
</div>
