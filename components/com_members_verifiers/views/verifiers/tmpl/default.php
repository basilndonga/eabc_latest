<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Shetrades_verifiers
 * @author     GreenBell Communications <michael@buluma.me.ke>
 * @copyright  Copyright (C) 2015. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_members_verifiers');
$canEdit    = $user->authorise('core.edit', 'com_members_verifiers');
$canCheckin = $user->authorise('core.manage', 'com_members_verifiers');
$canChange  = $user->authorise('core.edit.state', 'com_members_verifiers');
$canDelete  = $user->authorise('core.delete', 'com_members_verifiers');
?>
<?php if (!empty($this->item->id)): ?>

<div class="page-header">
  <h1>Edit your Verifiers account <?php echo $this->item->org_name; ?></h1>
</div>
<?php else: ?>
<div class="page-header">
  <h1>List of Verifiers</h1>
</div>
<?php endif; ?>
<form action="<?php echo JRoute::_('index.php?option=com_members_verifiers&view=verifiers'); ?>" method="post"
      name="adminForm" id="adminForm">
  <?php //echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>
  <div class="table-responsive">
  <table class="table table-striped" id="verifierList">
    <thead>
      <tr>
        <?php if (isset($this->items[0]->state)): ?>
<!--         <th width="5%"> <?php echo JHtml::_('grid.sort', 'JPUBLISHED', 'a.state', $listDirn, $listOrder); ?> </th>
        <?php endif; ?> -->
        <th class=''> <?php echo JHtml::_('grid.sort',  'COM_SHETRADES_VERIFIERS_VERIFIERS_ORG_NAME', 'a.org_name', $listDirn, $listOrder); ?> </th>
        <!-- <th class=''> <?php echo JHtml::_('grid.sort',  'COM_SHETRADES_VERIFIERS_VERIFIERS_SHETRADES_REP', 'a.shetrades_rep', $listDirn, $listOrder); ?> </th> -->
<!--         <th class=''> <?php echo JHtml::_('grid.sort',  'COM_SHETRADES_VERIFIERS_VERIFIERS_EMAIL', 'a.email', $listDirn, $listOrder); ?> </th> -->
        <!-- <th class=''> <?php echo JHtml::_('grid.sort',  'COM_SHETRADES_VERIFIERS_VERIFIERS_ORG_TYPE', 'a.org_type', $listDirn, $listOrder); ?> </th> -->
        <th class=''> <?php echo JHtml::_('grid.sort',  'COM_SHETRADES_VERIFIERS_VERIFIERS_COUNTRY', 'a.country', $listDirn, $listOrder); ?> </th>
        <!-- <th class=''> <?php echo JHtml::_('grid.sort',  'COM_SHETRADES_VERIFIERS_VERIFIERS_WEBSITE', 'a.website', $listDirn, $listOrder); ?> </th> -->
        <?php if (isset($this->items[0]->id)): ?>
        <!-- <th width="1%" class="nowrap center hidden-phone"> <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?> </th> -->
        <?php endif; ?>
        <?php if ($canEdit || $canDelete): ?>
        <th class="center"> <?php echo JText::_('COM_SHETRADES_VERIFIERS_VERIFIERS_ACTIONS'); ?> </th>
        <?php endif; ?>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>"><?php echo $this->pagination->getListFooter(); ?></td>
      </tr>
    </tfoot>
    <tbody>
      <?php foreach ($this->items as $i => $item) : ?>
      <?php $canEdit = $user->authorise('core.edit', 'com_members_verifiers'); ?>
      <?php if (!$canEdit && $user->authorise('core.edit.own', 'com_members_verifiers')): ?>
      <?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
      <?php endif; ?>
      <tr class="row<?php echo $i % 2; ?>">
        <?php if (isset($this->items[0]->state)) : ?>
        <?php $class = ($canEdit || $canChange) ? 'active' : 'disabled'; ?>
<!--         <td class="center"><a class="btn btn-micro <?php echo $class; ?>" href="<?php echo ($canEdit || $canChange) ? JRoute::_('index.php?option=com_members_verifiers&task=verifier.publish&id=' . $item->id . '&state=' . (($item->state + 1) % 2), false, 2) : '#'; ?>">
          <?php if ($item->state == 1): ?>
          <i class="icon-publish"></i>
          <?php else: ?>
          <i class="icon-unpublish"></i>
          <?php endif; ?>
          </a></td> -->
        <?php endif; ?>
        <td><?php if (isset($item->checked_out) && $item->checked_out) : ?>
          <?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'verifiers.', $canCheckin); ?>
          <?php endif; ?>
          <?php echo $this->escape($item->org_name); ?></td>
        <!-- <td><?php echo $item->shetrades_rep; ?></td> -->
        <!-- <td><?php echo $item->email; ?></td> -->
        <!-- <td><?php echo $item->org_type; ?></td> -->
        <td><?php echo $item->country; ?></td>
        <!-- <td><?php echo $item->website; ?></td> -->
        <?php if (isset($this->items[0]->id)): ?>
        <!-- <td class="center hidden-phone"><?php echo (int) $item->id; ?></td> -->
        <?php endif; ?>
        <?php if ($canEdit || $canDelete): ?>
        <td class="center"><?php if ($canEdit): ?>
          <a href="<?php echo JRoute::_('index.php?option=com_members_verifiers&task=verifierform.edit&id=' . $item->id, false, 2); ?>" class="btn btn-mini" type="button"><i class="icon-edit" ></i></a>
          <?php endif; ?>
          <?php if ($canDelete): ?>
          <button data-item-id="<?php echo $item->id; ?>" class="btn btn-mini delete-button" type="button"><i class="icon-trash" ></i></button>
          <?php endif; ?></td>
        <?php endif; ?>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  </div>
  <br />
  <br />
  <input type="hidden" name="task" value=""/>
  <input type="hidden" name="boxchecked" value="0"/>
  <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
  <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
  <?php echo JHtml::_('form.token'); ?>
</form>
<br />
<hr />
<?php if ($this->isVerifier()){ ?>
<a href="<?php echo JRoute::_('index.php?option=com_members_verifiers&view=verifier&id='.$this->verifierId()); ?>" class="btn btn-success btn-small"><i class="icon-plus"></i> 
View Your Verified Businesses
</a>
<?php } else { ?>
<a href="<?php echo JRoute::_('index.php?option=com_members_verifiers&task=verifierform.edit&id=0', false, 2); ?>"
		   class="btn btn-success btn-small"><i
				class="icon-plus"></i> Register As a Verifier</a>
<?php } ?>

<script type="text/javascript">

	jQuery(document).ready(function () {
		jQuery('.delete-button').click(deleteItem);
	});

	function deleteItem() {
		var item_id = jQuery(this).attr('data-item-id');
		<?php if($canDelete) : ?>
		if (confirm("<?php echo JText::_('COM_SHETRADES_VERIFIERS_DELETE_MESSAGE'); ?>")) {
			window.location.href = '<?php echo JRoute::_('index.php?option=com_members_verifiers&task=verifierform.remove&id=', false, 2) ?>' + item_id;
		}
		<?php endif; ?>
	}
</script> 
