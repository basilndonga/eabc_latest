<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Shetrades_verifiers
 * @author     GreenBell Communications <michael@buluma.me.ke>
 * @copyright  Copyright (C) 2015. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::register('Members_verifiersFrontendHelper', JPATH_COMPONENT . '/helpers/members_verifiers.php');

// Execute the task.
$controller = JControllerLegacy::getInstance('Members_verifiers');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
