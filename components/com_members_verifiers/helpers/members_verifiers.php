<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Shetrades_verifiers
 * @author     GreenBell Communications <michael@buluma.me.ke>
 * @copyright  Copyright (C) 2015. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Class Shetrades_verifiersFrontendHelper
 *
 * @since  1.6
 */
class Members_verifiersFrontendHelper
{
	/**
	 * Get an instance of the named model
	 *
	 * @param   string  $name  Model name
	 *
	 * @return null|object
	 */
	public static function getModel($name)
	{
		$model = null;

		// If the file exists, let's
		if (file_exists(JPATH_SITE . '/components/com_members_verifiers/models/' . strtolower($name) . '.php'))
		{
			require_once JPATH_SITE . '/components/com_members_verifiers/models/' . strtolower($name) . '.php';
			$model = JModelLegacy::getInstance($name, 'Members_verifiersModel');
		}

		return $model;
	}
}
