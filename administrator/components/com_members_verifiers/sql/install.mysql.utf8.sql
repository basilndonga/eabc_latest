CREATE TABLE IF NOT EXISTS `#__shetrades_verifiers` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`org_name` VARCHAR(255)  NOT NULL ,
`shetrades_rep` VARCHAR(255)  NOT NULL ,
`email` VARCHAR(255)  NOT NULL ,
`org_type` VARCHAR(255)  NOT NULL ,
`country` VARCHAR(255)  NOT NULL ,
`website` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

