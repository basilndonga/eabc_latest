<?php
defined('_AKEEBA_RESTORATION') or die('Restricted access');
$restoration_setup = array(
	'kickstart.security.password' => 'cOGBLUliQ4Te1eLCXPSYzqdXnIy48Uzw',
	'kickstart.tuning.max_exec_time' => '5',
	'kickstart.tuning.run_time_bias' => '75',
	'kickstart.tuning.min_exec_time' => '0',
	'kickstart.procengine' => 'direct',
	'kickstart.setup.sourcefile' => 'tmp/ju1082.tmp',
	'kickstart.setup.destdir' => 'C:\xampp\htdocs\eabc_repo',
	'kickstart.setup.restoreperms' => '0',
	'kickstart.setup.filetype' => 'zip',
	'kickstart.setup.dryrun' => '0',
	'kickstart.setup.renamefiles' => array(),
	'kickstart.setup.postrenamefiles' => false);