CREATE TABLE IF NOT EXISTS `#__shetrades_biz_info` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`email` VARCHAR(255)  NOT NULL ,
`phone` VARCHAR(255)  NOT NULL ,
`no_of_female` VARCHAR(255)  NOT NULL ,
`location` VARCHAR(255)  NOT NULL ,
`status` VARCHAR(255)  NOT NULL ,
`viewed` VARCHAR(255)  NOT NULL ,
`created_on` TIMESTAMP NOT NULL ,
`modified_on` TIMESTAMP NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

