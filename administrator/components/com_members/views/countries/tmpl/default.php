<?php
/**
 * @version     1.0.0
 * @package     com_members
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHTML::_('script', 'system/multiselect.js', false, true);
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_members/assets/css/shetrades.css');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$canOrder = $user->authorise('core.edit.state', 'com_members');
$saveOrder = $listOrder == 'a.`ordering`';
$input = JFactory::getApplication()->input;
jimport('joomla.form.form');
JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
$form = JForm::getInstance('com_members.country', 'country');
?>

<form action="<?php echo JRoute::_('index.php?option=com_members&view=countries'); ?>" method="post" name="adminForm" id="adminForm">
    <fieldset id="filter-bar">
        <div class="filter-search fltlft">
            <label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
            <input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('Search'); ?>" />
            <button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
            <button type="button" onclick="document.id('filter_search').value = '';
                    this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
        </div>

        
		<div class="filter-select fltrt">
			<select name="filter_published" class="inputbox" onchange="this.form.submit()">
				<option value=""><?php echo JText::_('JOPTION_SELECT_PUBLISHED');?></option>
				<?php echo JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true);?>
			</select>
		</div>


    </fieldset>
    <div class="clr"> </div>

    <table style="margin-top: 75px;" class="adminlist table table-striped">
        <thead> <!-- head -->
            <tr>
                <th width="1%">
                    <input type="checkbox" name="checkall-toggle" value="" onclick="checkAll(this)" />
                </th>
                
		<th class="left">
			<?php echo JHtml::_('grid.sort',  'Name', 'a.`name`', $listDirn, $listOrder); ?>
		</th>

		<th class="left">
			<?php echo JHtml::_('grid.sort',  'Nice Name', 'a.`nicename`', $listDirn, $listOrder); ?>
		</th>

		<th class="left">
			<?php echo JHtml::_('grid.sort',  'ISO 2', 'a.`iso2`', $listDirn, $listOrder); ?>
		</th>

		<th class="left">
			<?php echo JHtml::_('grid.sort',  'ISO 3', 'a.`iso3`', $listDirn, $listOrder); ?>
		</th>

		<th class="left">
			<?php echo JHtml::_('grid.sort',  'Number Code', 'a.`numcode`', $listDirn, $listOrder); ?>
		</th>

		<th class="left">
			<?php echo JHtml::_('grid.sort',  'Phone Code', 'a.`phonecode`', $listDirn, $listOrder); ?>
		</th>

                <?php if (isset($this->items[0]->state)) : ?>
                    <th width="5%"><?php echo JHtml::_('grid.sort', 'JPUBLISHED', 'a.`state`', $listDirn, $listOrder); ?>
                     </th>
                <?php endif; ?>

                <?php if (isset($this->items[0]->ordering)) : ?>
                    <th width="1%">
                        <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ORDERING', 'a.`ordering`', $listDirn, $listOrder); ?>
                        <?php if ($canOrder && $saveOrder) : ?>
                            <?php echo JHtml::_('grid.order', $this->items, 'filesave.png', 'countries.saveorder'); ?>
                        <?php endif; ?>
                    </th>
                <?php endif; ?>

                <?php if (isset($this->items[0]->id)) : ?>
                    <th width="1%" class="nowrap">
                        <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.`id`', $listDirn, $listOrder); ?>
                    </th>
                <?php endif; ?>
            </tr>
        </thead>
        <tfoot>
            <?php
            if (isset($this->items[0])) {
                $colspan = count(get_object_vars($this->items[0]));
            } else {
                $colspan = 10;
            }
            ?>
            <tr>
                <td colspan="<?php echo $colspan ?>">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php
            foreach ($this->items as $i => $item) :
                $ordering = ($listOrder == 'a.ordering');
                $canCreate = $user->authorise('core.create', 'com_members');
                $canEdit = $user->authorise('core.edit', 'com_members');
                $canCheckin = $user->authorise('core.manage', 'com_members');
                $canChange = $user->authorise('core.edit.state', 'com_members');
                ?>
                <tr class="row<?php echo $i % 2; ?>">
                    <td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
                    <td>
			<?php if ($canEdit): ?>
				<a href="<?php echo JRoute::_('index.php?option=com_members&task=country.edit&id=' . (int) $item->id); ?>">
	
					<?php echo $this->escape($item->name); ?>
				</a>
			<?php else: ?>
				<?php echo $this->escape($item->name); ?>
			<?php endif; ?>
		     </td>
	
		     <td><?php echo $item->nicename; ?></td>
		     <td><?php echo $item->iso2; ?></td>
		     <td><?php echo $item->iso3; ?></td>
		     <td><?php echo $item->numcode; ?></td>
		     <td><?php echo $item->phonecode; ?></td>
	

                    <?php if (isset($this->items[0]->state)) { ?>
                        <td class="center">
	<?php echo JHtml::_('jgrid.published', $item->state, $i, 'countries.', $canChange, 'cb'); ?>
</td>
                    <?php } ?>

                    <?php ?><?php if (isset($this->items[0]->ordering)) { ?>
                        <td class="order">
                            <?php if ($canChange) : ?>
                                <?php if ($saveOrder) : ?>
                                    <?php if ($listDirn == 'asc') : ?>
                                        <span><?php echo $this->pagination->orderUpIcon($i, true, 'countries.orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
                                        <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, true, 'countries.orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
                                    <?php elseif ($listDirn == 'desc') : ?>
                                        <span><?php echo $this->pagination->orderUpIcon($i, true, 'countries.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
                                        <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, true, 'countries.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php $disabled = $saveOrder ? '' : 'disabled="disabled"'; ?>
                                <input type="text" name="order[]" size="5" value="<?php echo $item->ordering; ?>" <?php echo $disabled ?> class="text-area-order span6" />
                            <?php else : ?>
                                <?php echo $item->ordering; ?>
                            <?php endif; ?>
                        </td>
                    <?php } ?><?php ?>

                    <?php if (isset($this->items[0]->id)) { ?>
                        <td class="center">
                            <?php echo (int) $item->id; ?>
                        </td>
                    <?php } ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>