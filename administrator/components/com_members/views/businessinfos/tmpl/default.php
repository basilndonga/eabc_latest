<?php
/**
 * @version     1.0.0
 * @package     com_members
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHTML::_('script', 'system/multiselect.js', false, true);
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_members/assets/css/shetrades.css');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$canOrder = $user->authorise('core.edit.state', 'com_members');
$saveOrder = $listOrder == 'a.`ordering`';
$input = JFactory::getApplication()->input;
jimport('joomla.form.form');
JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
$form = JForm::getInstance('com_members.businessinfo', 'businessinfo');
?>

<form action="<?php echo JRoute::_('index.php?option=com_members&view=businessinfos'); ?>" method="post" name="adminForm" id="adminForm">
    <fieldset id="filter-bar">
        <div class="filter-search fltlft">
            <label class="filter-search-lbl" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
            <input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('Search'); ?>" />
            <button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
            <button type="button" onclick="document.id('filter_search').value = '';
                    this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
        </div>

        
		<div class="filter-select fltrt">
			<select name="filter_published" class="inputbox" onchange="this.form.submit()">
				<option value=""><?php echo JText::_('JOPTION_SELECT_PUBLISHED');?></option>
				<?php echo JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true);?>
			</select>
		</div>


    </fieldset>
    <div class="clr"> </div>

    <table style="margin-top: 75px;" class="adminlist table table-striped">
        <thead> <!-- head -->
            <tr>
                <th width="1%">
                    <input type="checkbox" name="checkall-toggle" value="" onclick="checkAll(this)" />
                </th>
                
                <?php if (isset($this->items[0]->state)) : ?>
                    <th width="5%">
	<?php echo JHtml::_('grid.sort', 'JPUBLISHED', 'a.`state`', $listDirn, $listOrder); ?>
</th>
                <?php endif; ?>

                <?php /*?><?php if (isset($this->items[0]->ordering)) : ?> //hide ordering
                    <th width="10%">
                        <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ORDERING', 'a.`ordering`', $listDirn, $listOrder); ?>
                        <?php if ($canOrder && $saveOrder) : ?>
                            <?php echo JHtml::_('grid.order', $this->items, 'filesave.png', 'businessinfos.saveorder'); ?>
                        <?php endif; ?>
                    </th>
                <?php endif; ?><?php */?>

                <?php if (isset($this->items[0]->id)) : ?>
                    <th width="1%" class="nowrap">
                        <?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.`id`', $listDirn, $listOrder); ?>
                    </th>
                <?php endif; ?>
                
				<th class="left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_BUSINESSINFOS_NAME', 'a.`name`', $listDirn, $listOrder); ?>
				</th>

				<th class="left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_BUSINESSINFOS_EMAIL', 'a.`email`', $listDirn, $listOrder); ?>
				</th>

				<th class="left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_BUSINESSINFOS_PHONE', 'a.`phone`', $listDirn, $listOrder); ?>
				</th>

				<th class="left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_BUSINESSINFOS_NO_OF_FEMALE', 'a.`no_of_female`', $listDirn, $listOrder); ?>
				</th>

				<th class="left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_BUSINESSINFOS_LOCATION', 'a.`location`', $listDirn, $listOrder); ?>
				</th>

				<th class="left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_BUSINESSINFOS_STATUS', 'a.`status`', $listDirn, $listOrder); ?>
				</th>

				<th class="left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_BUSINESSINFOS_VIEWED', 'a.`viewed`', $listDirn, $listOrder); ?>
				</th>

				<th class="left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_BUSINESSINFOS_CREATED_ON', 'a.`created_on`', $listDirn, $listOrder); ?>
				</th>

				<?php /*?><th class="left">
					<?php echo JHtml::_('grid.sort',  'COM_SHETRADES_BUSINESSINFOS_MODIFIED_ON', 'a.`modified_on`', $listDirn, $listOrder); ?>
				</th><?php */?>
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'Headed by Woman', 'a.`headed_by_woman`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  '% owned by women', 'a.`perc_owned_by_woman`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  '# of employees', 'a.`no_of_employees`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'Country', 'a.`country`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'City', 'a.`city`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'P.O Box', 'a.`po_box`', $listDirn, $listOrder); ?>
				</th>         
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'www', 'a.`www`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'facebook', 'a.`facebook`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'twitter', 'a.`twitter`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'linkedin', 'a.`linkedin`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'favorites', 'a.`favorites`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'dislikes', 'a.`dislikes`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'fax', 'a.`fax`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'Annual Revenue in USD', 'a.`annual_revenue_usd`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'Annual Revenue per Year', 'a.`annual_revenue_year`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'Annual Values Exports', 'a.`annual_values_exports`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'Annual Value of Exports Year', 'a.`annual_values_exports_yr`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'Year of exports', 'a.`year_of_exports`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'Export Countries', 'a.`export_countries`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'Lead Times', 'a.`lead_times`', $listDirn, $listOrder); ?>
				</th>
                
                <th class="left">
					<?php echo JHtml::_('grid.sort',  'Delivery Terms', 'a.`delivery_terms`', $listDirn, $listOrder); ?>
				</th>
                
                <?php /*?><th class="left">
					<?php echo JHtml::_('grid.sort',  'Company Description', 'a.`company_desc`', $listDirn, $listOrder); ?>
				</th><?php */?>

                
            </tr>
        </thead>
        <tfoot>
            <?php
            if (isset($this->items[0])) {
                $colspan = count(get_object_vars($this->items[0]));
            } else {
                $colspan = 10;
            }
            ?>
            <tr>
                <td colspan="<?php echo $colspan ?>">
                    <?php echo $this->pagination->getListFooter(); ?>
                </td>
            </tr>
        </tfoot>
        <tbody>
            <?php
            foreach ($this->items as $i => $item) :
                $ordering = ($listOrder == 'a.ordering');
                $canCreate = $user->authorise('core.create', 'com_members');
                $canEdit = $user->authorise('core.edit', 'com_members');
                $canCheckin = $user->authorise('core.manage', 'com_members');
                $canChange = $user->authorise('core.edit.state', 'com_members');
                ?>
                <tr class="row<?php echo $i % 2; ?>">
                    <td class="center">
                        <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                    </td>
                    
                    <?php if (isset($this->items[0]->state)) { ?>
                        <td class="center">
	<?php echo JHtml::_('jgrid.published', $item->state, $i, 'businessinfos.', $canChange, 'cb'); ?>
</td>
                    <?php } ?>

                    <?php /*?><?php if (isset($this->items[0]->ordering)) { ?> //hide ordering
                        <td class="order">
                            <?php if ($canChange) : ?>
                                <?php if ($saveOrder) : ?>
                                    <?php if ($listDirn == 'asc') : ?>
                                        <span><?php echo $this->pagination->orderUpIcon($i, true, 'businessinfos.orderup', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
                                        <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, true, 'businessinfos.orderdown', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
                                    <?php elseif ($listDirn == 'desc') : ?>
                                        <span><?php echo $this->pagination->orderUpIcon($i, true, 'businessinfos.orderdown', 'JLIB_HTML_MOVE_UP', $ordering); ?></span>
                                        <span><?php echo $this->pagination->orderDownIcon($i, $this->pagination->total, true, 'businessinfos.orderup', 'JLIB_HTML_MOVE_DOWN', $ordering); ?></span>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php $disabled = $saveOrder ? '' : 'disabled="disabled"'; ?>
                                <input type="text" name="order[]" size="5" value="<?php echo $item->ordering; ?>" <?php echo $disabled ?> class="text-area-order" />
                            <?php else : ?>
                                <?php echo $item->ordering; ?>
                            <?php endif; ?>
                        </td>
                    <?php } ?><?php */?>

                    <?php if (isset($this->items[0]->id)) { ?>
                        <td class="center">
                            <?php echo (int) $item->id; ?>
                        </td>
                    <?php } ?>
                    
					<td>
					<?php if (isset($item->checked_out) && $item->checked_out) : ?>
						<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'businessinfos.', $canCheckin); ?>
					<?php endif; ?>
					<?php if ($canEdit): ?>
						<a href="<?php echo JRoute::_('index.php?option=com_members&task=businessinfo.edit&id=' . (int) $item->id); ?>">

							<?php echo $this->escape($item->name); ?>
						</a>
					<?php else: ?>
						<?php echo $this->escape($item->name); ?>
					<?php endif; ?>
					</td>

					<td>
						<?php echo $item->email; ?>
					</td>

					<td>
						<?php echo $item->phone; ?>
					</td>

					<td>
						<?php echo $item->no_of_female; ?>
					</td>

					<td>
						<?php echo $item->location; ?>
					</td>

					<td>
						<?php echo $item->status; ?>
					</td>

					<td>
						<?php echo $item->viewed; ?>
					</td>

					<td>
						<?php echo $item->created_on; ?>
					</td>

					<?php /*?><td>
						<?php echo $item->modified_on; ?>
					</td><?php */?>
                    
                    <td>
						<?php echo $item->headed_by_woman; ?>
					</td>
                    
                    <td>
						<?php echo $item->perc_owned_by_woman; ?>
					</td>
                    
                    <td>
						<?php echo $item->no_of_employees; ?>
					</td>
                    
                     <td>
						<?php echo $item->country; ?>
					</td>
                    
                     <td>
						<?php echo $item->city; ?>
					</td>
                    
                    <td>
						<?php echo $item->po_box; ?>
					</td>
                    
                    <td>
						<a href="http://<?php echo $item->www; ?>" target="_blank"><?php echo $item->www; ?></a>
					</td>
                    
                    <td>
						<?php echo $item->facebook; ?>
					</td>
                    
                    <td>
						<?php echo $item->twitter;?>
					</td>
                    
                     <td>
						<?php echo $item->linkedin;?>
					</td>
                    
                    <td>
						<?php echo $item->favorites;?>
					</td>
                    
                    <td>
						<?php echo $item->dislikes;?>
					</td>
                    
                    <td>
						<?php echo $item->fax;?>
					</td>
                    
                     <td>
						<?php echo $item->annual_revenue_usd;?>
					</td>
                    
                    <td>
						<?php echo $item->annual_revenue_year;?>
					</td>
                    
                    <td>
						<?php echo $item->annual_values_exports;?>
					</td>
                    
                    <td>
						<?php echo $item->annual_values_exports_yr;?>
					</td>
                    
                    <td>
						<?php echo $item->year_of_exports;?>
					</td>
                    
                    <td>
						<?php echo $item->export_countries;?>
					</td>
                    
                    <td>
						<?php echo $item->lead_times;?>
					</td>
                    
                    <td>
						<?php echo $item->delivery_terms;?>
					</td>
                    
                    <?php /*?><td>
						<?php echo $item->company_desc;?>
					</td><?php */?>

                    
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div>
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="boxchecked" value="0" />
        <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
        <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>