<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_shetrades/assets/css/shetrades.css');
?>
<script type="text/javascript">
    function getScript(url,success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0],
        done = false;
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState
                || this.readyState == 'loaded'
                || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }
        };
        head.appendChild(script);
    }
    getScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',function() {
        js = jQuery.noConflict();
        js(document).ready(function(){
            

            Joomla.submitbutton = function(task)
            {
                if (task == 'businessinfo.cancel') {
                    Joomla.submitform(task, document.getElementById('businessinfo-form'));
                }
                else{
                    
                    if (task != 'businessinfo.cancel' && document.formvalidator.isValid(document.id('businessinfo-form'))) {
                        
                        Joomla.submitform(task, document.getElementById('businessinfo-form'));
                    }
                    else {
                        alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
                    }
                }
            }
        });
    });
</script>

<form action="<?php echo JRoute::_('index.php?option=com_shetrades&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="businessinfo-form" class="form-validate">
    <div class="width-60 fltlft">
        <fieldset class="adminform">
            <legend><?php echo JText::_('COM_SHETRADES_LEGEND_BUSINESSINFO'); ?></legend>
            <ul class="adminformlist">

                				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php if(empty($this->item->created_by)){ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

				<?php } 
				else{ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

				<?php } ?>				<li><?php echo $this->form->getLabel('name'); ?>
				<?php echo $this->form->getInput('name'); ?></li>
				<li><?php echo $this->form->getLabel('email'); ?>
				<?php echo $this->form->getInput('email'); ?></li>
				<li><?php echo $this->form->getLabel('phone'); ?>
				<?php echo $this->form->getInput('phone'); ?></li>
				<li><?php echo $this->form->getLabel('no_of_female'); ?>
				<?php echo $this->form->getInput('no_of_female'); ?></li>
				<li><?php echo $this->form->getLabel('location'); ?>
				<?php echo $this->form->getInput('location'); ?></li>
				<li><?php echo 'GPS *'; ?>
				<?php echo $this->form->getInput('status'); ?></li>
				<li><?php echo 'Headed by Woman'; ?>
				<?php echo $this->form->getInput('headed_by_woman'); ?></li>    
				<li><?php echo $this->form->getLabel('viewed'); ?>
				<?php echo $this->form->getInput('viewed'); ?></li>            
				<li><?php echo '% Owned by Woman'; ?>
				<?php echo $this->form->getInput('perc_owned_by_woman'); ?></li>            
				<li><?php echo '# of Employees'; ?>
				<?php echo $this->form->getInput('no_of_employees'); ?></li>           
				<li><?php echo 'Country'; ?>
				<?php echo $this->form->getInput('country'); ?></li>          
				<li><?php echo 'City'; ?>
				<?php echo $this->form->getInput('city'); ?></li>         
				<li><?php echo 'P.O Box'; ?>
				<?php echo $this->form->getInput('po_box'); ?></li>         
				<li><?php echo 'WWW'; ?>
				<?php echo $this->form->getInput('www'); ?></li>       
				<li><?php echo 'Facebook'; ?>
				<?php echo $this->form->getInput('facebook'); ?></li>       
				<li><?php echo 'Twitter'; ?>
				<?php echo $this->form->getInput('twitter'); ?></li>       
				<li><?php echo 'LinkedIn'; ?>
				<?php echo $this->form->getInput('linkedin'); ?></li>       
				<li><?php echo 'Favorites'; ?>
				<?php echo $this->form->getInput('favorites'); ?></li>    
				<li><?php echo 'Dislikes'; ?>
				<?php echo $this->form->getInput('dislikes'); ?></li>    
				<li><?php echo 'Fax'; ?>
				<?php echo $this->form->getInput('fax'); ?></li>    
				<li><?php echo 'Annual Revenue in USD'; ?>
				<?php echo $this->form->getInput('annual_revenue_year'); ?></li>    
				<li><?php echo 'Annual Revenue per Year'; ?>
				<?php echo $this->form->getInput('annual_revenue_year'); ?></li>    
				<li><?php echo 'Annual Values Exports'; ?>
				<?php echo $this->form->getInput('annual_values_exports'); ?></li>    
				<li><?php echo 'Annual Value of Exports Year'; ?>
				<?php echo $this->form->getInput('annual_values_exports_yr'); ?></li>   
				<li><?php echo 'Year of exports'; ?>
				<?php echo $this->form->getInput('year_of_exports'); ?></li>   
				<li><?php echo 'Export Countries'; ?>
				<?php echo $this->form->getInput('export_countries'); ?></li>   
				<li><?php echo 'Lead Times'; ?>
				<?php echo $this->form->getInput('lead_times'); ?></li>   
				<li><?php echo 'Delivery Terms'; ?>
				<?php echo $this->form->getInput('delivery_terms'); ?></li>   
				<li><?php echo 'Company Description'; ?>
				<?php echo $this->form->getInput('company_desc'); ?></li>
                
                
				<li><?php echo $this->form->getLabel('created_on'); ?>
				<?php echo $this->form->getInput('created_on'); ?></li>
				<li><?php echo $this->form->getLabel('modified_on'); ?>
				<?php echo $this->form->getInput('modified_on'); ?></li>


            </ul>
        </fieldset>
    </div>

    <div class="clr"></div>

<?php if (JFactory::getUser()->authorise('core.admin','shetrades')): ?>
	<div class="width-100 fltlft">
		<?php echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
		<?php echo JHtml::_('sliders.panel', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL'), 'access-rules'); ?>
		<fieldset class="panelform">
			<?php echo $this->form->getLabel('rules'); ?>
			<?php echo $this->form->getInput('rules'); ?>
		</fieldset>
		<?php echo JHtml::_('sliders.end'); ?>
	</div>
<?php endif; ?>

    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>
    <div class="clr"></div>

    <style type="text/css">
        /* Temporary fix for drifting editor fields */
        .adminformlist li {
            clear: both;
        }
    </style>
</form>