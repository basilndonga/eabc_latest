<?php
/**
 * @version     1.0.0
 * @package     com_shetrades
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_members/assets/css/shetrades.css');
?>
<script type="text/javascript">
    function getScript(url,success) {
        var script = document.createElement('script');
        script.src = url;
        var head = document.getElementsByTagName('head')[0],
        done = false;
        // Attach handlers for all browsers
        script.onload = script.onreadystatechange = function() {
            if (!done && (!this.readyState
                || this.readyState == 'loaded'
                || this.readyState == 'complete')) {
                done = true;
                success();
                script.onload = script.onreadystatechange = null;
                head.removeChild(script);
            }
        };
        head.appendChild(script);
    }
    getScript('//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',function() {
        js = jQuery.noConflict();
        js(document).ready(function(){
            

            Joomla.submitbutton = function(task)
            {
                if (task == 'businessinfo.cancel') {
                    Joomla.submitform(task, document.getElementById('businessinfo-form'));
                }
                else{
                    
                    if (task != 'businessinfo.cancel' && document.formvalidator.isValid(document.id('businessinfo-form'))) {
                        
                        Joomla.submitform(task, document.getElementById('businessinfo-form'));
                    }
                    else {
                        alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
                    }
                }
            }
        });
    });
</script>

<form action="<?php echo JRoute::_('index.php?option=com_members&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="businessinfo-form" class="form-validate">

   <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
   <input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
   <input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
   <input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
   <input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>
	                <?php
			    // Iterate through the normal form fieldsets and display each one.
			    foreach ($this->form->getFieldsets('main') as $fieldsets => $fieldset):
			    ?>
			    <?php echo JHtml::_('bootstrap.addTab', 'myTab', $fieldset->name, JText::_($fieldset->label, true)); ?>
			    <fieldset class="adminform">
			        <legend>
			            <?php echo JText::_($fieldset->label); ?>
			        </legend>
			        <!-- Fields go here -->
			        
				<?php
				// Iterate through the fields and display them.
				foreach($this->form->getFieldset($fieldset->name) as $field):
				    // If the field is hidden, only use the input.
				    if ($field->hidden):
				        echo $field->input;
				    else:
				    ?>
				    <div class="control-group">
				      <div class="control-label"><?php echo $field->label; ?></div>
				      <div class="controls"<?php echo ($field->type == 'Editor' || $field->type == 'Textarea') ? ' style="clear: both; margin: 0;"' : ''?>>
				        <?php echo $field->input ?>
				      </div>
				    </div>
				    <?php
				    endif;
				endforeach;
				?>
				
			    </fieldset>
			    <?php echo JHtml::_('bootstrap.endTab'); ?>
		        <?php endforeach; ?>
	     <?php echo JHtml::_('bootstrap.endTabSet'); ?>
	        
	     <?php if (JFactory::getUser()->authorise('core.admin','shetrades')) : ?>
	     <?php //echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
			<?php //echo $this->form->getInput('rules'); ?>
	     <?php //echo JHtml::_('bootstrap.endTab'); ?>
	     <?php endif; ?>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>