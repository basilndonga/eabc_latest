<?php
/**
 * @version     1.0.0
 * @package     com_members
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
error_reporting(E_ALL);
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');
jimport('joomla.html.html');

/**
 * Shetrades model.
 */
class MembersModelBusinessinfo extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_SHETRADES';


	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Businessinfo', $prefix = 'MembersTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();
		// Get the form.
		$form = $this->loadForm('com_members.businessinfo', 'businessinfo', array('control' => 'jform', 'load_data' => $loadData));     
			/**/
			if($form->getFieldAttribute('created_on', 'default') == 'NOW'){
				$form->setFieldAttribute('created_on', 'default', date('Y-m-d H:i:s'));
			}		
			if($form->getFieldAttribute('modified_on', 'default') == 'NOW'){
				$form->setFieldAttribute('modified_on', 'default', date('Y-m-d H:i:s'));
			}
		if (empty($form)) {
			return false;
		}
		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_members.edit.businessinfo.data', array());
		if (empty($data)) {
		    $data = $this->getItem();            
		}
		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
	     if ($item = parent::getItem($pk)) {
		//Do any procesing on fields here if needed
	     }
	     return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');
		if (empty($table->id)) {
			// Set ordering to the last item if not set
			if (@$table->ordering === '') {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__shetrades_biz_info');
				$max = $db->loadResult();
				$table->ordering = $max+1;
			}
		}
	}
	
	public function save($data){
        $db = $this->getDBO();
	    $formdata = $data['main'];
	    $query = $db->getQuery(true);

        $insertdata = array(
	    	'created_by' => $db->quote($formdata['created_by']),
	    	'state' => $db->quote($formdata['state']),
	    	'name' => $db->quote($formdata['name']),
	    	'year_est' => $db->quote($formdata['year_est']),
	    	'no_of_female' => $db->quote($formdata['no_of_female']),
	    	'location' => $db->quote($formdata['location']),
	    	'status' => $db->quote($formdata['status']),
	    	'viewed' => $db->quote($formdata['viewed']),
	    	'created_on' => $db->quote($formdata['created_on']),
	    	'headed_by_woman' => $db->quote($formdata['headed_by_woman']),
	    	'perc_owned_by_woman' => $db->quote($formdata['perc_owned_by_woman']),
	    	'no_of_employees' => $db->quote($formdata['no_of_employees']),
	    	'country' => $db->quote($formdata['country']),
	    	'city' => $db->quote($formdata['city']),
	    	'company_desc' => $db->quote($formdata['company_desc']),
	    	'annual_revenue_year' => $db->quote($formdata['annual_revenue_year']),
	    	'annual_values_exports' => $db->quote($formdata['annual_values_exports']),
	    	'year_of_exports' => $db->quote($formdata['year_of_exports']),
	    	'export_countries' => $db->quote($formdata['export_countries']),
	    	'lead_times' => $db->quote($formdata['lead_times']),
	    	'delivery_terms' => $db->quote($formdata['delivery_terms']),
	    	'email' => $db->quote($formdata['email']),
	    	'phone' => $db->quote($formdata['phone']),
	    	'po_box' => $db->quote($formdata['po_box']),
	    	'fax' => $db->quote($formdata['fax']),
	    	'www' => $db->quote($formdata['www']),
	    	'facebook' => $db->quote($formdata['facebook']),
	    	'twitter' => $db->quote($formdata['twitter']),
	    	'linkedin' => $db->quote($formdata['linkedin']),
	    	'asset_id' => $db->quote($formdata['asset_id']),
	    	'primary_customers' => $db->quote($formdata['primary_customers']),
	    	'company_difference' => $db->quote($formdata['company_difference'])
	    );
	    $columns = array_keys($insertdata);
		$values = array_values($insertdata);
		$query = "INSERT INTO #__shetrades_biz_info (" .implode(',', $columns). ") VALUES (" .implode(',',$values). ")";
		/*$query->insert($db->quoteName('#__shetrades_biz_info'))
			  ->columns($db->quoteName(implode(',', $columns)))
			  ->values(implode(',', $values));
	    */
	    $db->setQuery($query);
        $db->execute();

        //Then get the id of the last inserted element
        //We'll use this to do further distributed inserts

        $bizid = $db->insertid(); 
        $type = 'none';
        if ($bizid != 0) {
        	$biz_buying = array(
        		'biz_id' => $db->quote($bizid),
        		'type' => $db->quote($type),
        		'item' => $db->quote($formdata['buying'])
        	);
        	$this->saveBizBuying($biz_buying);
        	//
        	$biz_offering = array(
        		'biz_id' => $db->quote($bizid),
        		'type' => $db->quote($type),
        		'item' => $db->quote($formdata['buying'])
        	);
        	$this->saveBizoffering($biz_offering);
        	//
        	$accreditation = array(
        		'biz_id' => $db->quote($bizid),
        		//'type' => $db->quote($type),
        		'accreditation_id' => $db->quote($formdata['accreditation_id']),
				'accreditation_date' => $db->quote($formdata['accreditation_date'])
        	);
			$this->saveBizAccreditations($accreditation);
			//
			$certificates = array(
        		'biz_id' => $db->quote($bizid),
        		//'type' => $db->quote($type),
        		'certificates' => $db->quote($formdata['certifications']),
				//'accreditation_date' => $db->quote($formdata['accreditation_date'])
        	);
			$this->saveBizCerts($certificates);
			//
			$certificates = array(
        		'biz_id' => $db->quote($bizid),
        		//'type' => $db->quote($type),
        		'certificates' => $db->quote($formdata['certifications']),
				//'accreditation_date' => $db->quote($formdata['accreditation_date'])
        	);
			$this->saveBizCerts($certificates);
			//
			$companyreps = array(
        		'biz_id' => $db->quote($bizid),
        		//'type' => $db->quote($type),
        		'title' => $db->quote($formdata['company_rep_title']),
				'contacts' => $db->quote($formdata['company_rep_contacts'])
        	);
			$this->saveBizRep($companyreps);
        	return true;
        } 
        else {
        	return false;
        }   
	
	}
	function saveBizBuying($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);
	    $columns = array_keys($data);
		$values = array_values($data);
		/*
		$query->insert($db->quoteName('#__shetrades_biz_buying'))
			  ->columns($db->quoteName(implode(',', $columns)))
			  ->values(implode(',', $values));
		*/
		$query = "INSERT INTO #__shetrades_biz_buying (" .implode(',', $columns). ") VALUES (" .implode(',',$values). ")";
	    $db->setQuery($query);
        $db->execute();

	}
	function getBizBuying($bizid){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);
	}
	function saveBizOffering($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);
	    $columns = array_keys($data);
		$values = array_values($data);
		$query = "INSERT INTO #__shetrades_biz_offering (" .implode(',', $columns). ") VALUES (" .implode(',',$values). ")";
	    $db->setQuery($query);
        $db->execute();
		
	}
	function getBizOffering($bizid){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);

	}
	function saveBizCerts($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);
	    $columns = array_keys($data);
		$values = array_values($data);
		$query = "INSERT INTO #__shetrades_biz_certification (" .implode(',', $columns). ") VALUES (" .implode(',',$values). ")";
	    $db->setQuery($query);
        $db->execute();

	}
	function getBizcerts($bizid){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);

	}
	function saveBizAccreditations($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);
	    $columns = array_keys($data);
		$values = array_values($data);
		$query = "INSERT INTO #__shetrades_accreditations (" .implode(',', $columns). ") VALUES (" .implode(',',$values). ")";
	    $db->setQuery($query);
        $db->execute();

	}
	function getBizAccreditations($bizid){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);

	}
	function saveBizMedia($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);

	}
	function getBizMedia($bizid){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);

	}
	function saveBizMatches($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);

	}
	function getBizMatches($bizid){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);

	}
	function saveBizRep($data = array()){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);
	    $columns = array_keys($data);
		$values = array_values($data);
		$query = "INSERT INTO #__shetrades_biz_rep (" .implode(',', $columns). ") VALUES (" .implode(',',$values). ")";
	    $db->setQuery($query);
        $db->execute();

	}
	function getBizRep($bizid){
		$db = $this->getDBO();
	    $query = $db->getQuery(true);

	}

}