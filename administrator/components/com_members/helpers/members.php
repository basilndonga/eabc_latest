<?php

/**
 * @version     1.0.0
 * @package     com_members
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Michael <michael@buluma.me.ke> - http://www.buluma.me.ke
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Shetrades helper.
 */
class MembersHelper {

    /**
     * Configure the Linkbar.
     */
    public static function addSubmenu($vName = '') {
        JSubMenuHelper::addEntry(JText::_('Businesses'),'index.php?option=com_members&view=businessinfos',$vName == 'businessinfos');
	JSubMenuHelper::addEntry(JText::_('Services'),'index.php?option=com_members&view=services',$vName == 'services');
	JSubMenuHelper::addEntry(JText::_('Products'),'index.php?option=com_members&view=products',$vName == 'products');
	JSubMenuHelper::addEntry(JText::_('Countries'),'index.php?option=com_members&view=countries',$vName == 'countries');

    }

    /**
     * Gets a list of the actions that can be performed.
     *
     * @return	JObject
     * @since	1.6
     */
    public static function getActions() {
        $user = JFactory::getUser();
        $result = new JObject;

        $assetName = 'com_members';

        $actions = array(
            'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
        );

        foreach ($actions as $action) {
            $result->set($action, $user->authorise($action, $assetName));
        }

        return $result;
    }


}
