<?php
/**
 * @package     Joomla.Module
 * @subpackage  mod_facebook_widget_plus
 *
 * @copyright   Copyright (C) 2014 Social Widgets Plus, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later
 */

defined('_JEXEC') or die;

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
require JModuleHelper::getLayoutPath('mod_facebook_widget_plus', $params->get('layout', 'default'));