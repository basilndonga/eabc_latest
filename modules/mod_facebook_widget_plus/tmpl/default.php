<?php
/**
 * @package     Joomla.Module
 * @subpackage  mod_facebook_widget_plus
 *
 * @copyright   Copyright (C) 2014 Social Widgets Plus, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later
*/

defined('_JEXEC') or die;
$document = JFactory::getDocument();
$backgroundColor = '#c7c7c7';

$style = "";
$style .= "
			.fb-page {
				background:$backgroundColor;
				padding:0px;
				border:solid 1px #c7c7c7;
				-webkit-box-shadow: 0px 0px 14px rgba(0, 0, 0, 0.30);
				-moz-box-shadow: 0px 0px 14px rgba(0, 0, 0, 0.30);
				box-shadow: 0px 0px 14px rgba(0, 0, 0, 0.30);
               }


		";
$document->addStyleDeclaration($style);

?>

			<!-- Top Navigation -->

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>



    <div class="mod_facebook_widget_plus <?php echo $moduleclass_sfx;?>">
        <div class="fb-page" data-href="<?php echo $params->get('fb_url');?>"
             data-width="<?php echo $params->get('width');?>"
             data-height="<?php echo $params->get('height');?>"
             data-small-header="false" data-adapt-container-width="true"
             data-hide-cover="<?php echo $params->get('header'); ?>"
             data-show-facepile="<?php echo $params->get('face'); ?>"
             data-show-posts="<?php echo $params->get('post'); ?>"><div class="fb-xfbml-parse-ignore">
                <blockquote cite="<?php echo $params->get('fb_url');?>">
                    <a href="<?php echo $params->get('fb_url');?>">Facebook</a></blockquote></div></div>

    </div>
    <div style="width: <?php echo $params->get('width');?>px;font-size: 9px; color: #808080; font-weight: normal; font-family: tahoma,verdana,arial,sans-serif; line-height: 1.28; text-align: right; direction: ltr; position: relative; top: -13px"></div>
