<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<hr>
<ul class="latesthomenews<?php echo $moduleclass_sfx; ?>">
<?php foreach ($list as $item) : 
	// print_r($item);
?>
	<li itemscope itemtype="https://schema.org/Article">
		<div class="col-sm-1">
			<i class="fa fa-book"></i> &nbsp;
		</div>
		<div class="col-sm-11">
			<span itemprop="date" class="date_home">
			<?php
			 $date=$item->created;
	         $phpdate=strtotime($date);
	         $new_date=date("j F, Y", $phpdate);
	         echo $new_date;
			?>
			</span>
			<span class="category_title"><?php echo $item->category_title; ?></span>
			<br />
			<a href="<?php echo $item->link; ?>" itemprop="url">
			<span itemprop="name" class="new_title">
				<?php echo $item->title; ?>
			</span>
			</a>
			<div class="clearfix"></div>
		</div>
	</li>
<?php endforeach; ?>
</ul>


<a href="#" class="btn news_readmore">More in News</a>