<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<hr>
<ul class="latestnews<?php echo $moduleclass_sfx; ?>">
<?php foreach ($list as $item) : 
	// print_r($item);
?>
	<li itemscope itemtype="https://schema.org/Article">
		<span class="pull-left">
			<i class="fa fa-book"></i> &nbsp;
		</span>
		<a href="<?php echo $item->link; ?>" itemprop="url">
			<span itemprop="name">
				<?php echo $item->title; ?>
			</span>
			</a>
			<div class="clearfix"></div>
			<span itemprop="date" class="date">
			<?php
			 $date=$item->created;
	         $phpdate=strtotime($date);
	         $new_date=date("j F, Y", $phpdate);
	         echo $new_date;
			?>
			</span>
		
	</li>
<?php endforeach; ?>
</ul>
