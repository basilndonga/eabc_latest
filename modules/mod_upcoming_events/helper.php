<?php
/**
 * Helper class for Hello World! module
 * 
 * @package    Joomla.Tutorials
 * @subpackage Modules
 * @link http://docs.joomla.org/J3.x:Creating_a_simple_module/Developing_a_Basic_Module
 * @license        GNU/GPL, see LICENSE.php
 * mod_helloworld is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class ModUpcomingEventsHelper
{
    /**
     * Retrieves the hello message
     *
     * @param   array  $params An object containing the module parameters
     *
     * @access public
     */    

    //get list of featured concepts
    public static function getUpcoming()
    {
        // Get a db connection.
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
         
        // Build the query
        $query->select($db->quoteName(array('id','state', 'title', 'image','startdate','alias')));
        $query->from($db->quoteName('#__icagenda_events'));
        // $query->where($db->quoteName('state') . ' = '. $db->quote('1'));
        $query->order('ordering DESC');
        $query->setLimit('2');
         
        $db->setQuery($query);
        $results = $db->loadObjectList();
        // var_dump($results);
        return $results;
        // Print the result

        foreach($results as $result){ ?>
            
                <ul class="ltst_publications<?php echo $moduleclass_sfx; ?>" >
  <?php foreach ($results as $pub) : ?>
    <li>
      <a href="index.php?option=com_issues&view=issue&id=<?php echo $pub->id; ?>&Itemid=278"><i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo $pub->issue_title; ?> </a> <span class="labels">
            <a href="#" class="label v-align-text-top labelstyle-7057ff linked-labelstyle-7057ff" style="background-color: #353a4a; color: #fff;" title="Label: good first issue"><?php echo $pub->cat_name; ?></a>
        </span>
    </li>
  <?php endforeach; ?>
  </ul>
        <?php }
    }

}